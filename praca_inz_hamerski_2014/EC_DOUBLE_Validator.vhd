----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:15:58 11/21/2014 
-- Design Name: 
-- Module Name:    EC_DOUBLE_Validator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EC_DOUBLE_Validator is
	port( P_X 				 : in std_logic_vector (8 downto 0);
			P_Y 				 : in std_logic_vector (8 downto 0);
			SIG_ZERO_DIV	 :	out std_logic;
			ZERO  	       : out std_logic_vector (1 downto 0));	
end EC_DOUBLE_Validator;

architecture Behavioral of EC_DOUBLE_Validator is

begin

process(P_X,P_Y)
begin
	if(P_X = "000000000" and P_Y = "000000000") then	
		ZERO <= "10";
		SIG_ZERO_DIV <= '0';
	elsif(P_X = "000000000" and P_Y /= "000000000") then	 
		SIG_ZERO_DIV <= '1';
	else
		ZERO <= "01"; -- ok
		SIG_ZERO_DIV <= '0';
	end if;
end process;

end Behavioral;

