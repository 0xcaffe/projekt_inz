--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:44:15 11/16/2014
-- Design Name:   
-- Module Name:   D:/inzynierka/praca_inz_hamerski_2014/MULTIPLEXER_Test_Bench.vhd
-- Project Name:  praca_inz_hamerski_2014
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: MULTIPLEXER_3
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY MULTIPLEXER_Test_Bench IS
END MULTIPLEXER_Test_Bench;
 
ARCHITECTURE behavior OF MULTIPLEXER_Test_Bench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MULTIPLEXER_3
    PORT(
         X_1 : IN  std_logic_vector(8 downto 0);
         X_2 : IN  std_logic_vector(8 downto 0);
         X_3 : IN  std_logic_vector(8 downto 0);
         CONTROLL : IN  std_logic_vector(1 downto 0);
         OUTPUT : OUT  std_logic_vector(8 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal X_1 : std_logic_vector(8 downto 0) := (others => '0');
   signal X_2 : std_logic_vector(8 downto 0) := (others => '0');
   signal X_3 : std_logic_vector(8 downto 0) := (others => '0');
   signal CONTROLL : std_logic_vector(1 downto 0) := (others => '0');

 	--Outputs
   signal OUTPUT : std_logic_vector(8 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MULTIPLEXER_3 PORT MAP (
          X_1 => X_1,
          X_2 => X_2,
          X_3 => X_3,
          CONTROLL => CONTROLL,
          OUTPUT => OUTPUT
        );

   -- Clock process definitions

 
 

   -- Stimulus process
   stim_proc: process 
   begin		
      -- hold reset state for 100 ns.

		CONTROLL <= "10";
      X_1 <= conv_std_logic_vector(11, 9);
		X_2 <= conv_std_logic_vector(22, 9);
		X_3 <= conv_std_logic_vector(33, 9);
		

      wait;
   end process;

END;
