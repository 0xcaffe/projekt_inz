<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK" />
        <signal name="X(8:0)" />
        <signal name="Y(8:0)" />
        <signal name="XLXN_4(8:0)" />
        <signal name="XLXN_5(8:0)" />
        <signal name="XLXN_6(8:0)" />
        <signal name="TEMP(8:0)">
        </signal>
        <signal name="FINISHED" />
        <signal name="PRODUCT(8:0)" />
        <signal name="TEMP_OUT(8:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="X(8:0)" />
        <port polarity="Input" name="Y(8:0)" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Output" name="PRODUCT(8:0)" />
        <port polarity="Output" name="TEMP_OUT(8:0)" />
        <blockdef name="Coverter">
            <timestamp>2014-11-15T19:47:59</timestamp>
            <rect width="352" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-108" height="24" />
            <line x2="480" y1="-96" y2="-96" x1="416" />
            <rect width="64" x="416" y="-44" height="24" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
        </blockdef>
        <blockdef name="GF_Add_Log">
            <timestamp>2014-11-15T20:10:23</timestamp>
            <rect width="64" x="416" y="20" height="24" />
            <line x2="480" y1="32" y2="32" x1="416" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="480" y1="-288" y2="-288" x1="416" />
            <rect width="64" x="416" y="-220" height="24" />
            <line x2="480" y1="-208" y2="-208" x1="416" />
            <rect width="64" x="416" y="-140" height="24" />
            <line x2="480" y1="-128" y2="-128" x1="416" />
            <rect width="64" x="416" y="-60" height="24" />
            <line x2="480" y1="-48" y2="-48" x1="416" />
            <rect width="352" x="64" y="-320" height="384" />
        </blockdef>
        <block symbolname="Coverter" name="XLXI_1">
            <blockpin signalname="XLXN_6(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="TEMP(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="XLXN_4(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="XLXN_5(8:0)" name="OUT_VALUE(8:0)" />
        </block>
        <block symbolname="GF_Add_Log" name="XLXI_2">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="X(8:0)" name="X(8:0)" />
            <blockpin signalname="Y(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_4(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="XLXN_5(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="FINISHED" name="FINISHED" />
            <blockpin signalname="PRODUCT(8:0)" name="PRODUCT(8:0)" />
            <blockpin signalname="XLXN_6(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="TEMP(8:0)" name="OUT_VALUE(8:0)" />
            <blockpin signalname="TEMP_OUT(8:0)" name="TEMP_OUT(8:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1008" y="1200" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1008" y="1456" name="XLXI_1" orien="R0">
        </instance>
        <branch name="CLK">
            <wire x2="1008" y1="912" y2="912" x1="800" />
        </branch>
        <branch name="X(8:0)">
            <wire x2="1008" y1="976" y2="976" x1="800" />
        </branch>
        <branch name="Y(8:0)">
            <wire x2="1008" y1="1040" y2="1040" x1="800" />
        </branch>
        <branch name="XLXN_4(8:0)">
            <wire x2="928" y1="832" y2="1104" x1="928" />
            <wire x2="1008" y1="1104" y2="1104" x1="928" />
            <wire x2="1552" y1="832" y2="832" x1="928" />
            <wire x2="1552" y1="832" y2="1360" x1="1552" />
            <wire x2="1552" y1="1360" y2="1360" x1="1488" />
        </branch>
        <branch name="XLXN_5(8:0)">
            <wire x2="1008" y1="1168" y2="1168" x1="928" />
            <wire x2="928" y1="1168" y2="1264" x1="928" />
            <wire x2="1536" y1="1264" y2="1264" x1="928" />
            <wire x2="1536" y1="1264" y2="1424" x1="1536" />
            <wire x2="1536" y1="1424" y2="1424" x1="1488" />
        </branch>
        <branch name="XLXN_6(8:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1360" type="branch" />
            <wire x2="944" y1="1280" y2="1360" x1="944" />
            <wire x2="992" y1="1360" y2="1360" x1="944" />
            <wire x2="1008" y1="1360" y2="1360" x1="992" />
            <wire x2="1520" y1="1280" y2="1280" x1="944" />
            <wire x2="1520" y1="1072" y2="1072" x1="1488" />
            <wire x2="1520" y1="1072" y2="1280" x1="1520" />
        </branch>
        <branch name="FINISHED">
            <wire x2="1760" y1="912" y2="912" x1="1488" />
        </branch>
        <branch name="PRODUCT(8:0)">
            <wire x2="1760" y1="992" y2="992" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="800" y="912" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="800" y="976" name="X(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1760" y="912" name="FINISHED" orien="R0" />
        <iomarker fontsize="28" x="1760" y="992" name="PRODUCT(8:0)" orien="R0" />
        <iomarker fontsize="28" x="800" y="1040" name="Y(8:0)" orien="R180" />
        <branch name="TEMP_OUT(8:0)">
            <wire x2="1840" y1="1232" y2="1232" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="1840" y="1232" name="TEMP_OUT(8:0)" orien="R0" />
        <branch name="TEMP(8:0)">
            <wire x2="992" y1="1296" y2="1424" x1="992" />
            <wire x2="1008" y1="1424" y2="1424" x1="992" />
            <wire x2="1504" y1="1296" y2="1296" x1="992" />
            <wire x2="1504" y1="1152" y2="1152" x1="1488" />
            <wire x2="1504" y1="1152" y2="1296" x1="1504" />
        </branch>
    </sheet>
</drawing>