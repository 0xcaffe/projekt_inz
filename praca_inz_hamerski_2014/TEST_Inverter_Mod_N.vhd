-- Vhdl test bench created from schematic D:\GIT_projekt_inz\praca_inz_hamerski_2014\Inverter_Mod_N.sch - Sun Nov 30 03:24:11 2014
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY Inverter_Mod_N_Inverter_Mod_N_sch_tb IS
END Inverter_Mod_N_Inverter_Mod_N_sch_tb;
ARCHITECTURE behavioral OF Inverter_Mod_N_Inverter_Mod_N_sch_tb IS 

   COMPONENT Inverter_Mod_N
   PORT( X	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          N	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          GO	:	IN	STD_LOGIC; 
			 ERROR_SIG : OUT STD_LOGIC;
          Y		:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0); 
			 FINISHED	:	OUT	STD_LOGIC; 
          CLK	:	IN	STD_LOGIC);
   END COMPONENT;

   SIGNAL X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL N	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL GO	:	STD_LOGIC;
	SIGNAL ERROR_SIG : STD_LOGIC;
	SIGNAL FINISHED	:	STD_LOGIC;
   SIGNAL Y		:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL CLK	:	STD_LOGIC;
	constant CLK_period : time := 10 ns;


BEGIN

   UUT: Inverter_Mod_N PORT MAP(
		X => X, 
		N => N, 
		GO => GO,
		Y	=> Y, 
		ERROR_SIG => ERROR_SIG,
		FINISHED => FINISHED, 
		CLK => CLK
   );

   -- Clock process definitions
   clk50_in_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;

	   -- Stimulus process
   stim_proc: process
   begin	
		
--			valid data test		57 * 78 = 1 mod 127
			wait for CLK_period;
			X <= conv_std_logic_vector(57, 9);
			N <= conv_std_logic_vector(127, 9);				
			GO <= '1'; 
			wait for CLK_period;
			GO <= '0';			
		   wait until FINISHED = '1' or ERROR_SIG = '1';
			wait;			
	end process;

END;
