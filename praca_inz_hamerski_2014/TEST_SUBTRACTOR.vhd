--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   04:04:48 11/30/2014
-- Design Name:   
-- Module Name:   D:/GIT_projekt_inz/praca_inz_hamerski_2014/TEST_SUBTRACTOR.vhd
-- Project Name:  praca_inz_hamerski_2014
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: SUBTRACKTOR
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TEST_SUBTRACTOR IS
END TEST_SUBTRACTOR;
 
ARCHITECTURE behavior OF TEST_SUBTRACTOR IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT SUBTRACKTOR
    PORT(
         X : IN  std_logic_vector(8 downto 0);
         Y : IN  std_logic_vector(8 downto 0);
         OUTPUT : OUT  std_logic_vector(8 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal X : std_logic_vector(8 downto 0) := (others => '0');
   signal Y : std_logic_vector(8 downto 0) := (others => '0');

 	--Outputs
   signal OUTPUT : std_logic_vector(8 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: SUBTRACKTOR PORT MAP (
          X => X,
          Y => Y,
          OUTPUT => OUTPUT
        );


 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		X <= "100111000";
		Y <= "001011000";

      wait;
   end process;

END;
