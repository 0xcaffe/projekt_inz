<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="R_X(8:0)" />
        <signal name="R_Y(8:0)" />
        <signal name="XLXN_3(8:0)" />
        <signal name="XLXN_4(8:0)" />
        <signal name="XLXN_11(8:0)" />
        <signal name="XLXN_12(8:0)" />
        <signal name="XLXN_18(8:0)" />
        <signal name="XLXN_19(8:0)" />
        <signal name="XLXN_20(8:0)" />
        <signal name="XLXN_25(8:0)" />
        <signal name="XLXN_33(8:0)" />
        <signal name="XLXN_34(8:0)" />
        <signal name="XLXN_35(8:0)" />
        <signal name="XLXN_36(8:0)" />
        <signal name="XLXN_38(8:0)" />
        <signal name="XLXN_39(8:0)" />
        <signal name="XLXN_40(8:0)" />
        <signal name="XLXN_41(8:0)" />
        <signal name="XLXN_43(8:0)" />
        <signal name="XLXN_44(8:0)" />
        <signal name="XLXN_46(8:0)" />
        <signal name="GO_ADD" />
        <signal name="MUX_CONV(1:0)" />
        <signal name="MUX_MUL(1:0)" />
        <signal name="GO_MUL" />
        <signal name="P_X(8:0)" />
        <signal name="P_Y(8:0)" />
        <signal name="MUX_REG_R(1:0)" />
        <signal name="MUX_REG_Q(1:0)" />
        <signal name="LOAD_REG_Q" />
        <signal name="LOAD_REG_R" />
        <signal name="SIG_WRONG_INPUT" />
        <signal name="ADD_FINISHED" />
        <signal name="MUL_FINISHED" />
        <signal name="CLK" />
        <signal name="A(8:0)" />
        <signal name="XLXN_88(8:0)" />
        <signal name="B(8:0)" />
        <signal name="EC_ADD_ZERO_DIV" />
        <signal name="EC_MUL_ZERO_DIV" />
        <port polarity="Output" name="R_X(8:0)" />
        <port polarity="Output" name="R_Y(8:0)" />
        <port polarity="Input" name="GO_ADD" />
        <port polarity="Input" name="MUX_CONV(1:0)" />
        <port polarity="Input" name="MUX_MUL(1:0)" />
        <port polarity="Input" name="GO_MUL" />
        <port polarity="Input" name="P_X(8:0)" />
        <port polarity="Input" name="P_Y(8:0)" />
        <port polarity="Input" name="MUX_REG_R(1:0)" />
        <port polarity="Input" name="MUX_REG_Q(1:0)" />
        <port polarity="Input" name="LOAD_REG_Q" />
        <port polarity="Input" name="LOAD_REG_R" />
        <port polarity="Output" name="SIG_WRONG_INPUT" />
        <port polarity="Output" name="ADD_FINISHED" />
        <port polarity="Output" name="MUL_FINISHED" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="A(8:0)" />
        <port polarity="Input" name="B(8:0)" />
        <port polarity="Output" name="EC_ADD_ZERO_DIV" />
        <port polarity="Output" name="EC_MUL_ZERO_DIV" />
        <blockdef name="EC_ADD">
            <timestamp>2014-11-29T20:1:15</timestamp>
            <line x2="480" y1="160" y2="160" x1="416" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <line x2="480" y1="96" y2="96" x1="416" />
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-492" height="24" />
            <line x2="480" y1="-480" y2="-480" x1="416" />
            <rect width="64" x="416" y="-380" height="24" />
            <line x2="480" y1="-368" y2="-368" x1="416" />
            <rect width="64" x="416" y="-268" height="24" />
            <line x2="480" y1="-256" y2="-256" x1="416" />
            <rect width="64" x="416" y="-156" height="24" />
            <line x2="480" y1="-144" y2="-144" x1="416" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
            <rect width="352" x="64" y="-512" height="768" />
        </blockdef>
        <blockdef name="EC_POINT_DOUBLE">
            <timestamp>2014-11-29T20:4:51</timestamp>
            <line x2="480" y1="160" y2="160" x1="416" />
            <rect width="64" x="0" y="84" height="24" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-364" height="24" />
            <line x2="480" y1="-352" y2="-352" x1="416" />
            <rect width="64" x="416" y="-284" height="24" />
            <line x2="480" y1="-272" y2="-272" x1="416" />
            <rect width="64" x="416" y="-204" height="24" />
            <line x2="480" y1="-192" y2="-192" x1="416" />
            <rect width="64" x="416" y="-124" height="24" />
            <line x2="480" y1="-112" y2="-112" x1="416" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
            <rect width="352" x="64" y="-384" height="640" />
        </blockdef>
        <blockdef name="Coverter">
            <timestamp>2014-11-15T19:47:59</timestamp>
            <rect width="352" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-108" height="24" />
            <line x2="480" y1="-96" y2="-96" x1="416" />
            <rect width="64" x="416" y="-44" height="24" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
        </blockdef>
        <blockdef name="MULTIPLEXER">
            <timestamp>2014-11-16T14:39:20</timestamp>
            <rect width="336" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="400" y="-172" height="24" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
        </blockdef>
        <blockdef name="REGISTER_8bit">
            <timestamp>2014-11-16T14:12:5</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="MULTIPLEXER_3">
            <timestamp>2014-11-16T13:47:20</timestamp>
            <rect width="336" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="400" y="-236" height="24" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
        </blockdef>
        <block symbolname="EC_ADD" name="XLXI_1">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="R_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="XLXN_4(8:0)" name="Q_Y(8:0)" />
            <blockpin signalname="R_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="XLXN_3(8:0)" name="Q_X(8:0)" />
            <blockpin signalname="XLXN_33(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="XLXN_34(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="A(8:0)" name="A(8:0)" />
            <blockpin signalname="GO_ADD" name="GO" />
            <blockpin signalname="XLXN_18(8:0)" name="OUT_VALUE(8:0)" />
            <blockpin signalname="XLXN_19(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="ADD_FINISHED" name="FINISHED" />
            <blockpin signalname="XLXN_38(8:0)" name="R_Y(8:0)" />
            <blockpin signalname="XLXN_41(8:0)" name="R_X(8:0)" />
            <blockpin signalname="SIG_WRONG_INPUT" name="SIG_WRONG_INPUT" />
            <blockpin signalname="EC_ADD_ZERO_DIV" name="EC_ADD_ZERO_DIV" />
        </block>
        <block symbolname="EC_POINT_DOUBLE" name="XLXI_2">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="XLXN_35(8:0)" name="P_X(8:0)" />
            <blockpin signalname="XLXN_36(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="XLXN_33(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="XLXN_34(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="GO_MUL" name="GO" />
            <blockpin signalname="B(8:0)" name="B(8:0)" />
            <blockpin signalname="XLXN_25(8:0)" name="OUT_VALUE(8:0)" />
            <blockpin signalname="XLXN_20(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="MUL_FINISHED" name="FINISHED" />
            <blockpin signalname="XLXN_39(8:0)" name="R_X(8:0)" />
            <blockpin signalname="XLXN_40(8:0)" name="R_Y(8:0)" />
            <blockpin signalname="EC_MUL_ZERO_DIV" name="EC_MUL_ZERO_DIV" />
        </block>
        <block symbolname="Coverter" name="XLXI_3">
            <blockpin signalname="XLXN_11(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="XLXN_12(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="XLXN_33(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="XLXN_34(8:0)" name="OUT_VALUE(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_4">
            <blockpin signalname="R_Y(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_4(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_MUL(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_36(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_5">
            <blockpin signalname="R_X(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_3(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_MUL(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_35(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_6">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="LOAD_REG_R" name="LOAD" />
            <blockpin signalname="XLXN_43(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="R_X(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_7">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="LOAD_REG_R" name="LOAD" />
            <blockpin signalname="XLXN_44(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="R_Y(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_8">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="LOAD_REG_Q" name="LOAD" />
            <blockpin signalname="XLXN_88(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="XLXN_3(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_9">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="LOAD_REG_Q" name="LOAD" />
            <blockpin signalname="XLXN_46(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="XLXN_4(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_10">
            <blockpin signalname="XLXN_41(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_39(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_REG_R(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_43(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_11">
            <blockpin signalname="XLXN_38(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_40(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_REG_R(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_44(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER_3" name="XLXI_12">
            <blockpin signalname="P_X(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_41(8:0)" name="X_2(8:0)" />
            <blockpin signalname="XLXN_39(8:0)" name="X_3(8:0)" />
            <blockpin signalname="MUX_REG_Q(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_88(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER_3" name="XLXI_13">
            <blockpin signalname="P_Y(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_38(8:0)" name="X_2(8:0)" />
            <blockpin signalname="XLXN_40(8:0)" name="X_3(8:0)" />
            <blockpin signalname="MUX_REG_Q(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_46(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_14">
            <blockpin signalname="XLXN_19(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_20(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_CONV(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_11(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_15">
            <blockpin signalname="XLXN_18(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_25(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_CONV(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_12(8:0)" name="OUTPUT(8:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="976" y="832" name="XLXI_1" orien="R0">
        </instance>
        <branch name="R_X(8:0)">
            <wire x2="400" y1="544" y2="2000" x1="400" />
            <wire x2="496" y1="2000" y2="2000" x1="400" />
            <wire x2="800" y1="544" y2="544" x1="400" />
            <wire x2="976" y1="544" y2="544" x1="800" />
            <wire x2="800" y1="208" y2="544" x1="800" />
            <wire x2="3360" y1="208" y2="208" x1="800" />
            <wire x2="3360" y1="208" y2="352" x1="3360" />
            <wire x2="3360" y1="352" y2="352" x1="3312" />
            <wire x2="3360" y1="160" y2="208" x1="3360" />
        </branch>
        <branch name="R_Y(8:0)">
            <wire x2="320" y1="416" y2="2320" x1="320" />
            <wire x2="496" y1="2320" y2="2320" x1="320" />
            <wire x2="880" y1="416" y2="416" x1="320" />
            <wire x2="976" y1="416" y2="416" x1="880" />
            <wire x2="880" y1="240" y2="416" x1="880" />
            <wire x2="3440" y1="240" y2="240" x1="880" />
            <wire x2="3440" y1="240" y2="736" x1="3440" />
            <wire x2="3440" y1="736" y2="736" x1="3312" />
            <wire x2="3440" y1="160" y2="240" x1="3440" />
        </branch>
        <branch name="XLXN_3(8:0)">
            <wire x2="976" y1="608" y2="608" x1="160" />
            <wire x2="160" y1="608" y2="2064" x1="160" />
            <wire x2="496" y1="2064" y2="2064" x1="160" />
            <wire x2="160" y1="2064" y2="2640" x1="160" />
            <wire x2="3440" y1="2640" y2="2640" x1="160" />
            <wire x2="3440" y1="1328" y2="1328" x1="3344" />
            <wire x2="3440" y1="1328" y2="2640" x1="3440" />
        </branch>
        <branch name="XLXN_4(8:0)">
            <wire x2="976" y1="480" y2="480" x1="80" />
            <wire x2="80" y1="480" y2="2384" x1="80" />
            <wire x2="496" y1="2384" y2="2384" x1="80" />
            <wire x2="80" y1="2384" y2="2704" x1="80" />
            <wire x2="3360" y1="2704" y2="2704" x1="80" />
            <wire x2="3360" y1="1792" y2="1792" x1="3328" />
            <wire x2="3360" y1="1792" y2="2704" x1="3360" />
        </branch>
        <branch name="XLXN_11(8:0)">
            <wire x2="1120" y1="1152" y2="1152" x1="1104" />
            <wire x2="1120" y1="1152" y2="1264" x1="1120" />
            <wire x2="1136" y1="1264" y2="1264" x1="1120" />
        </branch>
        <branch name="XLXN_12(8:0)">
            <wire x2="1120" y1="1424" y2="1424" x1="1104" />
            <wire x2="1136" y1="1328" y2="1328" x1="1120" />
            <wire x2="1120" y1="1328" y2="1424" x1="1120" />
        </branch>
        <instance x="1136" y="1360" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_18(8:0)">
            <wire x2="240" y1="112" y2="1424" x1="240" />
            <wire x2="640" y1="1424" y2="1424" x1="240" />
            <wire x2="1680" y1="112" y2="112" x1="240" />
            <wire x2="1680" y1="112" y2="352" x1="1680" />
            <wire x2="1680" y1="352" y2="352" x1="1456" />
        </branch>
        <branch name="XLXN_19(8:0)">
            <wire x2="480" y1="48" y2="1152" x1="480" />
            <wire x2="640" y1="1152" y2="1152" x1="480" />
            <wire x2="1600" y1="48" y2="48" x1="480" />
            <wire x2="1600" y1="48" y2="464" x1="1600" />
            <wire x2="1600" y1="464" y2="464" x1="1456" />
        </branch>
        <branch name="XLXN_20(8:0)">
            <wire x2="480" y1="1216" y2="1760" x1="480" />
            <wire x2="1760" y1="1760" y2="1760" x1="480" />
            <wire x2="1760" y1="1760" y2="2080" x1="1760" />
            <wire x2="624" y1="1216" y2="1216" x1="480" />
            <wire x2="640" y1="1216" y2="1216" x1="624" />
            <wire x2="1760" y1="2080" y2="2080" x1="1616" />
        </branch>
        <instance x="640" y="1584" name="XLXI_15" orien="R0">
        </instance>
        <branch name="XLXN_25(8:0)">
            <wire x2="560" y1="1488" y2="1680" x1="560" />
            <wire x2="1680" y1="1680" y2="1680" x1="560" />
            <wire x2="1680" y1="1680" y2="2000" x1="1680" />
            <wire x2="624" y1="1488" y2="1488" x1="560" />
            <wire x2="640" y1="1488" y2="1488" x1="624" />
            <wire x2="1680" y1="2000" y2="2000" x1="1616" />
        </branch>
        <instance x="640" y="1312" name="XLXI_14" orien="R0">
        </instance>
        <instance x="1136" y="2352" name="XLXI_2" orien="R0">
        </instance>
        <instance x="496" y="2160" name="XLXI_5" orien="R0">
        </instance>
        <instance x="496" y="2480" name="XLXI_4" orien="R0">
        </instance>
        <branch name="XLXN_33(8:0)">
            <wire x2="640" y1="144" y2="864" x1="640" />
            <wire x2="976" y1="864" y2="864" x1="640" />
            <wire x2="1760" y1="144" y2="144" x1="640" />
            <wire x2="1760" y1="144" y2="1264" x1="1760" />
            <wire x2="1760" y1="1264" y2="1600" x1="1760" />
            <wire x2="1088" y1="1600" y2="2192" x1="1088" />
            <wire x2="1136" y1="2192" y2="2192" x1="1088" />
            <wire x2="1760" y1="1600" y2="1600" x1="1088" />
            <wire x2="1760" y1="1264" y2="1264" x1="1616" />
        </branch>
        <branch name="XLXN_34(8:0)">
            <wire x2="720" y1="80" y2="736" x1="720" />
            <wire x2="976" y1="736" y2="736" x1="720" />
            <wire x2="1840" y1="80" y2="80" x1="720" />
            <wire x2="1840" y1="80" y2="1328" x1="1840" />
            <wire x2="1840" y1="1328" y2="1840" x1="1840" />
            <wire x2="1840" y1="1840" y2="1840" x1="1040" />
            <wire x2="1040" y1="1840" y2="2320" x1="1040" />
            <wire x2="1136" y1="2320" y2="2320" x1="1040" />
            <wire x2="1840" y1="1328" y2="1328" x1="1616" />
        </branch>
        <branch name="XLXN_35(8:0)">
            <wire x2="976" y1="2000" y2="2000" x1="960" />
            <wire x2="992" y1="2000" y2="2000" x1="976" />
            <wire x2="992" y1="2000" y2="2064" x1="992" />
            <wire x2="1136" y1="2064" y2="2064" x1="992" />
        </branch>
        <branch name="XLXN_36(8:0)">
            <wire x2="976" y1="2320" y2="2320" x1="960" />
            <wire x2="992" y1="2320" y2="2320" x1="976" />
            <wire x2="992" y1="2128" y2="2320" x1="992" />
            <wire x2="1136" y1="2128" y2="2128" x1="992" />
        </branch>
        <instance x="2928" y="576" name="XLXI_6" orien="R0">
        </instance>
        <instance x="2928" y="960" name="XLXI_7" orien="R0">
        </instance>
        <instance x="2320" y="592" name="XLXI_10" orien="R0">
        </instance>
        <instance x="2320" y="912" name="XLXI_11" orien="R0">
        </instance>
        <instance x="2960" y="1552" name="XLXI_8" orien="R0">
        </instance>
        <instance x="2944" y="2016" name="XLXI_9" orien="R0">
        </instance>
        <instance x="2336" y="1536" name="XLXI_12" orien="R0">
        </instance>
        <instance x="2352" y="2016" name="XLXI_13" orien="R0">
        </instance>
        <branch name="XLXN_39(8:0)">
            <wire x2="2000" y1="2160" y2="2160" x1="1616" />
            <wire x2="2240" y1="2160" y2="2160" x1="2000" />
            <wire x2="2320" y1="496" y2="496" x1="2000" />
            <wire x2="2000" y1="496" y2="2160" x1="2000" />
            <wire x2="2336" y1="1440" y2="1440" x1="2240" />
            <wire x2="2240" y1="1440" y2="2160" x1="2240" />
        </branch>
        <branch name="XLXN_40(8:0)">
            <wire x2="2160" y1="2240" y2="2240" x1="1616" />
            <wire x2="2320" y1="816" y2="816" x1="2160" />
            <wire x2="2160" y1="816" y2="1920" x1="2160" />
            <wire x2="2160" y1="1920" y2="2240" x1="2160" />
            <wire x2="2352" y1="1920" y2="1920" x1="2160" />
        </branch>
        <branch name="XLXN_41(8:0)">
            <wire x2="1920" y1="576" y2="576" x1="1456" />
            <wire x2="1920" y1="576" y2="1376" x1="1920" />
            <wire x2="2336" y1="1376" y2="1376" x1="1920" />
            <wire x2="2320" y1="432" y2="432" x1="1920" />
            <wire x2="1920" y1="432" y2="576" x1="1920" />
        </branch>
        <branch name="XLXN_38(8:0)">
            <wire x2="2080" y1="688" y2="688" x1="1456" />
            <wire x2="2080" y1="688" y2="1856" x1="2080" />
            <wire x2="2352" y1="1856" y2="1856" x1="2080" />
            <wire x2="2160" y1="688" y2="688" x1="2080" />
            <wire x2="2160" y1="688" y2="752" x1="2160" />
            <wire x2="2320" y1="752" y2="752" x1="2160" />
        </branch>
        <branch name="XLXN_43(8:0)">
            <wire x2="2832" y1="432" y2="432" x1="2784" />
            <wire x2="2832" y1="432" y2="544" x1="2832" />
            <wire x2="2928" y1="544" y2="544" x1="2832" />
        </branch>
        <branch name="XLXN_44(8:0)">
            <wire x2="2832" y1="752" y2="752" x1="2784" />
            <wire x2="2832" y1="752" y2="928" x1="2832" />
            <wire x2="2928" y1="928" y2="928" x1="2832" />
        </branch>
        <branch name="XLXN_46(8:0)">
            <wire x2="2848" y1="1792" y2="1792" x1="2816" />
            <wire x2="2848" y1="1792" y2="1984" x1="2848" />
            <wire x2="2928" y1="1984" y2="1984" x1="2848" />
            <wire x2="2944" y1="1984" y2="1984" x1="2928" />
        </branch>
        <branch name="GO_ADD">
            <wire x2="976" y1="928" y2="928" x1="944" />
        </branch>
        <iomarker fontsize="28" x="944" y="928" name="GO_ADD" orien="R180" />
        <branch name="MUX_CONV(1:0)">
            <wire x2="592" y1="1280" y2="1552" x1="592" />
            <wire x2="640" y1="1552" y2="1552" x1="592" />
            <wire x2="592" y1="1280" y2="1632" x1="592" />
            <wire x2="704" y1="1632" y2="1632" x1="592" />
            <wire x2="640" y1="1280" y2="1280" x1="592" />
        </branch>
        <iomarker fontsize="28" x="704" y="1632" name="MUX_CONV(1:0)" orien="R0" />
        <branch name="MUX_MUL(1:0)">
            <wire x2="400" y1="2128" y2="2448" x1="400" />
            <wire x2="400" y1="2448" y2="2528" x1="400" />
            <wire x2="480" y1="2528" y2="2528" x1="400" />
            <wire x2="496" y1="2448" y2="2448" x1="400" />
            <wire x2="496" y1="2128" y2="2128" x1="400" />
        </branch>
        <iomarker fontsize="28" x="480" y="2528" name="MUX_MUL(1:0)" orien="R0" />
        <branch name="GO_MUL">
            <wire x2="960" y1="2384" y2="2528" x1="960" />
            <wire x2="1136" y1="2528" y2="2528" x1="960" />
            <wire x2="1152" y1="2528" y2="2528" x1="1136" />
            <wire x2="1136" y1="2384" y2="2384" x1="960" />
        </branch>
        <branch name="P_X(8:0)">
            <wire x2="2240" y1="1296" y2="1312" x1="2240" />
            <wire x2="2336" y1="1312" y2="1312" x1="2240" />
        </branch>
        <branch name="P_Y(8:0)">
            <wire x2="1920" y1="1776" y2="1792" x1="1920" />
            <wire x2="2352" y1="1792" y2="1792" x1="1920" />
        </branch>
        <iomarker fontsize="28" x="2240" y="1296" name="P_X(8:0)" orien="R270" />
        <branch name="MUX_REG_R(1:0)">
            <wire x2="2320" y1="560" y2="560" x1="2240" />
            <wire x2="2240" y1="560" y2="880" x1="2240" />
            <wire x2="2240" y1="880" y2="960" x1="2240" />
            <wire x2="2400" y1="960" y2="960" x1="2240" />
            <wire x2="2320" y1="880" y2="880" x1="2240" />
        </branch>
        <iomarker fontsize="28" x="2400" y="960" name="MUX_REG_R(1:0)" orien="R0" />
        <iomarker fontsize="28" x="1920" y="1776" name="P_Y(8:0)" orien="R270" />
        <branch name="MUX_REG_Q(1:0)">
            <wire x2="2336" y1="1504" y2="1504" x1="2320" />
            <wire x2="2320" y1="1504" y2="1984" x1="2320" />
            <wire x2="2320" y1="1984" y2="2096" x1="2320" />
            <wire x2="2336" y1="2096" y2="2096" x1="2320" />
            <wire x2="2352" y1="1984" y2="1984" x1="2320" />
        </branch>
        <iomarker fontsize="28" x="2336" y="2096" name="MUX_REG_Q(1:0)" orien="R0" />
        <branch name="LOAD_REG_Q">
            <wire x2="2912" y1="1456" y2="1616" x1="2912" />
            <wire x2="2912" y1="1616" y2="1920" x1="2912" />
            <wire x2="2944" y1="1920" y2="1920" x1="2912" />
            <wire x2="3024" y1="1616" y2="1616" x1="2912" />
            <wire x2="2960" y1="1456" y2="1456" x1="2912" />
        </branch>
        <iomarker fontsize="28" x="3024" y="1616" name="LOAD_REG_Q" orien="R0" />
        <branch name="LOAD_REG_R">
            <wire x2="2928" y1="480" y2="480" x1="2912" />
            <wire x2="2912" y1="480" y2="864" x1="2912" />
            <wire x2="2928" y1="864" y2="864" x1="2912" />
            <wire x2="2912" y1="864" y2="1040" x1="2912" />
            <wire x2="2992" y1="1040" y2="1040" x1="2912" />
        </branch>
        <iomarker fontsize="28" x="2992" y="1040" name="LOAD_REG_R" orien="R0" />
        <branch name="ADD_FINISHED">
            <wire x2="1472" y1="800" y2="800" x1="1456" />
        </branch>
        <branch name="MUL_FINISHED">
            <wire x2="1632" y1="2320" y2="2320" x1="1616" />
            <wire x2="1648" y1="2320" y2="2320" x1="1632" />
        </branch>
        <iomarker fontsize="28" x="1648" y="2320" name="MUL_FINISHED" orien="R0" />
        <branch name="SIG_WRONG_INPUT">
            <wire x2="1712" y1="928" y2="928" x1="1456" />
            <wire x2="1712" y1="928" y2="944" x1="1712" />
        </branch>
        <iomarker fontsize="28" x="3360" y="160" name="R_X(8:0)" orien="R270" />
        <iomarker fontsize="28" x="3440" y="160" name="R_Y(8:0)" orien="R270" />
        <branch name="CLK">
            <wire x2="944" y1="176" y2="352" x1="944" />
            <wire x2="976" y1="352" y2="352" x1="944" />
            <wire x2="1888" y1="176" y2="176" x1="944" />
            <wire x2="1888" y1="176" y2="1808" x1="1888" />
            <wire x2="2880" y1="176" y2="176" x1="1888" />
            <wire x2="2880" y1="176" y2="352" x1="2880" />
            <wire x2="2928" y1="352" y2="352" x1="2880" />
            <wire x2="2880" y1="352" y2="736" x1="2880" />
            <wire x2="2928" y1="736" y2="736" x1="2880" />
            <wire x2="2880" y1="736" y2="1328" x1="2880" />
            <wire x2="2960" y1="1328" y2="1328" x1="2880" />
            <wire x2="2880" y1="1328" y2="1792" x1="2880" />
            <wire x2="2944" y1="1792" y2="1792" x1="2880" />
            <wire x2="1888" y1="1808" y2="1808" x1="1056" />
            <wire x2="1056" y1="1808" y2="2000" x1="1056" />
            <wire x2="1136" y1="2000" y2="2000" x1="1056" />
            <wire x2="2880" y1="128" y2="176" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2880" y="128" name="CLK" orien="R270" />
        <iomarker fontsize="28" x="1152" y="2528" name="GO_MUL" orien="R0" />
        <branch name="XLXN_88(8:0)">
            <wire x2="2832" y1="1312" y2="1312" x1="2800" />
            <wire x2="2832" y1="1312" y2="1520" x1="2832" />
            <wire x2="2960" y1="1520" y2="1520" x1="2832" />
        </branch>
        <branch name="A(8:0)">
            <wire x2="976" y1="800" y2="800" x1="928" />
        </branch>
        <iomarker fontsize="28" x="928" y="800" name="A(8:0)" orien="R180" />
        <branch name="B(8:0)">
            <wire x2="1120" y1="2448" y2="2448" x1="1104" />
            <wire x2="1136" y1="2448" y2="2448" x1="1120" />
        </branch>
        <iomarker fontsize="28" x="1104" y="2448" name="B(8:0)" orien="R180" />
        <branch name="EC_ADD_ZERO_DIV">
            <wire x2="1472" y1="1120" y2="1120" x1="1424" />
            <wire x2="1472" y1="992" y2="992" x1="1456" />
            <wire x2="1472" y1="992" y2="1120" x1="1472" />
        </branch>
        <iomarker fontsize="28" x="1472" y="800" name="ADD_FINISHED" orien="R0" />
        <iomarker fontsize="28" x="1712" y="944" name="SIG_WRONG_INPUT" orien="R90" />
        <iomarker fontsize="28" x="1424" y="1120" name="EC_ADD_ZERO_DIV" orien="R180" />
        <branch name="EC_MUL_ZERO_DIV">
            <wire x2="1632" y1="2512" y2="2512" x1="1616" />
            <wire x2="1648" y1="2512" y2="2512" x1="1632" />
        </branch>
        <iomarker fontsize="28" x="1648" y="2512" name="EC_MUL_ZERO_DIV" orien="R0" />
    </sheet>
</drawing>