<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(8:0)" />
        <signal name="XLXN_2(8:0)" />
        <signal name="XLXN_3(8:0)" />
        <signal name="XLXN_4(8:0)" />
        <signal name="XLXN_6(8:0)" />
        <signal name="XLXN_7(8:0)" />
        <signal name="XLXN_8(8:0)" />
        <signal name="XLXN_11(8:0)" />
        <signal name="XLXN_12(8:0)" />
        <signal name="XLXN_15(8:0)" />
        <signal name="XLXN_16(8:0)" />
        <signal name="ONE(8:0)" />
        <signal name="Y_1(8:0)" />
        <signal name="ZERO(8:0)" />
        <signal name="Y_2(8:0)" />
        <signal name="Y_GT_0" />
        <signal name="Y_EQ_0" />
        <signal name="Y1_GT_Y2" />
        <signal name="LOAD_REG_Y" />
        <signal name="Y(8:0)" />
        <signal name="LOAD_VEC_1" />
        <signal name="LOAD_VEC_2" />
        <signal name="MUX_SET_1(1:0)" />
        <signal name="CLK" />
        <signal name="STATE(8:0)">
        </signal>
        <port polarity="Input" name="ONE(8:0)" />
        <port polarity="Input" name="Y_1(8:0)" />
        <port polarity="Input" name="ZERO(8:0)" />
        <port polarity="Input" name="Y_2(8:0)" />
        <port polarity="Output" name="Y_GT_0" />
        <port polarity="Output" name="Y_EQ_0" />
        <port polarity="Output" name="Y1_GT_Y2" />
        <port polarity="Input" name="LOAD_REG_Y" />
        <port polarity="Output" name="Y(8:0)" />
        <port polarity="Input" name="LOAD_VEC_1" />
        <port polarity="Input" name="LOAD_VEC_2" />
        <port polarity="Input" name="MUX_SET_1(1:0)" />
        <port polarity="Input" name="CLK" />
        <blockdef name="REGISTER_8bit">
            <timestamp>2014-11-16T14:12:5</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="MULTIPLEXER">
            <timestamp>2014-11-16T14:39:20</timestamp>
            <rect width="336" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="400" y="-172" height="24" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
        </blockdef>
        <blockdef name="COMPARATOR">
            <timestamp>2014-11-30T1:29:9</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="SUBTRACKTOR">
            <timestamp>2014-11-30T1:37:54</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="REGISTER_8bit" name="XLXI_1">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="LOAD_VEC_1" name="LOAD" />
            <blockpin signalname="XLXN_1(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="STATE(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_2">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="LOAD_VEC_1" name="LOAD" />
            <blockpin signalname="XLXN_2(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="XLXN_7(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_3">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="LOAD_VEC_2" name="LOAD" />
            <blockpin signalname="XLXN_3(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="XLXN_6(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_4">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="LOAD_VEC_2" name="LOAD" />
            <blockpin signalname="XLXN_4(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="XLXN_8(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_7">
            <blockpin signalname="ZERO(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_16(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_SET_1(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_3(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_8">
            <blockpin signalname="Y_2(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_15(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_SET_1(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_4(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="COMPARATOR" name="XLXI_13">
            <blockpin signalname="XLXN_8(8:0)" name="X(8:0)" />
            <blockpin signalname="ZERO(8:0)" name="Y(8:0)" />
            <blockpin signalname="Y_GT_0" name="X_GREATER" />
            <blockpin signalname="Y_EQ_0" name="EQUAL" />
        </block>
        <block symbolname="COMPARATOR" name="XLXI_14">
            <blockpin signalname="XLXN_7(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_8(8:0)" name="Y(8:0)" />
            <blockpin signalname="Y1_GT_Y2" name="X_GREATER" />
            <blockpin name="EQUAL" />
        </block>
        <block symbolname="SUBTRACKTOR" name="XLXI_19">
            <blockpin signalname="STATE(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_6(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_11(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="SUBTRACKTOR" name="XLXI_20">
            <blockpin signalname="XLXN_7(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_8(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_12(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="SUBTRACKTOR" name="XLXI_21">
            <blockpin signalname="XLXN_6(8:0)" name="X(8:0)" />
            <blockpin signalname="STATE(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_16(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="SUBTRACKTOR" name="XLXI_22">
            <blockpin signalname="XLXN_8(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_7(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_15(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_23">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="LOAD_REG_Y" name="LOAD" />
            <blockpin signalname="STATE(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="Y(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_6">
            <blockpin signalname="Y_1(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_12(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_SET_1(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_2(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_5">
            <blockpin signalname="ONE(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_11(8:0)" name="X_2(8:0)" />
            <blockpin signalname="MUX_SET_1(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="XLXN_1(8:0)" name="OUTPUT(8:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="896" y="576" name="XLXI_1" orien="R0">
        </instance>
        <instance x="896" y="976" name="XLXI_2" orien="R0">
        </instance>
        <instance x="896" y="1776" name="XLXI_3" orien="R0">
        </instance>
        <instance x="240" y="1712" name="XLXI_7" orien="R0">
        </instance>
        <instance x="896" y="2176" name="XLXI_4" orien="R0">
        </instance>
        <instance x="240" y="2112" name="XLXI_8" orien="R0">
        </instance>
        <branch name="XLXN_1(8:0)">
            <wire x2="800" y1="352" y2="352" x1="704" />
            <wire x2="800" y1="352" y2="544" x1="800" />
            <wire x2="896" y1="544" y2="544" x1="800" />
        </branch>
        <branch name="XLXN_2(8:0)">
            <wire x2="800" y1="752" y2="752" x1="704" />
            <wire x2="800" y1="752" y2="944" x1="800" />
            <wire x2="896" y1="944" y2="944" x1="800" />
        </branch>
        <branch name="XLXN_3(8:0)">
            <wire x2="800" y1="1552" y2="1552" x1="704" />
            <wire x2="800" y1="1552" y2="1744" x1="800" />
            <wire x2="896" y1="1744" y2="1744" x1="800" />
        </branch>
        <branch name="XLXN_4(8:0)">
            <wire x2="800" y1="1952" y2="1952" x1="704" />
            <wire x2="800" y1="1952" y2="2144" x1="800" />
            <wire x2="896" y1="2144" y2="2144" x1="800" />
        </branch>
        <instance x="2496" y="1088" name="XLXI_13" orien="R0">
        </instance>
        <branch name="XLXN_6(8:0)">
            <wire x2="1520" y1="1552" y2="1552" x1="1280" />
            <wire x2="1776" y1="1552" y2="1552" x1="1520" />
            <wire x2="1520" y1="416" y2="1552" x1="1520" />
            <wire x2="1776" y1="416" y2="416" x1="1520" />
        </branch>
        <branch name="XLXN_7(8:0)">
            <wire x2="1360" y1="752" y2="752" x1="1280" />
            <wire x2="1776" y1="752" y2="752" x1="1360" />
            <wire x2="1360" y1="752" y2="1232" x1="1360" />
            <wire x2="1360" y1="1232" y2="2016" x1="1360" />
            <wire x2="1776" y1="2016" y2="2016" x1="1360" />
            <wire x2="2496" y1="1232" y2="1232" x1="1360" />
        </branch>
        <branch name="XLXN_8(8:0)">
            <wire x2="1600" y1="1952" y2="1952" x1="1280" />
            <wire x2="1776" y1="1952" y2="1952" x1="1600" />
            <wire x2="1776" y1="816" y2="816" x1="1600" />
            <wire x2="1600" y1="816" y2="992" x1="1600" />
            <wire x2="2496" y1="992" y2="992" x1="1600" />
            <wire x2="1600" y1="992" y2="1296" x1="1600" />
            <wire x2="1600" y1="1296" y2="1952" x1="1600" />
            <wire x2="2496" y1="1296" y2="1296" x1="1600" />
        </branch>
        <instance x="1776" y="448" name="XLXI_19" orien="R0">
        </instance>
        <instance x="1776" y="848" name="XLXI_20" orien="R0">
        </instance>
        <instance x="1776" y="1648" name="XLXI_21" orien="R0">
        </instance>
        <instance x="1776" y="2048" name="XLXI_22" orien="R0">
        </instance>
        <instance x="2496" y="640" name="XLXI_23" orien="R0">
        </instance>
        <instance x="240" y="912" name="XLXI_6" orien="R0">
        </instance>
        <instance x="240" y="512" name="XLXI_5" orien="R0">
        </instance>
        <branch name="XLXN_11(8:0)">
            <wire x2="2224" y1="80" y2="80" x1="80" />
            <wire x2="2224" y1="80" y2="352" x1="2224" />
            <wire x2="80" y1="80" y2="416" x1="80" />
            <wire x2="240" y1="416" y2="416" x1="80" />
            <wire x2="2224" y1="352" y2="352" x1="2160" />
        </branch>
        <branch name="XLXN_12(8:0)">
            <wire x2="240" y1="816" y2="816" x1="80" />
            <wire x2="80" y1="816" y2="1120" x1="80" />
            <wire x2="2240" y1="1120" y2="1120" x1="80" />
            <wire x2="2240" y1="752" y2="752" x1="2160" />
            <wire x2="2240" y1="752" y2="1120" x1="2240" />
        </branch>
        <branch name="XLXN_15(8:0)">
            <wire x2="240" y1="2016" y2="2016" x1="160" />
            <wire x2="160" y1="2016" y2="2240" x1="160" />
            <wire x2="2240" y1="2240" y2="2240" x1="160" />
            <wire x2="2240" y1="1952" y2="1952" x1="2160" />
            <wire x2="2240" y1="1952" y2="2240" x1="2240" />
        </branch>
        <branch name="XLXN_16(8:0)">
            <wire x2="240" y1="1616" y2="1616" x1="160" />
            <wire x2="160" y1="1616" y2="1808" x1="160" />
            <wire x2="2240" y1="1808" y2="1808" x1="160" />
            <wire x2="2240" y1="1552" y2="1552" x1="2160" />
            <wire x2="2240" y1="1552" y2="1808" x1="2240" />
        </branch>
        <branch name="ONE(8:0)">
            <wire x2="176" y1="336" y2="352" x1="176" />
            <wire x2="240" y1="352" y2="352" x1="176" />
        </branch>
        <branch name="Y_1(8:0)">
            <wire x2="240" y1="752" y2="752" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="752" name="Y_1(8:0)" orien="R180" />
        <branch name="ZERO(8:0)">
            <wire x2="80" y1="1520" y2="1552" x1="80" />
            <wire x2="144" y1="1552" y2="1552" x1="80" />
            <wire x2="240" y1="1552" y2="1552" x1="144" />
            <wire x2="2496" y1="1056" y2="1056" x1="144" />
            <wire x2="144" y1="1056" y2="1552" x1="144" />
        </branch>
        <branch name="Y_2(8:0)">
            <wire x2="240" y1="1952" y2="1952" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="1952" name="Y_2(8:0)" orien="R180" />
        <iomarker fontsize="28" x="176" y="336" name="ONE(8:0)" orien="R270" />
        <branch name="Y_GT_0">
            <wire x2="2912" y1="992" y2="992" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2912" y="992" name="Y_GT_0" orien="R0" />
        <branch name="Y_EQ_0">
            <wire x2="2912" y1="1056" y2="1056" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2912" y="1056" name="Y_EQ_0" orien="R0" />
        <branch name="Y1_GT_Y2">
            <wire x2="2912" y1="1232" y2="1232" x1="2880" />
        </branch>
        <iomarker fontsize="28" x="2912" y="1232" name="Y1_GT_Y2" orien="R0" />
        <branch name="LOAD_REG_Y">
            <wire x2="2496" y1="544" y2="544" x1="2320" />
        </branch>
        <branch name="Y(8:0)">
            <wire x2="2944" y1="416" y2="416" x1="2880" />
        </branch>
        <branch name="LOAD_VEC_1">
            <wire x2="848" y1="608" y2="608" x1="800" />
            <wire x2="848" y1="608" y2="880" x1="848" />
            <wire x2="896" y1="880" y2="880" x1="848" />
            <wire x2="848" y1="480" y2="608" x1="848" />
            <wire x2="896" y1="480" y2="480" x1="848" />
        </branch>
        <iomarker fontsize="28" x="800" y="608" name="LOAD_VEC_1" orien="R180" />
        <branch name="LOAD_VEC_2">
            <wire x2="832" y1="1776" y2="1776" x1="736" />
            <wire x2="832" y1="1776" y2="2080" x1="832" />
            <wire x2="896" y1="2080" y2="2080" x1="832" />
            <wire x2="832" y1="1680" y2="1776" x1="832" />
            <wire x2="896" y1="1680" y2="1680" x1="832" />
        </branch>
        <branch name="MUX_SET_1(1:0)">
            <wire x2="240" y1="480" y2="480" x1="32" />
            <wire x2="32" y1="480" y2="880" x1="32" />
            <wire x2="32" y1="880" y2="1328" x1="32" />
            <wire x2="32" y1="1328" y2="1680" x1="32" />
            <wire x2="32" y1="1680" y2="2080" x1="32" />
            <wire x2="240" y1="2080" y2="2080" x1="32" />
            <wire x2="240" y1="1680" y2="1680" x1="32" />
            <wire x2="192" y1="1328" y2="1328" x1="32" />
            <wire x2="240" y1="880" y2="880" x1="32" />
        </branch>
        <iomarker fontsize="28" x="80" y="1520" name="ZERO(8:0)" orien="R270" />
        <iomarker fontsize="28" x="192" y="1328" name="MUX_SET_1(1:0)" orien="R0" />
        <branch name="CLK">
            <wire x2="2384" y1="160" y2="160" x1="816" />
            <wire x2="2384" y1="160" y2="416" x1="2384" />
            <wire x2="2384" y1="416" y2="1184" x1="2384" />
            <wire x2="2496" y1="416" y2="416" x1="2384" />
            <wire x2="816" y1="160" y2="352" x1="816" />
            <wire x2="896" y1="352" y2="352" x1="816" />
            <wire x2="864" y1="752" y2="1184" x1="864" />
            <wire x2="864" y1="1184" y2="1552" x1="864" />
            <wire x2="864" y1="1552" y2="1952" x1="864" />
            <wire x2="896" y1="1952" y2="1952" x1="864" />
            <wire x2="896" y1="1552" y2="1552" x1="864" />
            <wire x2="2384" y1="1184" y2="1184" x1="864" />
            <wire x2="896" y1="752" y2="752" x1="864" />
            <wire x2="2384" y1="128" y2="160" x1="2384" />
        </branch>
        <iomarker fontsize="28" x="2384" y="128" name="CLK" orien="R270" />
        <iomarker fontsize="28" x="2320" y="544" name="LOAD_REG_Y" orien="R180" />
        <iomarker fontsize="28" x="736" y="1776" name="LOAD_VEC_2" orien="R180" />
        <iomarker fontsize="28" x="2944" y="416" name="Y(8:0)" orien="R0" />
        <instance x="2496" y="1328" name="XLXI_14" orien="R0">
        </instance>
        <branch name="STATE(8:0)">
            <wire x2="1440" y1="352" y2="352" x1="1280" />
            <wire x2="1776" y1="352" y2="352" x1="1440" />
            <wire x2="1440" y1="352" y2="608" x1="1440" />
            <wire x2="1440" y1="608" y2="1616" x1="1440" />
            <wire x2="1776" y1="1616" y2="1616" x1="1440" />
            <wire x2="2496" y1="608" y2="608" x1="1440" />
        </branch>
    </sheet>
</drawing>