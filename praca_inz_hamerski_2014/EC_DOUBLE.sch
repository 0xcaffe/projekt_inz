<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4(2:0)" />
        <signal name="XLXN_5(2:0)" />
        <signal name="XLXN_6(1:0)" />
        <signal name="XLXN_7(1:0)" />
        <signal name="XLXN_8(1:0)" />
        <signal name="XLXN_9(1:0)" />
        <signal name="FINISHED" />
        <signal name="P_Y(8:0)" />
        <signal name="P_X(8:0)" />
        <signal name="IN_LOG(8:0)" />
        <signal name="IN_VALUE(8:0)" />
        <signal name="CLK" />
        <signal name="OUT_VALUE(8:0)" />
        <signal name="OUT_LOG(8:0)" />
        <signal name="R_X(8:0)" />
        <signal name="R_Y(8:0)" />
        <signal name="A(8:0)" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Input" name="P_Y(8:0)" />
        <port polarity="Input" name="P_X(8:0)" />
        <port polarity="Input" name="IN_LOG(8:0)" />
        <port polarity="Input" name="IN_VALUE(8:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Output" name="OUT_VALUE(8:0)" />
        <port polarity="Output" name="OUT_LOG(8:0)" />
        <port polarity="Output" name="R_X(8:0)" />
        <port polarity="Output" name="R_Y(8:0)" />
        <port polarity="Input" name="A(8:0)" />
        <blockdef name="EC_DOUBLE_Datapath">
            <timestamp>2014-11-18T1:18:29</timestamp>
            <rect width="448" x="64" y="-960" height="960" />
            <rect width="64" x="0" y="-940" height="24" />
            <line x2="0" y1="-928" y2="-928" x1="64" />
            <rect width="64" x="0" y="-876" height="24" />
            <line x2="0" y1="-864" y2="-864" x1="64" />
            <rect width="64" x="0" y="-812" height="24" />
            <line x2="0" y1="-800" y2="-800" x1="64" />
            <rect width="64" x="0" y="-748" height="24" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <rect width="64" x="0" y="-684" height="24" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <rect width="64" x="0" y="-620" height="24" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="512" y="-940" height="24" />
            <line x2="576" y1="-928" y2="-928" x1="512" />
            <rect width="64" x="512" y="-652" height="24" />
            <line x2="576" y1="-640" y2="-640" x1="512" />
            <rect width="64" x="512" y="-364" height="24" />
            <line x2="576" y1="-352" y2="-352" x1="512" />
            <rect width="64" x="512" y="-76" height="24" />
            <line x2="576" y1="-64" y2="-64" x1="512" />
        </blockdef>
        <blockdef name="EC_DOUBLE_Control_Unit">
            <timestamp>2014-11-18T1:18:36</timestamp>
            <rect width="336" x="64" y="-640" height="640" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="464" y1="-608" y2="-608" x1="400" />
            <line x2="464" y1="-544" y2="-544" x1="400" />
            <line x2="464" y1="-480" y2="-480" x1="400" />
            <line x2="464" y1="-416" y2="-416" x1="400" />
            <rect width="64" x="400" y="-364" height="24" />
            <line x2="464" y1="-352" y2="-352" x1="400" />
            <rect width="64" x="400" y="-300" height="24" />
            <line x2="464" y1="-288" y2="-288" x1="400" />
            <rect width="64" x="400" y="-236" height="24" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
            <rect width="64" x="400" y="-172" height="24" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
            <rect width="64" x="400" y="-108" height="24" />
            <line x2="464" y1="-96" y2="-96" x1="400" />
            <rect width="64" x="400" y="-44" height="24" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
        </blockdef>
        <block symbolname="EC_DOUBLE_Datapath" name="XLXI_1">
            <blockpin signalname="P_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="P_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="XLXN_4(2:0)" name="MUX_LOG_CTRL(2:0)" />
            <blockpin signalname="IN_LOG(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="XLXN_5(2:0)" name="DEMUX_LOG_CTRL(2:0)" />
            <blockpin signalname="XLXN_8(1:0)" name="MUX_REG_CTRL(1:0)" />
            <blockpin signalname="XLXN_1" name="REG_LOAD" />
            <blockpin signalname="XLXN_6(1:0)" name="MUX_VAL_CTRL(1:0)" />
            <blockpin signalname="IN_VALUE(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="XLXN_7(1:0)" name="DEMUX_VAL_CTRL(1:0)" />
            <blockpin signalname="A(8:0)" name="A(8:0)" />
            <blockpin signalname="XLXN_9(1:0)" name="MUX_REGX_CTRL(1:0)" />
            <blockpin signalname="XLXN_2" name="REGX_LOAD" />
            <blockpin signalname="XLXN_3" name="REGY_LOAD" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="OUT_VALUE(8:0)" name="OUT_VALUE(8:0)" />
            <blockpin signalname="OUT_LOG(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="R_X(8:0)" name="R_X(8:0)" />
            <blockpin signalname="R_Y(8:0)" name="R_Y(8:0)" />
        </block>
        <block symbolname="EC_DOUBLE_Control_Unit" name="XLXI_2">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="XLXN_1" name="REG_LOAD" />
            <blockpin signalname="XLXN_2" name="REGX_LOAD" />
            <blockpin signalname="XLXN_3" name="REGY_LOAD" />
            <blockpin signalname="FINISHED" name="FINISHED" />
            <blockpin signalname="XLXN_4(2:0)" name="MUX_LOG_CTRL(2:0)" />
            <blockpin signalname="XLXN_5(2:0)" name="DEMUX_LOG_CTRL(2:0)" />
            <blockpin signalname="XLXN_6(1:0)" name="MUX_VAL_CTRL(1:0)" />
            <blockpin signalname="XLXN_7(1:0)" name="DEMUX_VAL_CTRL(1:0)" />
            <blockpin signalname="XLXN_8(1:0)" name="MUX_REG_CTRL(1:0)" />
            <blockpin signalname="XLXN_9(1:0)" name="MUX_REGX_CTRL(1:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="2016" y="2864" name="XLXI_2" orien="R0">
        </instance>
        <instance x="3200" y="3024" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_1">
            <wire x2="2944" y1="2256" y2="2256" x1="2480" />
            <wire x2="2944" y1="2256" y2="2480" x1="2944" />
            <wire x2="3200" y1="2480" y2="2480" x1="2944" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="2912" y1="2320" y2="2320" x1="2480" />
            <wire x2="2912" y1="2320" y2="2864" x1="2912" />
            <wire x2="3200" y1="2864" y2="2864" x1="2912" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="2880" y1="2384" y2="2384" x1="2480" />
            <wire x2="2880" y1="2384" y2="2928" x1="2880" />
            <wire x2="3200" y1="2928" y2="2928" x1="2880" />
        </branch>
        <branch name="XLXN_4(2:0)">
            <wire x2="2784" y1="2512" y2="2512" x1="2480" />
            <wire x2="3200" y1="2224" y2="2224" x1="2784" />
            <wire x2="2784" y1="2224" y2="2512" x1="2784" />
        </branch>
        <branch name="XLXN_5(2:0)">
            <wire x2="3136" y1="2576" y2="2576" x1="2480" />
            <wire x2="3200" y1="2352" y2="2352" x1="3136" />
            <wire x2="3136" y1="2352" y2="2576" x1="3136" />
        </branch>
        <branch name="XLXN_6(1:0)">
            <wire x2="2848" y1="2640" y2="2640" x1="2480" />
            <wire x2="3200" y1="2544" y2="2544" x1="2848" />
            <wire x2="2848" y1="2544" y2="2640" x1="2848" />
        </branch>
        <branch name="XLXN_7(1:0)">
            <wire x2="3136" y1="2704" y2="2704" x1="2480" />
            <wire x2="3200" y1="2672" y2="2672" x1="3136" />
            <wire x2="3136" y1="2672" y2="2704" x1="3136" />
        </branch>
        <branch name="XLXN_8(1:0)">
            <wire x2="2816" y1="2768" y2="2768" x1="2480" />
            <wire x2="2816" y1="2416" y2="2768" x1="2816" />
            <wire x2="3200" y1="2416" y2="2416" x1="2816" />
        </branch>
        <branch name="XLXN_9(1:0)">
            <wire x2="3136" y1="2832" y2="2832" x1="2480" />
            <wire x2="3200" y1="2800" y2="2800" x1="3136" />
            <wire x2="3136" y1="2800" y2="2832" x1="3136" />
        </branch>
        <branch name="FINISHED">
            <wire x2="2528" y1="2448" y2="2448" x1="2480" />
        </branch>
        <iomarker fontsize="28" x="2528" y="2448" name="FINISHED" orien="R0" />
        <branch name="P_Y(8:0)">
            <wire x2="3200" y1="2160" y2="2160" x1="3120" />
        </branch>
        <branch name="P_X(8:0)">
            <wire x2="3200" y1="2096" y2="2096" x1="3120" />
        </branch>
        <branch name="IN_LOG(8:0)">
            <wire x2="3200" y1="2288" y2="2288" x1="3184" />
        </branch>
        <branch name="IN_VALUE(8:0)">
            <wire x2="3200" y1="2608" y2="2608" x1="3184" />
        </branch>
        <branch name="CLK">
            <wire x2="2000" y1="2256" y2="2256" x1="1968" />
            <wire x2="2016" y1="2256" y2="2256" x1="2000" />
            <wire x2="2000" y1="2256" y2="2992" x1="2000" />
            <wire x2="3200" y1="2992" y2="2992" x1="2000" />
        </branch>
        <iomarker fontsize="28" x="1968" y="2256" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="3120" y="2096" name="P_X(8:0)" orien="R180" />
        <iomarker fontsize="28" x="3120" y="2160" name="P_Y(8:0)" orien="R180" />
        <iomarker fontsize="28" x="3184" y="2608" name="IN_VALUE(8:0)" orien="R180" />
        <iomarker fontsize="28" x="3184" y="2288" name="IN_LOG(8:0)" orien="R180" />
        <branch name="OUT_VALUE(8:0)">
            <wire x2="3808" y1="2096" y2="2096" x1="3776" />
        </branch>
        <iomarker fontsize="28" x="3808" y="2096" name="OUT_VALUE(8:0)" orien="R0" />
        <branch name="OUT_LOG(8:0)">
            <wire x2="3792" y1="2384" y2="2384" x1="3776" />
            <wire x2="3824" y1="2384" y2="2384" x1="3792" />
        </branch>
        <branch name="R_X(8:0)">
            <wire x2="3808" y1="2672" y2="2672" x1="3776" />
        </branch>
        <iomarker fontsize="28" x="3808" y="2672" name="R_X(8:0)" orien="R0" />
        <branch name="R_Y(8:0)">
            <wire x2="3792" y1="2960" y2="2960" x1="3776" />
            <wire x2="3856" y1="2960" y2="2960" x1="3792" />
        </branch>
        <branch name="A(8:0)">
            <wire x2="3200" y1="2736" y2="2736" x1="3168" />
        </branch>
        <iomarker fontsize="28" x="3168" y="2736" name="A(8:0)" orien="R180" />
        <iomarker fontsize="28" x="3824" y="2384" name="OUT_LOG(8:0)" orien="R0" />
        <iomarker fontsize="28" x="3856" y="2960" name="R_Y(8:0)" orien="R0" />
    </sheet>
</drawing>