----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:38:23 11/16/2014 
-- Design Name: 
-- Module Name:    MULTIPLEXER_3 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MULTIPLEXER_3 is
	port( X_1 : in std_logic_vector(8 downto 0);
			X_2 : in std_logic_vector(8 downto 0);
			X_3 : in std_logic_vector(8 downto 0);
			CONTROLL : in std_logic_vector(1 downto 0);
			OUTPUT : out std_logic_vector(8 downto 0));
end MULTIPLEXER_3;

architecture Behavioral of MULTIPLEXER_3 is

begin
	process(X_1,X_2,X_3,CONTROLL)
	begin
		if(CONTROLL = "01") then
			OUTPUT <= X_1;
		elsif(CONTROLL = "10") then
			OUTPUT <= X_2;
		elsif(CONTROLL = "11") then
			OUTPUT <= X_3;
		else
			OUTPUT <= "000000000";
		end if;
	end process;

end Behavioral;

