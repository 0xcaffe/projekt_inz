----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:14:01 11/15/2014 
-- Design Name: 
-- Module Name:    GF_Div_Log - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity GF_Div_Log is
	port( X	 : in std_logic_vector(8 downto 0);
			Y	 : in std_logic_vector(8 downto 0);			
			PRODUCT : out std_logic_vector(8 downto 0));
end GF_Div_Log;

architecture Behavioral of GF_Div_Log is

	signal temp : std_logic_vector(8 downto 0);	
	
begin

	process(X,Y)
	begin
		temp <= std_logic_vector(signed(X)-signed(Y));
	end process;

	process(temp)
	begin
		if(signed(temp)<0) then
			PRODUCT <= std_logic_vector(unsigned(temp) + 255);
		else			
			PRODUCT <= temp;
		end if;
	end process;
end Behavioral;

