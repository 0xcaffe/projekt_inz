-- Vhdl test bench created from schematic D:\inzynierka\praca_inz_hamerski_2014\Coverter.sch - Sat Nov 15 16:01:47 2014
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY Coverter_Coverter_sch_tb IS
END Coverter_Coverter_sch_tb;
ARCHITECTURE behavioral OF Coverter_Coverter_sch_tb IS 

   COMPONENT Coverter
   PORT( IN_LOG	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          IN_VALUE	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          OUT_LOG	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          OUT_VALUE	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0));
   END COMPONENT;

   SIGNAL IN_LOG	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL IN_VALUE	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL OUT_LOG	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL OUT_VALUE	:	STD_LOGIC_VECTOR (8 DOWNTO 0);

BEGIN

   UUT: Coverter PORT MAP(
		IN_LOG => IN_LOG, 
		IN_VALUE => IN_VALUE, 
		OUT_LOG => OUT_LOG, 
		OUT_VALUE => OUT_VALUE
   );


   -- Stimulus process
   stim_proc: process 
   begin		
      -- hold reset state for 100 ns.

      IN_LOG <= conv_std_logic_vector(248, 9);
		IN_VALUE <= conv_std_logic_vector(28, 9);

      wait;
   end process;

END;
