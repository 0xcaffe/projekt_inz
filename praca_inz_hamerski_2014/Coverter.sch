<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_10(7:0)" />
        <signal name="XLXN_11(7:0)" />
        <signal name="XLXN_12(7:0)" />
        <signal name="XLXN_13(7:0)" />
        <signal name="IN_LOG(8:0)" />
        <signal name="IN_VALUE(8:0)" />
        <signal name="OUT_LOG(8:0)" />
        <signal name="OUT_VALUE(8:0)" />
        <port polarity="Input" name="IN_LOG(8:0)" />
        <port polarity="Input" name="IN_VALUE(8:0)" />
        <port polarity="Output" name="OUT_LOG(8:0)" />
        <port polarity="Output" name="OUT_VALUE(8:0)" />
        <blockdef name="ROM_VALUES">
            <timestamp>2014-11-15T14:34:38</timestamp>
            <rect width="224" x="32" y="32" height="512" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="256" y1="80" y2="80" style="linewidth:W" x1="288" />
        </blockdef>
        <blockdef name="ROM_ADDRESSES">
            <timestamp>2014-11-15T15:28:8</timestamp>
            <rect width="224" x="32" y="32" height="512" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="256" y1="80" y2="80" style="linewidth:W" x1="288" />
        </blockdef>
        <blockdef name="Bus_Reducer">
            <timestamp>2014-11-15T14:55:5</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Bus_Extender">
            <timestamp>2014-11-15T14:58:13</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="ROM_VALUES" name="XLXI_1">
            <blockpin signalname="XLXN_11(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_12(7:0)" name="spo(7:0)" />
        </block>
        <block symbolname="ROM_ADDRESSES" name="XLXI_2">
            <blockpin signalname="XLXN_10(7:0)" name="a(7:0)" />
            <blockpin signalname="XLXN_13(7:0)" name="spo(7:0)" />
        </block>
        <block symbolname="Bus_Reducer" name="XLXI_3">
            <blockpin signalname="IN_VALUE(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="XLXN_10(7:0)" name="OUTPUT(7:0)" />
        </block>
        <block symbolname="Bus_Reducer" name="XLXI_4">
            <blockpin signalname="IN_LOG(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="XLXN_11(7:0)" name="OUTPUT(7:0)" />
        </block>
        <block symbolname="Bus_Extender" name="XLXI_5">
            <blockpin signalname="XLXN_12(7:0)" name="INPUT(7:0)" />
            <blockpin signalname="OUT_VALUE(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="Bus_Extender" name="XLXI_6">
            <blockpin signalname="XLXN_13(7:0)" name="INPUT(7:0)" />
            <blockpin signalname="OUT_LOG(8:0)" name="OUTPUT(8:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1616" y="480" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1616" y="1040" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1136" y="1152" name="XLXI_3" orien="R0">
        </instance>
        <instance x="1136" y="592" name="XLXI_4" orien="R0">
        </instance>
        <instance x="2000" y="592" name="XLXI_5" orien="R0">
        </instance>
        <instance x="2000" y="1152" name="XLXI_6" orien="R0">
        </instance>
        <branch name="XLXN_10(7:0)">
            <wire x2="1616" y1="1120" y2="1120" x1="1520" />
        </branch>
        <branch name="XLXN_11(7:0)">
            <wire x2="1616" y1="560" y2="560" x1="1520" />
        </branch>
        <branch name="XLXN_12(7:0)">
            <wire x2="2000" y1="560" y2="560" x1="1904" />
        </branch>
        <branch name="XLXN_13(7:0)">
            <wire x2="2000" y1="1120" y2="1120" x1="1904" />
        </branch>
        <branch name="IN_LOG(8:0)">
            <wire x2="1136" y1="560" y2="560" x1="1040" />
        </branch>
        <branch name="IN_VALUE(8:0)">
            <wire x2="1136" y1="1120" y2="1120" x1="1040" />
        </branch>
        <branch name="OUT_LOG(8:0)">
            <wire x2="2480" y1="1120" y2="1120" x1="2384" />
        </branch>
        <branch name="OUT_VALUE(8:0)">
            <wire x2="2480" y1="560" y2="560" x1="2384" />
        </branch>
        <iomarker fontsize="28" x="1040" y="560" name="IN_LOG(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1040" y="1120" name="IN_VALUE(8:0)" orien="R180" />
        <iomarker fontsize="28" x="2480" y="560" name="OUT_VALUE(8:0)" orien="R0" />
        <iomarker fontsize="28" x="2480" y="1120" name="OUT_LOG(8:0)" orien="R0" />
    </sheet>
</drawing>