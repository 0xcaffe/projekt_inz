----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:21:29 11/15/2014 
-- Design Name: 
-- Module Name:    GF_Add_Log - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.numeric_std.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity GF_Add_Log is
	port( X		 	 : in std_logic_vector(8 downto 0);
			Y			 : in std_logic_vector(8 downto 0);	
			IN_LOG	 : in std_logic_vector(8 downto 0);	
			IN_VALUE  : in std_logic_vector(8 downto 0);				
			PRODUCT 	 : out std_logic_vector(8 downto 0);
			OUT_LOG   : out std_logic_vector(8 downto 0);
			OUT_VALUE : out std_logic_vector(8 downto 0);	
			CLK		 : in  std_logic;
			FINISHED	 : out std_logic);					
end GF_Add_Log;

architecture Behavioral of GF_Add_Log is

	type state_type is (init, convertX, convertY, execute, convertP);
	signal present_state, next_state: state_type;

	signal temp : std_logic_vector(8 downto 0);		

begin

process(CLK)
begin
	if(rising_edge(CLK)) then
		present_state <= next_state;
	end if;
end process;

process(present_state)
begin
	case present_state is 
	
				when init =>
						next_state <= convertX;
						
				when convertX =>
						next_state <= convertY;
							
				when convertY =>
						next_state <= execute;
						
				when execute =>						
						next_state <= convertP;
						
				when convertP =>
						next_state <= convertP;		
				
				when others =>
						null;
	end case;
end process;


process(present_state)
begin
	
	FINISHED <= '0';
	
	case present_state is 
				
			when convertX => 
				OUT_LOG <= X;	
				
			when convertY =>		
				temp <= IN_VALUE;
				OUT_LOG <= Y;			
				
			when execute =>
				OUT_VALUE <= temp xor IN_VALUE;		
				
			when convertP =>
				PRODUCT <= IN_LOG;
				FINISHED <= '1';	
						
			when others =>
				null;
				
	end case;	
end process;	

end Behavioral;

