----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:25:52 11/26/2014 
-- Design Name: 
-- Module Name:    EC_SCALAR_MUL_Control_Unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EC_SCALAR_MUL_Control_Unit is
	port( CLK 		     : in std_logic;
			GO 		     : in std_logic;
			k				  : in std_logic_vector (8 downto 0);
			ADD_FINISHED  : in std_logic;
			MUL_FINISHED  : in std_logic;
			FINISHED	     : out std_logic;
			GO_ADD	     : out std_logic;
			GO_MUL	     : out std_logic;
			LOAD_REG_R    : out std_logic;
			LOAD_REG_Q    : out std_logic;
			MUX_CONV		  : out std_logic_vector (1 downto 0);
			MUX_MUL		  : out std_logic_vector (1 downto 0);
			MUX_REG_R	  : out std_logic_vector (1 downto 0);
			MUX_REG_Q	  : out std_logic_vector (1 downto 0));
end EC_SCALAR_MUL_Control_Unit;

architecture Behavioral of EC_SCALAR_MUL_Control_Unit is

type state_type is (q0, q1, q2, q3, q4, q5, q6, q7, q8, q9);
signal present_state, next_state: state_type;

signal counter	: std_logic_vector(8 downto 0);

begin

process(CLK) 
begin
	if(rising_edge(CLK)) then
		present_state <= next_state;
	end if;
end process;

process(present_state, counter, k, GO, ADD_FINISHED, MUL_FINISHED)
begin

	case present_state is

				when q0 =>
						if(GO = '1') then
							next_state <= q1;
						else
							next_state <= q0;
						end if;	
					
				when q1 =>
						next_state <= q2;						
				
				when q2 =>
						if(counter = "000000000") then 
							next_state <= q9;
						else
							next_state <= q3;
						end if; 
																		
				when q3 =>
						if(ADD_FINISHED = '0') then
							next_state <= q3;
						else
							if((k and counter) = "000000000") then
								next_state <= q4;
							else
								next_state <= q5;
							end if;
						end if;
						
				when q4 =>
						next_state <= q6;
				
				when q5 =>
						next_state <= q6;						
				
				when q6 =>
						if(MUL_FINISHED = '0') then
							next_state <= q6;
						else
							if((k and counter) = "000000000") then
								next_state <= q7;
							else
								next_state <= q8;
							end if;
						end if;
						
				when q7 =>
						next_state <= q2;
						
				when q8 =>
						next_state <= q2;				
						
				when q9 =>
						next_state <= q0;
						
				when others =>
						null;
	end case;
end process;


process(present_state)
begin

	GO_ADD		<= '0';
	GO_MUL		<= '0';
	LOAD_REG_R	<= '0';
	LOAD_REG_Q	<= '0';	
	FINISHED 	<= '0';
	
	case present_state is 	
			
			when q1 =>
				MUX_REG_R	<= "00";
				LOAD_REG_R 	<= '1';
				MUX_REG_Q 	<= "01";
				LOAD_REG_Q 	<= '1';
				counter 		<= "010000000";

			when q2 =>
				MUX_CONV 	<= "01";
				GO_ADD 		<= '1';
	
			when q4 =>
				MUX_REG_Q 	<= "10";
				LOAD_REG_Q 	<= '1';
				MUX_MUL 		<= "01";
				MUX_CONV 	<= "10";
				GO_MUL 		<= '1';				

			when q5 =>
				MUX_REG_R 	<= "01";
				LOAD_REG_R 	<= '1';
				MUX_MUL 		<= "10";
				MUX_CONV 	<= "10";
				GO_MUL 		<= '1';		
				
			when q7 =>
				MUX_REG_R 	<= "10";
				LOAD_REG_R 	<= '1';				
				counter 		<= '0' & counter(8 downto 1);

			when q8 => 				
				MUX_REG_Q	<= "11";
				LOAD_REG_Q 	<= '1';				
				counter 		<= '0' & counter(8 downto 1);

			when q9 => 	
				FINISHED 	<= '1';
				
			when others =>
					null;
	end case;	
	
end process;
end Behavioral;

