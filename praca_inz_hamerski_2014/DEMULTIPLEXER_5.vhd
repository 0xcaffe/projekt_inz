----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:03:18 11/29/2014 
-- Design Name: 
-- Module Name:    DEMULTIPLEXER_5 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DEMULTIPLEXER_5 is
	port( X_1 : out std_logic_vector(8 downto 0);
			X_2 : out std_logic_vector(8 downto 0);
			X_3 : out std_logic_vector(8 downto 0);
			X_4 : out std_logic_vector(8 downto 0);
			X_5 : out std_logic_vector(8 downto 0);
			CONTROLL : in std_logic_vector(2 downto 0);
			INPUT : in std_logic_vector(8 downto 0));
end DEMULTIPLEXER_5;

architecture Behavioral of DEMULTIPLEXER_5 is

begin
	process(INPUT,CONTROLL)
	begin
		if(CONTROLL = "001") then
			X_1 <= INPUT;
			X_2 <= "000000000";
			X_3 <= "000000000";
			X_4 <= "000000000";
			X_5 <= "000000000";
		elsif(CONTROLL = "010") then			
			X_1 <= "000000000";
			X_2 <= INPUT;
			X_3 <= "000000000";			
			X_4 <= "000000000";
			X_5 <= "000000000";
		elsif(CONTROLL = "011") then			
			X_1 <= "000000000";
			X_2 <= "000000000";
			X_3 <= INPUT;
			X_4 <= "000000000";
			X_5 <= "000000000";
		elsif(CONTROLL = "100") then			
			X_1 <= "000000000";
			X_2 <= "000000000";
			X_3 <= "000000000";
			X_4 <= INPUT;
			X_5 <= "000000000";
		elsif(CONTROLL = "101") then
			X_1 <= "000000000";
			X_2 <= "000000000";
			X_3 <= "000000000";
			X_4 <= "000000000";
			X_5 <= INPUT;
		else
			X_1 <= "000000000";
			X_2 <= "000000000";
			X_3 <= "000000000";
			X_4 <= "000000000";
			X_5 <= "000000000";
		end if;
	end process;
end Behavioral;

