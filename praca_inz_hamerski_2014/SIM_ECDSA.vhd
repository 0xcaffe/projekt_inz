-- Vhdl test bench created from schematic D:\GIT_projekt_inz\praca_inz_hamerski_2014\ECDSA.sch - Sun Nov 30 18:54:45 2014
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY ECDSA_ECDSA_sch_tb IS
END ECDSA_ECDSA_sch_tb;
ARCHITECTURE behavioral OF ECDSA_ECDSA_sch_tb IS 

   COMPONENT ECDSA
   PORT( 	k	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          GO	:	IN	STD_LOGIC; 
          CLK	:	IN	STD_LOGIC; 
          dA	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          r	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          EC_ADD_ZERO_DIV	:	OUT	STD_LOGIC; 
          EC_MUL_ZERO_DIV	:	OUT	STD_LOGIC; 
          e	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          s	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          FINISHED	:	OUT	STD_LOGIC; 
          INVERTER_ERROR	:	OUT	STD_LOGIC; 
          A	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          G_Y	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          G_X	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          B	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          n	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0));
   END COMPONENT;

	
   SIGNAL EC_ADD_ZERO_DIV	:	STD_LOGIC;
   SIGNAL EC_MUL_ZERO_DIV	:	STD_LOGIC;
   SIGNAL INVERTER_ERROR	:	STD_LOGIC;
	constant CLK_period : time := 10 ns;
   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL GO	:	STD_LOGIC;
   SIGNAL G_X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL G_Y	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL A	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL B	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL k	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL e	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL dA	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL n	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL FINISHED	:	STD_LOGIC;
   SIGNAL r	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL s	:	STD_LOGIC_VECTOR (8 DOWNTO 0);


BEGIN

   UUT: ECDSA PORT MAP(
		k => k, 
		GO => GO, 
		CLK => CLK, 
		dA => dA, 
		r => r, 
		EC_ADD_ZERO_DIV => EC_ADD_ZERO_DIV, 
		EC_MUL_ZERO_DIV => EC_MUL_ZERO_DIV, 
		e => e, 
		s => s, 
		FINISHED => FINISHED, 
		INVERTER_ERROR => INVERTER_ERROR, 
		A => A, 
		G_Y => G_Y, 
		G_X => G_X, 
		B => B, 
		n => n
   );

   -- Clock process definitions
   clk50_in_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
	
	
	-- Stimulus process
   stim_proc: process
   begin	
		
			wait for CLK_period;
			G_X <= conv_std_logic_vector(82, 9);
			G_Y <= conv_std_logic_vector(244, 9);					
			A <= conv_std_logic_vector(11, 9);
			B <= conv_std_logic_vector(1, 9);
			
			e <= conv_std_logic_vector(27, 9);
			k <= conv_std_logic_vector(15, 9);
			dA <= conv_std_logic_vector(34, 9);
			n <= conv_std_logic_vector(22, 9);			
			
			GO <= '1'; 
			wait for CLK_period;
			GO <= '0';			
			
		   wait until FINISHED = '1';				
			wait;
   end process;

END;
