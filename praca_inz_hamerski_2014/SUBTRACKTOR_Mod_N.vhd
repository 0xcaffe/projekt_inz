----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:23:55 11/30/2014 
-- Design Name: 
-- Module Name:    SUBTRACKTOR_Mod_N - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SUBTRACKTOR_Mod_N is
	port( X	 : in std_logic_vector(8 downto 0);
			Y	 : in std_logic_vector(8 downto 0);
			N	 : in std_logic_vector(8 downto 0);
			PRODUCT : out std_logic_vector(8 downto 0));
end SUBTRACKTOR_Mod_N;

architecture Behavioral of SUBTRACKTOR_Mod_N is

	signal temp : std_logic_vector(8 downto 0);	
	
begin

	process(X,Y)
	begin
		temp <= std_logic_vector(signed(X)-signed(Y));
	end process;

	process(temp, N)
	begin
		if(signed(temp)<0) then
			PRODUCT <= std_logic_vector(unsigned(temp) + unsigned(N));
		else			
			PRODUCT <= temp;
		end if;
	end process;
end Behavioral;

