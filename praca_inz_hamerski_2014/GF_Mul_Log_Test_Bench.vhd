--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:30:48 11/15/2014
-- Design Name:   
-- Module Name:   D:/inzynierka/praca_inz_hamerski_2014/GF_Mul_Log_Test_Bench.vhd
-- Project Name:  praca_inz_hamerski_2014
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: GF_Mul_Log
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY GF_Mul_Log_Test_Bench IS
END GF_Mul_Log_Test_Bench;
 
ARCHITECTURE behavior OF GF_Mul_Log_Test_Bench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT GF_Mul_Log
    PORT(
         X : IN  std_logic_vector(8 downto 0);
         Y : IN  std_logic_vector(8 downto 0);
         PRODUCT : OUT  std_logic_vector(8 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal X : std_logic_vector(8 downto 0) := (others => '0');
   signal Y : std_logic_vector(8 downto 0) := (others => '0');

 	--Outputs
   signal PRODUCT : std_logic_vector(8 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: GF_Mul_Log PORT MAP (
          X => X,
          Y => Y,
          PRODUCT => PRODUCT
        );

   -- Clock process definitions

 
   -- Stimulus process
   stim_proc: process 
   begin		
      -- hold reset state for 100 ns.

      X <= conv_std_logic_vector(20, 9);
		Y <= conv_std_logic_vector(240, 9);

      wait;
   end process;

END;
