<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK" />
        <signal name="P_Y(8:0)" />
        <signal name="Q_Y(8:0)" />
        <signal name="P_X(8:0)" />
        <signal name="Q_X(8:0)" />
        <signal name="IN_VALUE(8:0)" />
        <signal name="A(8:0)" />
        <signal name="FINISHED" />
        <signal name="R_Y(8:0)" />
        <signal name="R_X(8:0)" />
        <signal name="OUT_VALUE(8:0)" />
        <signal name="TEMP1(8:0)" />
        <signal name="IN_LOG(8:0)" />
        <signal name="GO" />
        <signal name="SIG_WRONG_INPUT" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="P_Y(8:0)" />
        <port polarity="Input" name="Q_Y(8:0)" />
        <port polarity="Input" name="P_X(8:0)" />
        <port polarity="Input" name="Q_X(8:0)" />
        <port polarity="Input" name="A(8:0)" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Output" name="R_Y(8:0)" />
        <port polarity="Output" name="R_X(8:0)" />
        <port polarity="Input" name="GO" />
        <port polarity="Output" name="SIG_WRONG_INPUT" />
        <blockdef name="EC_ADD">
            <timestamp>2014-11-29T20:1:15</timestamp>
            <line x2="480" y1="160" y2="160" x1="416" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <line x2="480" y1="96" y2="96" x1="416" />
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-492" height="24" />
            <line x2="480" y1="-480" y2="-480" x1="416" />
            <rect width="64" x="416" y="-380" height="24" />
            <line x2="480" y1="-368" y2="-368" x1="416" />
            <rect width="64" x="416" y="-268" height="24" />
            <line x2="480" y1="-256" y2="-256" x1="416" />
            <rect width="64" x="416" y="-156" height="24" />
            <line x2="480" y1="-144" y2="-144" x1="416" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
            <rect width="352" x="64" y="-512" height="768" />
        </blockdef>
        <blockdef name="Coverter">
            <timestamp>2014-11-15T19:47:59</timestamp>
            <rect width="352" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-108" height="24" />
            <line x2="480" y1="-96" y2="-96" x1="416" />
            <rect width="64" x="416" y="-44" height="24" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
        </blockdef>
        <block symbolname="EC_ADD" name="XLXI_1">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="P_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="Q_Y(8:0)" name="Q_Y(8:0)" />
            <blockpin signalname="P_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="Q_X(8:0)" name="Q_X(8:0)" />
            <blockpin signalname="IN_LOG(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="IN_VALUE(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="A(8:0)" name="A(8:0)" />
            <blockpin signalname="GO" name="GO" />
            <blockpin signalname="OUT_VALUE(8:0)" name="OUT_VALUE(8:0)" />
            <blockpin signalname="TEMP1(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="FINISHED" name="FINISHED" />
            <blockpin signalname="R_Y(8:0)" name="R_Y(8:0)" />
            <blockpin signalname="R_X(8:0)" name="R_X(8:0)" />
            <blockpin signalname="SIG_WRONG_INPUT" name="SIG_WRONG_INPUT" />
            <blockpin name="EC_ADD_ZERO_DIV" />
        </block>
        <block symbolname="Coverter" name="XLXI_2">
            <blockpin signalname="TEMP1(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="OUT_VALUE(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="IN_LOG(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="IN_VALUE(8:0)" name="OUT_VALUE(8:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1376" y="1424" name="XLXI_1" orien="R0">
        </instance>
        <branch name="CLK">
            <wire x2="1376" y1="944" y2="944" x1="1280" />
        </branch>
        <branch name="P_Y(8:0)">
            <wire x2="1376" y1="1008" y2="1008" x1="1280" />
        </branch>
        <branch name="Q_Y(8:0)">
            <wire x2="1376" y1="1072" y2="1072" x1="1280" />
        </branch>
        <branch name="P_X(8:0)">
            <wire x2="1376" y1="1136" y2="1136" x1="1296" />
        </branch>
        <branch name="Q_X(8:0)">
            <wire x2="1376" y1="1200" y2="1200" x1="1296" />
        </branch>
        <branch name="A(8:0)">
            <wire x2="1376" y1="1392" y2="1392" x1="1280" />
        </branch>
        <branch name="FINISHED">
            <wire x2="1936" y1="1392" y2="1392" x1="1856" />
        </branch>
        <branch name="R_Y(8:0)">
            <wire x2="1952" y1="1280" y2="1280" x1="1856" />
        </branch>
        <branch name="R_X(8:0)">
            <wire x2="1952" y1="1168" y2="1168" x1="1856" />
        </branch>
        <iomarker fontsize="28" x="1280" y="944" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="1280" y="1008" name="P_Y(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1280" y="1072" name="Q_Y(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1296" y="1136" name="P_X(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1296" y="1200" name="Q_X(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1280" y="1392" name="A(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1952" y="1168" name="R_X(8:0)" orien="R0" />
        <iomarker fontsize="28" x="1936" y="1392" name="FINISHED" orien="R0" />
        <branch name="OUT_VALUE(8:0)">
            <wire x2="976" y1="736" y2="2160" x1="976" />
            <wire x2="1344" y1="2160" y2="2160" x1="976" />
            <wire x2="1920" y1="736" y2="736" x1="976" />
            <wire x2="1920" y1="736" y2="944" x1="1920" />
            <wire x2="1920" y1="944" y2="944" x1="1856" />
        </branch>
        <branch name="TEMP1(8:0)">
            <wire x2="1280" y1="1984" y2="2096" x1="1280" />
            <wire x2="1344" y1="2096" y2="2096" x1="1280" />
            <wire x2="2240" y1="1984" y2="1984" x1="1280" />
            <wire x2="2240" y1="1056" y2="1056" x1="1856" />
            <wire x2="2240" y1="1056" y2="1984" x1="2240" />
        </branch>
        <branch name="IN_VALUE(8:0)">
            <wire x2="2320" y1="800" y2="800" x1="1056" />
            <wire x2="2320" y1="800" y2="2160" x1="2320" />
            <wire x2="1056" y1="800" y2="1328" x1="1056" />
            <wire x2="1376" y1="1328" y2="1328" x1="1056" />
            <wire x2="2320" y1="2160" y2="2160" x1="1824" />
        </branch>
        <branch name="IN_LOG(8:0)">
            <wire x2="1152" y1="1456" y2="1696" x1="1152" />
            <wire x2="1952" y1="1696" y2="1696" x1="1152" />
            <wire x2="1952" y1="1696" y2="2096" x1="1952" />
            <wire x2="1376" y1="1456" y2="1456" x1="1152" />
            <wire x2="1952" y1="2096" y2="2096" x1="1824" />
        </branch>
        <instance x="1344" y="2192" name="XLXI_2" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1952" y="1280" name="R_Y(8:0)" orien="R0" />
        <branch name="GO">
            <wire x2="1376" y1="1520" y2="1520" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="1344" y="1520" name="GO" orien="R180" />
        <branch name="SIG_WRONG_INPUT">
            <wire x2="1888" y1="1520" y2="1520" x1="1856" />
        </branch>
        <iomarker fontsize="28" x="1888" y="1520" name="SIG_WRONG_INPUT" orien="R0" />
    </sheet>
</drawing>