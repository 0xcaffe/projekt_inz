----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:40:09 11/30/2014 
-- Design Name: 
-- Module Name:    Reduce_Mod_N - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Reduce_Mod_N is
	port( X : in std_logic_vector(8 downto 0);
			N : in std_logic_vector(8 downto 0);
			OUTPUT : out std_logic_vector(8 downto 0));
end Reduce_Mod_N;

architecture Behavioral of Reduce_Mod_N is

begin

process(X,N)
	begin
		if(unsigned(N) > 0) then
			OUTPUT <= conv_std_logic_vector((conv_integer(unsigned(X)) mod conv_integer(unsigned(N))),9);
--			OUTPUT <= "000000000";
		else
			OUTPUT <= "000000000";
		end if;
	end process;	
end Behavioral;

