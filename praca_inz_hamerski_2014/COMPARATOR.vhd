----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:28:17 11/30/2014 
-- Design Name: 
-- Module Name:    COMPARATOR - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_MISC.ALL;
use IEEE.NUMERIC_STD.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity COMPARATOR is
	port( X : in std_logic_vector(8 downto 0);
			Y : in std_logic_vector(8 downto 0);
			X_GREATER : out std_logic;
			EQUAL : out std_logic);
end COMPARATOR;

architecture Behavioral of COMPARATOR is

begin
	process(X,Y)
	begin
		EQUAL <= nor_reduce(X xor Y);
		if(unsigned(X)>unsigned(Y)) then
			X_GREATER <= '1';
		else
			X_GREATER <= '0';
		end if;
	end process;
end Behavioral;

