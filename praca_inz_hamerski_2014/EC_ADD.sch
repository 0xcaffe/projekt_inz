<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6(1:0)" />
        <signal name="XLXN_7(1:0)" />
        <signal name="XLXN_8(1:0)" />
        <signal name="XLXN_9(1:0)" />
        <signal name="CLK" />
        <signal name="P_Y(8:0)" />
        <signal name="Q_Y(8:0)" />
        <signal name="P_X(8:0)" />
        <signal name="Q_X(8:0)" />
        <signal name="IN_LOG(8:0)" />
        <signal name="IN_VALUE(8:0)" />
        <signal name="A(8:0)" />
        <signal name="OUT_VALUE(8:0)" />
        <signal name="OUT_LOG(8:0)" />
        <signal name="FINISHED" />
        <signal name="XLXN_10" />
        <signal name="GO" />
        <signal name="XLXN_22(8:0)" />
        <signal name="XLXN_23(8:0)" />
        <signal name="XLXN_24(1:0)" />
        <signal name="R_Y(8:0)" />
        <signal name="R_X(8:0)" />
        <signal name="SIG_WRONG_INPUT" />
        <signal name="EC_ADD_ZERO_DIV" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="P_Y(8:0)" />
        <port polarity="Input" name="Q_Y(8:0)" />
        <port polarity="Input" name="P_X(8:0)" />
        <port polarity="Input" name="Q_X(8:0)" />
        <port polarity="Input" name="IN_LOG(8:0)" />
        <port polarity="Input" name="IN_VALUE(8:0)" />
        <port polarity="Input" name="A(8:0)" />
        <port polarity="Output" name="OUT_VALUE(8:0)" />
        <port polarity="Output" name="OUT_LOG(8:0)" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Input" name="GO" />
        <port polarity="Output" name="R_Y(8:0)" />
        <port polarity="Output" name="R_X(8:0)" />
        <port polarity="Output" name="SIG_WRONG_INPUT" />
        <port polarity="Output" name="EC_ADD_ZERO_DIV" />
        <blockdef name="EC_ADD_Datapath">
            <timestamp>2014-11-19T2:18:35</timestamp>
            <line x2="0" y1="96" y2="96" x1="64" />
            <rect width="64" x="0" y="-1068" height="24" />
            <line x2="0" y1="-1056" y2="-1056" x1="64" />
            <rect width="64" x="0" y="-1004" height="24" />
            <line x2="0" y1="-992" y2="-992" x1="64" />
            <rect width="64" x="0" y="-940" height="24" />
            <line x2="0" y1="-928" y2="-928" x1="64" />
            <rect width="64" x="0" y="-876" height="24" />
            <line x2="0" y1="-864" y2="-864" x1="64" />
            <rect width="64" x="0" y="-812" height="24" />
            <line x2="0" y1="-800" y2="-800" x1="64" />
            <rect width="64" x="0" y="-748" height="24" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <rect width="64" x="0" y="-684" height="24" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="512" y="-1068" height="24" />
            <line x2="576" y1="-1056" y2="-1056" x1="512" />
            <rect width="64" x="512" y="-732" height="24" />
            <line x2="576" y1="-720" y2="-720" x1="512" />
            <rect width="64" x="512" y="-396" height="24" />
            <line x2="576" y1="-384" y2="-384" x1="512" />
            <rect width="64" x="512" y="-60" height="24" />
            <line x2="576" y1="-48" y2="-48" x1="512" />
            <rect width="448" x="64" y="-1088" height="1216" />
        </blockdef>
        <blockdef name="EC_ADD_Control_Unit">
            <timestamp>2014-11-21T1:32:14</timestamp>
            <line x2="0" y1="96" y2="96" x1="64" />
            <rect width="64" x="0" y="148" height="24" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="464" y1="32" y2="32" x1="400" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="464" y1="-544" y2="-544" x1="400" />
            <line x2="464" y1="-480" y2="-480" x1="400" />
            <line x2="464" y1="-416" y2="-416" x1="400" />
            <line x2="464" y1="-352" y2="-352" x1="400" />
            <line x2="464" y1="-288" y2="-288" x1="400" />
            <rect width="64" x="400" y="-236" height="24" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
            <rect width="64" x="400" y="-172" height="24" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
            <rect width="64" x="400" y="-108" height="24" />
            <line x2="464" y1="-96" y2="-96" x1="400" />
            <rect width="64" x="400" y="-44" height="24" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
            <rect width="336" x="64" y="-640" height="832" />
        </blockdef>
        <blockdef name="EC_ADD_Validator">
            <timestamp>2014-11-29T20:0:22</timestamp>
            <line x2="464" y1="32" y2="32" x1="400" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
            <rect width="64" x="400" y="-44" height="24" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
            <rect width="336" x="64" y="-256" height="320" />
        </blockdef>
        <blockdef name="MULTIPLEXER_3">
            <timestamp>2014-11-16T13:47:20</timestamp>
            <rect width="336" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="400" y="-236" height="24" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
        </blockdef>
        <block symbolname="EC_ADD_Datapath" name="XLXI_1">
            <blockpin signalname="P_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="Q_Y(8:0)" name="Q_Y(8:0)" />
            <blockpin signalname="P_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="Q_X(8:0)" name="Q_X(8:0)" />
            <blockpin signalname="XLXN_6(1:0)" name="MUX_LOG_CTRL(1:0)" />
            <blockpin signalname="XLXN_7(1:0)" name="DEMUX_LOG_CTRL(1:0)" />
            <blockpin signalname="IN_LOG(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="XLXN_3" name="REG_LOAD_S2" />
            <blockpin signalname="XLXN_2" name="REG_LOAD_S1" />
            <blockpin signalname="XLXN_9(1:0)" name="DEMUX_VAL_CTRL(1:0)" />
            <blockpin signalname="IN_VALUE(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="XLXN_8(1:0)" name="MUX_VAL_CTRL(1:0)" />
            <blockpin signalname="A(8:0)" name="A(8:0)" />
            <blockpin signalname="XLXN_4" name="REG_LOAD_R_X" />
            <blockpin signalname="XLXN_5" name="REG_LOAD_R_Y" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="XLXN_10" name="REG_LOAD_SVAL" />
            <blockpin signalname="OUT_VALUE(8:0)" name="OUT_VALUE(8:0)" />
            <blockpin signalname="OUT_LOG(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="XLXN_22(8:0)" name="R_X(8:0)" />
            <blockpin signalname="XLXN_23(8:0)" name="R_Y(8:0)" />
        </block>
        <block symbolname="EC_ADD_Control_Unit" name="XLXI_2">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="GO" name="GO" />
            <blockpin signalname="XLXN_24(1:0)" name="VALID(1:0)" />
            <blockpin signalname="XLXN_2" name="REG_LOAD_S1" />
            <blockpin signalname="XLXN_3" name="REG_LOAD_S2" />
            <blockpin signalname="XLXN_10" name="REG_LOAD_SVAL" />
            <blockpin signalname="XLXN_4" name="REG_LOAD_RX" />
            <blockpin signalname="XLXN_5" name="REG_LOAD_RY" />
            <blockpin signalname="FINISHED" name="FINISHED" />
            <blockpin signalname="XLXN_6(1:0)" name="MUX_LOG_CTRL(1:0)" />
            <blockpin signalname="XLXN_7(1:0)" name="DEMUX_LOG_CTRL(1:0)" />
            <blockpin signalname="XLXN_8(1:0)" name="MUX_VAL_CTRL(1:0)" />
            <blockpin signalname="XLXN_9(1:0)" name="DEMUX_VAL_CTRL(1:0)" />
        </block>
        <block symbolname="EC_ADD_Validator" name="XLXI_3">
            <blockpin signalname="P_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="P_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="Q_X(8:0)" name="Q_X(8:0)" />
            <blockpin signalname="Q_Y(8:0)" name="Q_Y(8:0)" />
            <blockpin signalname="SIG_WRONG_INPUT" name="SIG_WRONG_INPUT" />
            <blockpin signalname="XLXN_24(1:0)" name="OUTPUT(1:0)" />
            <blockpin signalname="EC_ADD_ZERO_DIV" name="SIG_ZERO_DIV" />
        </block>
        <block symbolname="MULTIPLEXER_3" name="XLXI_4">
            <blockpin signalname="XLXN_22(8:0)" name="X_1(8:0)" />
            <blockpin signalname="P_X(8:0)" name="X_2(8:0)" />
            <blockpin signalname="Q_X(8:0)" name="X_3(8:0)" />
            <blockpin signalname="XLXN_24(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="R_X(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER_3" name="XLXI_5">
            <blockpin signalname="XLXN_23(8:0)" name="X_1(8:0)" />
            <blockpin signalname="P_Y(8:0)" name="X_2(8:0)" />
            <blockpin signalname="Q_Y(8:0)" name="X_3(8:0)" />
            <blockpin signalname="XLXN_24(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="R_Y(8:0)" name="OUTPUT(8:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="XLXN_6(1:0)">
            <wire x2="1520" y1="1600" y2="1600" x1="816" />
            <wire x2="1968" y1="1056" y2="1056" x1="1520" />
            <wire x2="1520" y1="1056" y2="1600" x1="1520" />
        </branch>
        <branch name="XLXN_7(1:0)">
            <wire x2="1568" y1="1664" y2="1664" x1="816" />
            <wire x2="1968" y1="1120" y2="1120" x1="1568" />
            <wire x2="1568" y1="1120" y2="1664" x1="1568" />
        </branch>
        <branch name="XLXN_8(1:0)">
            <wire x2="1616" y1="1728" y2="1728" x1="816" />
            <wire x2="1968" y1="1568" y2="1568" x1="1616" />
            <wire x2="1616" y1="1568" y2="1728" x1="1616" />
        </branch>
        <branch name="XLXN_9(1:0)">
            <wire x2="1664" y1="1792" y2="1792" x1="816" />
            <wire x2="1968" y1="1440" y2="1440" x1="1664" />
            <wire x2="1664" y1="1440" y2="1792" x1="1664" />
        </branch>
        <branch name="CLK">
            <wire x2="320" y1="1216" y2="1216" x1="256" />
            <wire x2="352" y1="1216" y2="1216" x1="320" />
            <wire x2="320" y1="1216" y2="2080" x1="320" />
            <wire x2="1840" y1="2080" y2="2080" x1="320" />
            <wire x2="1840" y1="1824" y2="2080" x1="1840" />
            <wire x2="1968" y1="1824" y2="1824" x1="1840" />
        </branch>
        <branch name="P_Y(8:0)">
            <wire x2="1136" y1="800" y2="800" x1="1024" />
            <wire x2="1968" y1="800" y2="800" x1="1136" />
            <wire x2="1136" y1="800" y2="2272" x1="1136" />
            <wire x2="2752" y1="2272" y2="2272" x1="1136" />
            <wire x2="1136" y1="480" y2="800" x1="1136" />
            <wire x2="2016" y1="480" y2="480" x1="1136" />
            <wire x2="2752" y1="1792" y2="2272" x1="2752" />
            <wire x2="2832" y1="1792" y2="1792" x1="2752" />
        </branch>
        <branch name="Q_Y(8:0)">
            <wire x2="1264" y1="864" y2="864" x1="1024" />
            <wire x2="1968" y1="864" y2="864" x1="1264" />
            <wire x2="1264" y1="864" y2="2144" x1="1264" />
            <wire x2="2656" y1="2144" y2="2144" x1="1264" />
            <wire x2="1264" y1="608" y2="864" x1="1264" />
            <wire x2="2016" y1="608" y2="608" x1="1264" />
            <wire x2="2832" y1="1856" y2="1856" x1="2656" />
            <wire x2="2656" y1="1856" y2="2144" x1="2656" />
        </branch>
        <branch name="P_X(8:0)">
            <wire x2="1072" y1="928" y2="928" x1="1024" />
            <wire x2="1968" y1="928" y2="928" x1="1072" />
            <wire x2="1072" y1="928" y2="2336" x1="1072" />
            <wire x2="2800" y1="2336" y2="2336" x1="1072" />
            <wire x2="1072" y1="416" y2="928" x1="1072" />
            <wire x2="2016" y1="416" y2="416" x1="1072" />
            <wire x2="2832" y1="1456" y2="1456" x1="2800" />
            <wire x2="2800" y1="1456" y2="2336" x1="2800" />
        </branch>
        <branch name="Q_X(8:0)">
            <wire x2="1200" y1="992" y2="992" x1="1024" />
            <wire x2="1968" y1="992" y2="992" x1="1200" />
            <wire x2="1200" y1="992" y2="2208" x1="1200" />
            <wire x2="2704" y1="2208" y2="2208" x1="1200" />
            <wire x2="1200" y1="544" y2="992" x1="1200" />
            <wire x2="2016" y1="544" y2="544" x1="1200" />
            <wire x2="2832" y1="1520" y2="1520" x1="2704" />
            <wire x2="2704" y1="1520" y2="2208" x1="2704" />
        </branch>
        <branch name="IN_LOG(8:0)">
            <wire x2="1968" y1="1184" y2="1184" x1="1936" />
        </branch>
        <branch name="IN_VALUE(8:0)">
            <wire x2="1968" y1="1504" y2="1504" x1="1936" />
        </branch>
        <branch name="A(8:0)">
            <wire x2="1968" y1="1632" y2="1632" x1="1920" />
        </branch>
        <iomarker fontsize="28" x="1936" y="1184" name="IN_LOG(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1936" y="1504" name="IN_VALUE(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1920" y="1632" name="A(8:0)" orien="R180" />
        <branch name="OUT_VALUE(8:0)">
            <wire x2="2576" y1="800" y2="800" x1="2544" />
        </branch>
        <branch name="OUT_LOG(8:0)">
            <wire x2="2576" y1="1136" y2="1136" x1="2544" />
        </branch>
        <branch name="FINISHED">
            <wire x2="864" y1="1536" y2="1536" x1="816" />
        </branch>
        <iomarker fontsize="28" x="2576" y="1136" name="OUT_LOG(8:0)" orien="R0" />
        <iomarker fontsize="28" x="2576" y="800" name="OUT_VALUE(8:0)" orien="R0" />
        <branch name="GO">
            <wire x2="352" y1="1920" y2="1920" x1="240" />
        </branch>
        <instance x="2016" y="640" name="XLXI_3" orien="R0">
        </instance>
        <instance x="2832" y="1616" name="XLXI_4" orien="R0">
        </instance>
        <instance x="2832" y="1952" name="XLXI_5" orien="R0">
        </instance>
        <branch name="XLXN_10">
            <wire x2="1920" y1="1856" y2="1856" x1="816" />
            <wire x2="1920" y1="1856" y2="1952" x1="1920" />
            <wire x2="1968" y1="1952" y2="1952" x1="1920" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="1952" y1="1280" y2="1280" x1="816" />
            <wire x2="1952" y1="1280" y2="1312" x1="1952" />
            <wire x2="1968" y1="1312" y2="1312" x1="1952" />
        </branch>
        <instance x="1968" y="1856" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_22(8:0)">
            <wire x2="2576" y1="1472" y2="1472" x1="2544" />
            <wire x2="2576" y1="1392" y2="1472" x1="2576" />
            <wire x2="2832" y1="1392" y2="1392" x1="2576" />
        </branch>
        <branch name="XLXN_23(8:0)">
            <wire x2="2576" y1="1808" y2="1808" x1="2544" />
            <wire x2="2576" y1="1728" y2="1808" x1="2576" />
            <wire x2="2832" y1="1728" y2="1728" x1="2576" />
        </branch>
        <branch name="XLXN_24(1:0)">
            <wire x2="2880" y1="240" y2="240" x1="80" />
            <wire x2="2880" y1="240" y2="608" x1="2880" />
            <wire x2="80" y1="240" y2="1984" x1="80" />
            <wire x2="352" y1="1984" y2="1984" x1="80" />
            <wire x2="80" y1="1984" y2="2400" x1="80" />
            <wire x2="2608" y1="2400" y2="2400" x1="80" />
            <wire x2="2880" y1="608" y2="608" x1="2480" />
            <wire x2="2608" y1="1584" y2="1920" x1="2608" />
            <wire x2="2832" y1="1920" y2="1920" x1="2608" />
            <wire x2="2608" y1="1920" y2="2400" x1="2608" />
            <wire x2="2832" y1="1584" y2="1584" x1="2608" />
        </branch>
        <iomarker fontsize="28" x="256" y="1216" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="240" y="1920" name="GO" orien="R180" />
        <instance x="352" y="1824" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_3">
            <wire x2="1424" y1="1344" y2="1344" x1="816" />
            <wire x2="1968" y1="1248" y2="1248" x1="1424" />
            <wire x2="1424" y1="1248" y2="1344" x1="1424" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1424" y1="1408" y2="1408" x1="816" />
            <wire x2="1424" y1="1408" y2="1696" x1="1424" />
            <wire x2="1968" y1="1696" y2="1696" x1="1424" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1472" y1="1472" y2="1472" x1="816" />
            <wire x2="1472" y1="1472" y2="1760" x1="1472" />
            <wire x2="1968" y1="1760" y2="1760" x1="1472" />
        </branch>
        <iomarker fontsize="28" x="864" y="1536" name="FINISHED" orien="R0" />
        <iomarker fontsize="28" x="1024" y="800" name="P_Y(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1024" y="864" name="Q_Y(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1024" y="928" name="P_X(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1024" y="992" name="Q_X(8:0)" orien="R180" />
        <branch name="R_Y(8:0)">
            <wire x2="3328" y1="1728" y2="1728" x1="3296" />
        </branch>
        <iomarker fontsize="28" x="3328" y="1728" name="R_Y(8:0)" orien="R0" />
        <branch name="R_X(8:0)">
            <wire x2="3328" y1="1392" y2="1392" x1="3296" />
        </branch>
        <iomarker fontsize="28" x="3328" y="1392" name="R_X(8:0)" orien="R0" />
        <branch name="SIG_WRONG_INPUT">
            <wire x2="2512" y1="416" y2="416" x1="2480" />
        </branch>
        <iomarker fontsize="28" x="2512" y="416" name="SIG_WRONG_INPUT" orien="R0" />
        <branch name="EC_ADD_ZERO_DIV">
            <wire x2="2512" y1="672" y2="672" x1="2480" />
        </branch>
        <iomarker fontsize="28" x="2512" y="672" name="EC_ADD_ZERO_DIV" orien="R0" />
    </sheet>
</drawing>