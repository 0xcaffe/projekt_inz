----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:57:06 11/29/2014 
-- Design Name: 
-- Module Name:    adsf - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adsf is
	port( CLK 		     : in std_logic;
			k				  : in std_logic_vector (8 downto 0);
			a	 		     : out std_logic;
			COUNTER		  : out std_logic_vector (8 downto 0));
end adsf;

architecture Behavioral of adsf is

type state_type is (q0, q1, q2, q3,q4);
signal present_state, next_state: state_type;

signal scounter 	: std_logic_vector(8 downto 0);

begin

process(CLK) 
begin
	if(rising_edge(CLK)) then
		present_state <= next_state;
	end if;
end process;

process(present_state, scounter,k)
begin

	
	

	
	case present_state is 
					
				when q0 =>
						next_state <= q1;
				
				when q1 =>
						if(scounter = "000000000") then
							next_state <= q4;
						else
							if((k and scounter) = "000000000") then
								next_state <= q2;
							else
								next_state <= q3;
							end if;
						end if;
																		
				when q2 =>						
						next_state <= q1;						
						
				when q3 =>
						next_state <= q1;
						
				when q4 =>
						next_state <= q4;
							
				when others =>
						null;
	end case;
end process;


process(present_state)
begin

	
	a <= '0';
	COUNTER <= scounter;
	
	case present_state is 	
			
			when q0 =>
				scounter <= "010000000";
			
			when q2 =>
				a <= '0';
				scounter <= '0' & scounter(8 downto 1);	-- bitwise right shift
				
				
			when q3 =>
				a <= '1'; 
				scounter <= '0' & scounter(8 downto 1);	-- bitwise right shift	
			
			when q4 =>
				COUNTER <= "000011111";	
							
			when others =>
					null;
					
	end case;		
	
end process;

end Behavioral;

