----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:35:59 11/30/2014 
-- Design Name: 
-- Module Name:    SUBTRACKTOR - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SUBTRACKTOR is
	port( X : in std_logic_vector(8 downto 0);
			Y : in std_logic_vector(8 downto 0);
			OUTPUT : out std_logic_vector(8 downto 0));
end SUBTRACKTOR;

architecture Behavioral of SUBTRACKTOR is

begin
process(X,Y)
	begin
		OUTPUT <= std_logic_vector(unsigned(X) - unsigned(Y));
	end process;
end Behavioral;

