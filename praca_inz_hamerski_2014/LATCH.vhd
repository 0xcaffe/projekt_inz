----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:25:13 11/30/2014 
-- Design Name: 
-- Module Name:    LATCH - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LATCH is
	port(	INPUT 	: in	STD_LOGIC;
			OUTPUT 	: out	STD_LOGIC);
end LATCH;

architecture Behavioral of LATCH is

	signal set : std_logic;	

begin
	process(INPUT,set)
	begin
		if(set = '1') then	
			OUTPUT <= '1';
		elsif(INPUT = '1') then
			set <= '1';
		else
			OUTPUT <= '0';		
		end if;
	end process;
end Behavioral;

