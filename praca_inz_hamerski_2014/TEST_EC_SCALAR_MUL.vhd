-- Vhdl test bench created from schematic D:\GIT_projekt_inz\praca_inz_hamerski_2014\EC_SCALAR_MUL.sch - Thu Nov 27 02:47:07 2014
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY EC_SCALAR_MUL_EC_SCALAR_MUL_sch_tb IS
END EC_SCALAR_MUL_EC_SCALAR_MUL_sch_tb;
ARCHITECTURE behavioral OF EC_SCALAR_MUL_EC_SCALAR_MUL_sch_tb IS 

   COMPONENT EC_SCALAR_MUL
   PORT( A	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          P_Y	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          P_X	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          k	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          GO	:	IN	STD_LOGIC; 
          CLK	:	IN	STD_LOGIC; 
          SIG_WRONG_INPUT	:	OUT	STD_LOGIC; 
          R_Y	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          R_X	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          FINISHED	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL A	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL P_Y	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL P_X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL k	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL GO	:	STD_LOGIC;
   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL SIG_WRONG_INPUT	:	STD_LOGIC;
   SIGNAL R_Y	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL R_X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL FINISHED	:	STD_LOGIC;

BEGIN

   UUT: EC_SCALAR_MUL PORT MAP(
		A => A, 
		P_Y => P_Y, 
		P_X => P_X, 
		k => k, 
		GO => GO, 
		CLK => CLK, 
		SIG_WRONG_INPUT => SIG_WRONG_INPUT, 
		R_Y => R_Y, 
		R_X => R_X, 
		FINISHED => FINISHED
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
