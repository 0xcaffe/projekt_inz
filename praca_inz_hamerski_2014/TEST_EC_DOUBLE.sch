<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="P_X(8:0)" />
        <signal name="CLK" />
        <signal name="A(8:0)" />
        <signal name="FINISHED" />
        <signal name="R_X(8:0)" />
        <signal name="R_Y(8:0)" />
        <signal name="XLXN_12(8:0)" />
        <signal name="P_Y(8:0)" />
        <signal name="XLXN_15(8:0)" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <port polarity="Input" name="P_X(8:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="A(8:0)" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Output" name="R_X(8:0)" />
        <port polarity="Output" name="R_Y(8:0)" />
        <port polarity="Input" name="P_Y(8:0)" />
        <blockdef name="Coverter">
            <timestamp>2014-11-15T19:47:59</timestamp>
            <rect width="352" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-108" height="24" />
            <line x2="480" y1="-96" y2="-96" x1="416" />
            <rect width="64" x="416" y="-44" height="24" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
        </blockdef>
        <block symbolname="Coverter" name="XLXI_2">
            <blockpin name="IN_LOG(8:0)" />
            <blockpin signalname="XLXN_15(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="XLXN_12(8:0)" name="OUT_LOG(8:0)" />
            <blockpin name="OUT_VALUE(8:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="2512" y="3152" name="XLXI_2" orien="R0">
        </instance>
        <branch name="P_X(8:0)">
            <wire x2="2608" y1="2576" y2="2576" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2576" y="2576" name="P_X(8:0)" orien="R180" />
        <branch name="CLK">
            <wire x2="2608" y1="2768" y2="2768" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2576" y="2768" name="CLK" orien="R180" />
        <branch name="A(8:0)">
            <wire x2="2608" y1="2832" y2="2832" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2576" y="2832" name="A(8:0)" orien="R180" />
        <branch name="FINISHED">
            <wire x2="2896" y1="2512" y2="2512" x1="2864" />
        </branch>
        <iomarker fontsize="28" x="2896" y="2512" name="FINISHED" orien="R0" />
        <branch name="R_X(8:0)">
            <wire x2="2896" y1="2704" y2="2704" x1="2864" />
        </branch>
        <iomarker fontsize="28" x="2896" y="2704" name="R_X(8:0)" orien="R0" />
        <branch name="R_Y(8:0)">
            <wire x2="2896" y1="2768" y2="2768" x1="2864" />
        </branch>
        <iomarker fontsize="28" x="2896" y="2768" name="R_Y(8:0)" orien="R0" />
        <branch name="XLXN_12(8:0)">
            <wire x2="2592" y1="2368" y2="2640" x1="2592" />
            <wire x2="2608" y1="2640" y2="2640" x1="2592" />
            <wire x2="3152" y1="2368" y2="2368" x1="2592" />
            <wire x2="3152" y1="2368" y2="3056" x1="3152" />
            <wire x2="3152" y1="3056" y2="3056" x1="2992" />
        </branch>
        <iomarker fontsize="28" x="2448" y="2496" name="P_Y(8:0)" orien="R180" />
        <branch name="P_Y(8:0)">
            <wire x2="2528" y1="2496" y2="2496" x1="2448" />
            <wire x2="2528" y1="2496" y2="2512" x1="2528" />
            <wire x2="2608" y1="2512" y2="2512" x1="2528" />
            <wire x2="2624" y1="2512" y2="2512" x1="2608" />
            <wire x2="3312" y1="2432" y2="2432" x1="2608" />
            <wire x2="2608" y1="2432" y2="2512" x1="2608" />
            <wire x2="2624" y1="2464" y2="2512" x1="2624" />
            <wire x2="3312" y1="2464" y2="2464" x1="2624" />
        </branch>
        <branch name="XLXN_15(8:0)">
            <wire x2="2432" y1="2832" y2="2832" x1="2320" />
            <wire x2="2432" y1="2832" y2="2895" x1="2432" />
            <wire x2="2432" y1="2895" y2="2896" x1="2432" />
            <wire x2="2432" y1="2896" y2="2959" x1="2432" />
            <wire x2="2432" y1="2959" y2="2960" x1="2432" />
            <wire x2="2432" y1="2960" y2="3120" x1="2432" />
            <wire x2="2512" y1="3120" y2="3120" x1="2432" />
            <wire x2="2400" y1="2928" y2="2928" x1="2384" />
            <wire x2="2400" y1="2992" y2="2992" x1="2384" />
            <wire x2="2400" y1="2896" y2="2928" x1="2400" />
            <wire x2="2432" y1="2896" y2="2896" x1="2400" />
            <wire x2="2400" y1="2960" y2="2992" x1="2400" />
            <wire x2="2432" y1="2960" y2="2960" x1="2400" />
        </branch>
    </sheet>
</drawing>