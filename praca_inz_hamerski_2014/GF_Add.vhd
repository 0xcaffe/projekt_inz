----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:34:05 11/16/2014 
-- Design Name: 
-- Module Name:    GF_Add - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity GF_Add is
	port( X	 : in std_logic_vector(8 downto 0);
			Y	 : in std_logic_vector(8 downto 0);			
			PRODUCT : out std_logic_vector(8 downto 0));
end GF_Add;

architecture Behavioral of GF_Add is

begin
	process(X,Y)
	begin
		PRODUCT <= X xor Y;
	end process;

end Behavioral;

