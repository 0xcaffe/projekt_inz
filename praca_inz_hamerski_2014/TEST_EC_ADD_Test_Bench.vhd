-- Vhdl test bench created from schematic D:\inzynierka\praca_inz_hamerski_2014\TEST_EC_ADD.sch - Sun Nov 16 21:44:57 2014
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY TEST_EC_ADD_TEST_EC_ADD_sch_tb IS
END TEST_EC_ADD_TEST_EC_ADD_sch_tb;
ARCHITECTURE behavioral OF TEST_EC_ADD_TEST_EC_ADD_sch_tb IS 

   COMPONENT TEST_EC_ADD
   PORT( CLK	:	IN	STD_LOGIC; 
          P_Y	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          Q_Y	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          P_X	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          Q_X	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          A	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
			 GO : IN STD_LOGIC;
			 SIG_WRONG_INPUT	:	OUT	STD_LOGIC; 
          FINISHED	:	OUT	STD_LOGIC; 
          R_Y	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          R_X	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0)); 
   END COMPONENT;

   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL P_Y	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL Q_Y	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL P_X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL Q_X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL A	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
	SIGNAL GO	:	STD_LOGIC;
	SIGNAL SIG_WRONG_INPUT	:	STD_LOGIC; 
   SIGNAL FINISHED	:	STD_LOGIC;
   SIGNAL R_Y	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL R_X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
	constant CLK_period : time := 10 ns;

BEGIN

   UUT: TEST_EC_ADD PORT MAP(
		CLK => CLK, 
		P_Y => P_Y, 
		Q_Y => Q_Y, 
		P_X => P_X, 
		Q_X => Q_X, 
		A => A, 
		GO => GO,
		SIG_WRONG_INPUT => SIG_WRONG_INPUT,
		FINISHED => FINISHED, 
		R_Y => R_Y, 
		R_X => R_X
   );

   -- Clock process definitions
   clk50_in_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
	
	   -- Stimulus process
   stim_proc: process
   begin	
		
--			valid data test
			wait for CLK_period;
			P_X <= conv_std_logic_vector(82, 9);
			P_Y <= conv_std_logic_vector(244, 9);	
			Q_X <= conv_std_logic_vector(184, 9);
			Q_Y <= conv_std_logic_vector(225, 9);						
			A <= conv_std_logic_vector(11, 9);
			GO <= '1'; 
			wait for CLK_period;
			GO <= '0';			
		   wait until FINISHED = '1';				
			wait for CLK_period;
			
--			valid exchanged data test 
			wait for CLK_period;
			Q_X <= conv_std_logic_vector(5, 9);
			Q_Y <= conv_std_logic_vector(56, 9);	
			P_X <= conv_std_logic_vector(209, 9);
			P_Y <= conv_std_logic_vector(6, 9);						
			A <= conv_std_logic_vector(16, 9);
			GO <= '1'; 
			wait for CLK_period;
			GO <= '0';			
		   wait for 8*CLK_period;			
			
			
--			invalid input test
			wait for CLK_period;
			P_X <= conv_std_logic_vector(209, 9);
			P_Y <= conv_std_logic_vector(51, 9);	
			Q_X <= conv_std_logic_vector(209, 9);
			Q_Y <= conv_std_logic_vector(226, 9);						
			A <= conv_std_logic_vector(16, 9);
			GO <= '1'; 
			wait for CLK_period;
			GO <= '0';			
		   wait for 8*CLK_period;
			
			
--			test Q=0
			wait for CLK_period;
			Q_X <= conv_std_logic_vector(0, 9);
			Q_Y <= conv_std_logic_vector(0, 9);	
			P_X <= conv_std_logic_vector(209, 9);
			P_Y <= conv_std_logic_vector(6, 9);						
			A <= conv_std_logic_vector(16, 9);
			GO <= '1'; 
			wait for CLK_period;
			GO <= '0';			
		   wait for 8*CLK_period;
			
--			test P=0
			wait for CLK_period;
			P_X <= conv_std_logic_vector(0, 9);
			P_Y <= conv_std_logic_vector(0, 9);	
			Q_X <= conv_std_logic_vector(209, 9);
			Q_Y <= conv_std_logic_vector(6, 9);						
			A <= conv_std_logic_vector(16, 9);
			GO <= '1'; 
			wait for CLK_period;
			GO <= '0';			
		   wait for 8*CLK_period;		

--			data test
			wait for CLK_period;
			P_X <= conv_std_logic_vector(5, 9);
			P_Y <= conv_std_logic_vector(56, 9);	
			Q_X <= conv_std_logic_vector(0, 9);
			Q_Y <= conv_std_logic_vector(247, 9);						
			A <= conv_std_logic_vector(77, 9);
			GO <= '1'; 
			wait for CLK_period;
			GO <= '0';			
		   wait for 8*CLK_period;			
			
			wait for 2000 ns;	   
   end process;

END;
