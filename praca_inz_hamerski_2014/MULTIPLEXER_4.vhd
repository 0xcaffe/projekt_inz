----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:11:05 11/18/2014 
-- Design Name: 
-- Module Name:    MULTIPLEXER_4 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MULTIPLEXER_4 is
	port( X_1 : in std_logic_vector(8 downto 0);
			X_2 : in std_logic_vector(8 downto 0);
			X_3 : in std_logic_vector(8 downto 0);			
			X_4 : in std_logic_vector(8 downto 0);
			CONTROLL : in std_logic_vector(2 downto 0);
			OUTPUT : out std_logic_vector(8 downto 0));
end MULTIPLEXER_4;

architecture Behavioral of MULTIPLEXER_4 is

begin
	process(X_1,X_2,X_3,X_4,CONTROLL)
	begin
		if(CONTROLL = "001") then
			OUTPUT <= X_1;
		elsif(CONTROLL = "010") then
			OUTPUT <= X_2;
		elsif(CONTROLL = "011") then
			OUTPUT <= X_3;
		elsif(CONTROLL = "100") then
			OUTPUT <= X_4;
		else
			OUTPUT <= "000000000";
		end if;
	end process;

end Behavioral;

