----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:08:09 11/16/2014 
-- Design Name: 
-- Module Name:    REGISTER - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity REGISTER is   
	port( CLK : in std_logic;
			CLR : in std_logic;
			LOAD : in std_logic;
			INPUT : in std_logic_vector(8 downto 0);
			OUTPUT : out std_logic_vector(8 downto 0));
end REGISTER;

architecture Behavioral of REGISTER is

	signal reg : std_logic_vector(8 downto 0);	
	
begin
	process(CLK,CLR,LOAD,INPUT)
	begin
		if(rising_edge(CLK)) then
			if(CLR = '0') then
				reg <= (others => '0');
			elsif(LOAD = '1') then
				reg <= INPUT;
			end if;
			OUTPUT <= reg;
		end if;
	end process;
	
end Behavioral;

