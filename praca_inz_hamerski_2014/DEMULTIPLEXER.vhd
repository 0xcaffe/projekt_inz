----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:03:57 11/16/2014 
-- Design Name: 
-- Module Name:    DEMULTIPLEXER - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DEMULTIPLEXER is
	port( X_1 : out std_logic_vector(8 downto 0);
			X_2 : out std_logic_vector(8 downto 0);
			CONTROLL : in std_logic_vector(1 downto 0);
			INPUT : in std_logic_vector(8 downto 0));
end DEMULTIPLEXER;

architecture Behavioral of DEMULTIPLEXER is

begin
	process(INPUT,CONTROLL)
	begin
		if(CONTROLL = "01") then
			X_1 <= INPUT;
		elsif(CONTROLL = "10") then
			X_2 <= INPUT;
		end if;
	end process;

end Behavioral;

