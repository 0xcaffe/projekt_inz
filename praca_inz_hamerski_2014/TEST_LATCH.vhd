--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:33:54 11/30/2014
-- Design Name:   
-- Module Name:   D:/GIT_projekt_inz/praca_inz_hamerski_2014/TEST_LATCH.vhd
-- Project Name:  praca_inz_hamerski_2014
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: LATCH
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TEST_LATCH IS
END TEST_LATCH;
 
ARCHITECTURE behavior OF TEST_LATCH IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT LATCH
    PORT(
         INPUT : IN  std_logic;
         OUTPUT : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal INPUT : std_logic := '0';

 	--Outputs
   signal OUTPUT : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: LATCH PORT MAP (
          INPUT => INPUT,
          OUTPUT => OUTPUT
        );


 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      
		INPUT <= '0';
		wait for 100 ns;
		INPUT <= '1';
		wait for 100 ns;
		INPUT <= '0';
      wait;
   end process;

END;
