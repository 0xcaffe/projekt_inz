-- Vhdl test bench created from schematic D:\GIT_projekt_inz\praca_inz_hamerski_2014\EC_SCALAR_MUL.sch - Sat Nov 29 04:28:29 2014
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY EC_SCALAR_MUL_EC_SCALAR_MUL_sch_tb IS
END EC_SCALAR_MUL_EC_SCALAR_MUL_sch_tb;
ARCHITECTURE behavioral OF EC_SCALAR_MUL_EC_SCALAR_MUL_sch_tb IS 

   COMPONENT EC_SCALAR_MUL
   PORT( A	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          P_Y	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          P_X	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          k	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          GO	:	IN	STD_LOGIC; 
          CLK	:	IN	STD_LOGIC; 
          SIG_WRONG_INPUT	:	OUT	STD_LOGIC; 
          EC_ADD_ZERO_DIV	:	OUT	STD_LOGIC; 
          EC_MUL_ZERO_DIV	:	OUT	STD_LOGIC; 
          R_Y	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          R_X	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          FINISHED	:	OUT	STD_LOGIC; 	
          B	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0));
   END COMPONENT;

   SIGNAL A	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL P_Y	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL P_X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL k	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL GO	:	STD_LOGIC;
   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL SIG_WRONG_INPUT	:	STD_LOGIC;
	SIGNAL EC_ADD_ZERO_DIV	:	STD_LOGIC;
	SIGNAL EC_MUL_ZERO_DIV	:	STD_LOGIC;
   SIGNAL R_Y	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL R_X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL FINISHED	:	STD_LOGIC;
   SIGNAL B	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
	constant CLK_period : time := 10 ns;

BEGIN

   UUT: EC_SCALAR_MUL PORT MAP(
		A => A, 
		P_Y => P_Y, 
		P_X => P_X, 
		k => k, 
		GO => GO, 
		CLK => CLK, 
		SIG_WRONG_INPUT => SIG_WRONG_INPUT, 
		EC_ADD_ZERO_DIV => EC_ADD_ZERO_DIV,	
		EC_MUL_ZERO_DIV => EC_MUL_ZERO_DIV,
		R_Y => R_Y, 
		R_X => R_X, 
		FINISHED => FINISHED, 
		B => B
   );

   -- Clock process definitions
   clk50_in_process :process
   begin
		CLK <= '0'; 
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
	
	
		   -- Stimulus process
   stim_proc: process
	variable I :
      integer range 0 to 255;
   begin	
		
--			valid data test
			wait for CLK_period;
			P_X <= conv_std_logic_vector(82, 9);
			P_Y <= conv_std_logic_vector(244, 9);				
			A <= conv_std_logic_vector(11, 9);			 
			B <= conv_std_logic_vector(1, 9);
			k <= conv_std_logic_vector(6, 9);
			GO <= '1'; 
			wait for CLK_period;
			GO <= '0';			
			wait until FINISHED = '1';				
			wait for CLK_period;
			
			I:=3;
			while (R_X /= "111111111" and R_Y /= "111111111") loop
				k <= conv_std_logic_vector(I, 9);
				GO <= '1'; 
				wait for CLK_period;
				GO <= '0';			
				wait until FINISHED = '1';				
				wait for CLK_period;
				I:=I+1;
			end loop;		
			wait;
   end process;

END;
