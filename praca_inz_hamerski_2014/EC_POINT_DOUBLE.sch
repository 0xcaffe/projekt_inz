<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK" />
        <signal name="P_X(8:0)" />
        <signal name="P_Y(8:0)" />
        <signal name="IN_LOG(8:0)" />
        <signal name="IN_VALUE(8:0)" />
        <signal name="OUT_VALUE(8:0)" />
        <signal name="OUT_LOG(8:0)" />
        <signal name="XLXN_23(1:0)" />
        <signal name="XLXN_24(8:0)" />
        <signal name="XLXN_25(8:0)" />
        <signal name="R_X(8:0)" />
        <signal name="R_Y(8:0)" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40(2:0)" />
        <signal name="XLXN_41(2:0)" />
        <signal name="XLXN_42(2:0)" />
        <signal name="XLXN_43(2:0)" />
        <signal name="XLXN_44(1:0)" />
        <signal name="XLXN_45(1:0)" />
        <signal name="XLXN_46(1:0)" />
        <signal name="B(8:0)" />
        <signal name="FINISHED" />
        <signal name="GO" />
        <signal name="EC_MUL_ZERO_DIV" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="P_X(8:0)" />
        <port polarity="Input" name="P_Y(8:0)" />
        <port polarity="Input" name="IN_LOG(8:0)" />
        <port polarity="Input" name="IN_VALUE(8:0)" />
        <port polarity="Output" name="OUT_VALUE(8:0)" />
        <port polarity="Output" name="OUT_LOG(8:0)" />
        <port polarity="Output" name="R_X(8:0)" />
        <port polarity="Output" name="R_Y(8:0)" />
        <port polarity="Input" name="B(8:0)" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Input" name="GO" />
        <port polarity="Output" name="EC_MUL_ZERO_DIV" />
        <blockdef name="EC_DOUBLE_Datapath">
            <timestamp>2014-11-29T1:3:59</timestamp>
            <rect width="64" x="0" y="212" height="24" />
            <line x2="0" y1="224" y2="224" x1="64" />
            <rect width="64" x="0" y="276" height="24" />
            <line x2="0" y1="288" y2="288" x1="64" />
            <rect width="64" x="0" y="340" height="24" />
            <line x2="0" y1="352" y2="352" x1="64" />
            <rect width="64" x="0" y="404" height="24" />
            <line x2="0" y1="416" y2="416" x1="64" />
            <line x2="0" y1="480" y2="480" x1="64" />
            <rect width="64" x="0" y="532" height="24" />
            <line x2="0" y1="544" y2="544" x1="64" />
            <line x2="0" y1="608" y2="608" x1="64" />
            <rect width="64" x="0" y="660" height="24" />
            <line x2="0" y1="672" y2="672" x1="64" />
            <rect width="64" x="0" y="724" height="24" />
            <line x2="0" y1="736" y2="736" x1="64" />
            <rect width="64" x="0" y="788" height="24" />
            <line x2="0" y1="800" y2="800" x1="64" />
            <line x2="0" y1="864" y2="864" x1="64" />
            <rect width="64" x="0" y="-940" height="24" />
            <line x2="0" y1="-928" y2="-928" x1="64" />
            <rect width="64" x="0" y="-876" height="24" />
            <line x2="0" y1="-864" y2="-864" x1="64" />
            <rect width="64" x="0" y="-748" height="24" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="512" y="-940" height="24" />
            <line x2="576" y1="-928" y2="-928" x1="512" />
            <rect width="64" x="512" y="-652" height="24" />
            <line x2="576" y1="-640" y2="-640" x1="512" />
            <rect width="64" x="512" y="-364" height="24" />
            <line x2="576" y1="-352" y2="-352" x1="512" />
            <rect width="64" x="512" y="-76" height="24" />
            <line x2="576" y1="-64" y2="-64" x1="512" />
            <rect width="448" x="64" y="-960" height="1856" />
        </blockdef>
        <blockdef name="EC_DOUBLE_Validator">
            <timestamp>2014-11-29T20:3:37</timestamp>
            <line x2="384" y1="96" y2="96" x1="320" />
            <rect width="64" x="320" y="20" height="24" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="256" x="64" y="-128" height="256" />
        </blockdef>
        <blockdef name="MULTIPLEXER">
            <timestamp>2014-11-16T14:39:20</timestamp>
            <rect width="336" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="400" y="-172" height="24" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
        </blockdef>
        <blockdef name="EC_DOUBLE_Control_Unit">
            <timestamp>2014-12-15T6:49:27</timestamp>
            <line x2="464" y1="288" y2="288" x1="400" />
            <line x2="464" y1="352" y2="352" x1="400" />
            <line x2="464" y1="416" y2="416" x1="400" />
            <rect width="64" x="400" y="468" height="24" />
            <line x2="464" y1="480" y2="480" x1="400" />
            <rect width="64" x="400" y="532" height="24" />
            <line x2="464" y1="544" y2="544" x1="400" />
            <rect width="64" x="400" y="596" height="24" />
            <line x2="464" y1="608" y2="608" x1="400" />
            <rect width="64" x="400" y="660" height="24" />
            <line x2="464" y1="672" y2="672" x1="400" />
            <rect width="64" x="400" y="724" height="24" />
            <line x2="464" y1="736" y2="736" x1="400" />
            <rect width="64" x="400" y="788" height="24" />
            <line x2="464" y1="800" y2="800" x1="400" />
            <rect width="64" x="400" y="852" height="24" />
            <line x2="464" y1="864" y2="864" x1="400" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <rect width="64" x="0" y="148" height="24" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="464" y1="-416" y2="-416" x1="400" />
            <rect width="336" x="64" y="-640" height="1536" />
        </blockdef>
        <block symbolname="EC_DOUBLE_Validator" name="XLXI_3">
            <blockpin signalname="P_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="P_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="XLXN_23(1:0)" name="ZERO(1:0)" />
            <blockpin signalname="EC_MUL_ZERO_DIV" name="SIG_ZERO_DIV" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_4">
            <blockpin signalname="XLXN_24(8:0)" name="X_1(8:0)" />
            <blockpin signalname="P_X(8:0)" name="X_2(8:0)" />
            <blockpin signalname="XLXN_23(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="R_X(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER" name="XLXI_5">
            <blockpin signalname="XLXN_25(8:0)" name="X_1(8:0)" />
            <blockpin signalname="P_Y(8:0)" name="X_2(8:0)" />
            <blockpin signalname="XLXN_23(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="R_Y(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="EC_DOUBLE_Datapath" name="XLXI_7">
            <blockpin signalname="P_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="B(8:0)" name="B(8:0)" />
            <blockpin signalname="P_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="XLXN_40(2:0)" name="MUX_LOG(2:0)" />
            <blockpin signalname="XLXN_45(1:0)" name="MUX_REGX(1:0)" />
            <blockpin signalname="XLXN_38" name="LOAD_REGX" />
            <blockpin signalname="XLXN_44(1:0)" name="MUX_REGT(1:0)" />
            <blockpin signalname="XLXN_37" name="LOAD_REGT" />
            <blockpin signalname="XLXN_42(2:0)" name="MUX_VAL(2:0)" />
            <blockpin signalname="XLXN_46(1:0)" name="MUX_REGY(1:0)" />
            <blockpin signalname="XLXN_39" name="LOAD_REGY" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="XLXN_41(2:0)" name="DEMUX_LOG(2:0)" />
            <blockpin signalname="IN_LOG(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="XLXN_43(2:0)" name="DEMUX_VAL(2:0)" />
            <blockpin signalname="IN_VALUE(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="XLXN_24(8:0)" name="R_X(8:0)" />
            <blockpin signalname="XLXN_25(8:0)" name="R_Y(8:0)" />
            <blockpin signalname="OUT_VALUE(8:0)" name="OUT_VALUE(8:0)" />
            <blockpin signalname="OUT_LOG(8:0)" name="OUT_LOG(8:0)" />
        </block>
        <block symbolname="EC_DOUBLE_Control_Unit" name="XLXI_8">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="GO" name="GO" />
            <blockpin signalname="XLXN_23(1:0)" name="VALID(1:0)" />
            <blockpin signalname="XLXN_37" name="LOAD_REGT" />
            <blockpin signalname="XLXN_38" name="LOAD_REGX" />
            <blockpin signalname="XLXN_39" name="LOAD_REGY" />
            <blockpin signalname="FINISHED" name="FINISHED" />
            <blockpin signalname="XLXN_40(2:0)" name="MUX_LOG(2:0)" />
            <blockpin signalname="XLXN_41(2:0)" name="DEMUX_LOG(2:0)" />
            <blockpin signalname="XLXN_42(2:0)" name="MUX_VAL(2:0)" />
            <blockpin signalname="XLXN_43(2:0)" name="DEMUX_VAL(2:0)" />
            <blockpin signalname="XLXN_44(1:0)" name="MUX_REGT(1:0)" />
            <blockpin signalname="XLXN_45(1:0)" name="MUX_REGX(1:0)" />
            <blockpin signalname="XLXN_46(1:0)" name="MUX_REGY(1:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <branch name="IN_LOG(8:0)">
            <wire x2="3360" y1="2768" y2="2768" x1="3328" />
        </branch>
        <iomarker fontsize="28" x="3328" y="2768" name="IN_LOG(8:0)" orien="R180" />
        <branch name="IN_VALUE(8:0)">
            <wire x2="3360" y1="3088" y2="3088" x1="3328" />
        </branch>
        <iomarker fontsize="28" x="3328" y="3088" name="IN_VALUE(8:0)" orien="R180" />
        <branch name="OUT_VALUE(8:0)">
            <wire x2="3968" y1="2576" y2="2576" x1="3936" />
        </branch>
        <iomarker fontsize="28" x="3968" y="2576" name="OUT_VALUE(8:0)" orien="R0" />
        <branch name="OUT_LOG(8:0)">
            <wire x2="3968" y1="2864" y2="2864" x1="3936" />
        </branch>
        <iomarker fontsize="28" x="3968" y="2864" name="OUT_LOG(8:0)" orien="R0" />
        <iomarker fontsize="28" x="2544" y="2960" name="FINISHED" orien="R0" />
        <instance x="3456" y="2384" name="XLXI_3" orien="R0">
        </instance>
        <branch name="XLXN_24(8:0)">
            <wire x2="4464" y1="3152" y2="3152" x1="3936" />
        </branch>
        <branch name="XLXN_25(8:0)">
            <wire x2="4464" y1="3440" y2="3440" x1="3936" />
        </branch>
        <instance x="4464" y="3312" name="XLXI_4" orien="R0">
        </instance>
        <instance x="4464" y="3600" name="XLXI_5" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1536" y="2352" name="P_Y(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1536" y="2288" name="P_X(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1904" y="2768" name="CLK" orien="R180" />
        <branch name="R_X(8:0)">
            <wire x2="4960" y1="3152" y2="3152" x1="4928" />
        </branch>
        <iomarker fontsize="28" x="4960" y="3152" name="R_X(8:0)" orien="R0" />
        <branch name="R_Y(8:0)">
            <wire x2="4960" y1="3440" y2="3440" x1="4928" />
        </branch>
        <iomarker fontsize="28" x="4960" y="3440" name="R_Y(8:0)" orien="R0" />
        <branch name="P_Y(8:0)">
            <wire x2="1600" y1="2352" y2="2352" x1="1536" />
            <wire x2="3120" y1="2352" y2="2352" x1="1600" />
            <wire x2="3456" y1="2352" y2="2352" x1="3120" />
            <wire x2="3120" y1="2352" y2="2640" x1="3120" />
            <wire x2="3360" y1="2640" y2="2640" x1="3120" />
            <wire x2="1600" y1="2352" y2="4560" x1="1600" />
            <wire x2="4240" y1="4560" y2="4560" x1="1600" />
            <wire x2="4240" y1="3504" y2="4560" x1="4240" />
            <wire x2="4464" y1="3504" y2="3504" x1="4240" />
        </branch>
        <branch name="P_X(8:0)">
            <wire x2="3200" y1="2288" y2="2288" x1="1536" />
            <wire x2="3456" y1="2288" y2="2288" x1="3200" />
            <wire x2="3200" y1="2288" y2="2576" x1="3200" />
            <wire x2="3360" y1="2576" y2="2576" x1="3200" />
            <wire x2="3200" y1="2576" y2="2576" x1="1696" />
            <wire x2="1696" y1="2576" y2="4480" x1="1696" />
            <wire x2="4080" y1="4480" y2="4480" x1="1696" />
            <wire x2="4080" y1="3216" y2="4480" x1="4080" />
            <wire x2="4464" y1="3216" y2="3216" x1="4080" />
        </branch>
        <instance x="3360" y="3504" name="XLXI_7" orien="R0">
        </instance>
        <branch name="XLXN_37">
            <wire x2="3280" y1="3664" y2="3664" x1="2512" />
            <wire x2="3280" y1="3664" y2="4112" x1="3280" />
            <wire x2="3360" y1="4112" y2="4112" x1="3280" />
        </branch>
        <branch name="XLXN_38">
            <wire x2="3200" y1="3728" y2="3728" x1="2512" />
            <wire x2="3200" y1="3728" y2="3984" x1="3200" />
            <wire x2="3360" y1="3984" y2="3984" x1="3200" />
        </branch>
        <branch name="XLXN_39">
            <wire x2="3120" y1="3792" y2="3792" x1="2512" />
            <wire x2="3120" y1="3792" y2="4368" x1="3120" />
            <wire x2="3360" y1="4368" y2="4368" x1="3120" />
        </branch>
        <branch name="XLXN_40(2:0)">
            <wire x2="3152" y1="3856" y2="3856" x1="2512" />
            <wire x2="3152" y1="3792" y2="3856" x1="3152" />
            <wire x2="3360" y1="3792" y2="3792" x1="3152" />
        </branch>
        <branch name="XLXN_41(2:0)">
            <wire x2="3248" y1="3920" y2="3920" x1="2512" />
            <wire x2="3360" y1="3856" y2="3856" x1="3248" />
            <wire x2="3248" y1="3856" y2="3920" x1="3248" />
        </branch>
        <branch name="XLXN_42(2:0)">
            <wire x2="3040" y1="3984" y2="3984" x1="2512" />
            <wire x2="3040" y1="3984" y2="4176" x1="3040" />
            <wire x2="3360" y1="4176" y2="4176" x1="3040" />
        </branch>
        <branch name="XLXN_43(2:0)">
            <wire x2="2960" y1="4048" y2="4048" x1="2512" />
            <wire x2="2960" y1="4048" y2="4240" x1="2960" />
            <wire x2="3360" y1="4240" y2="4240" x1="2960" />
        </branch>
        <branch name="XLXN_44(1:0)">
            <wire x2="2992" y1="4112" y2="4112" x1="2512" />
            <wire x2="3360" y1="4048" y2="4048" x1="2992" />
            <wire x2="2992" y1="4048" y2="4112" x1="2992" />
        </branch>
        <branch name="XLXN_45(1:0)">
            <wire x2="2880" y1="4176" y2="4176" x1="2512" />
            <wire x2="3312" y1="3952" y2="3952" x1="2880" />
            <wire x2="2880" y1="3952" y2="4176" x1="2880" />
            <wire x2="3360" y1="3920" y2="3920" x1="3312" />
            <wire x2="3312" y1="3920" y2="3952" x1="3312" />
        </branch>
        <branch name="XLXN_46(1:0)">
            <wire x2="2928" y1="4240" y2="4240" x1="2512" />
            <wire x2="2928" y1="4240" y2="4304" x1="2928" />
            <wire x2="3360" y1="4304" y2="4304" x1="2928" />
        </branch>
        <branch name="B(8:0)">
            <wire x2="3360" y1="3728" y2="3728" x1="3328" />
        </branch>
        <iomarker fontsize="28" x="3328" y="3728" name="B(8:0)" orien="R270" />
        <branch name="XLXN_23(1:0)">
            <wire x2="4384" y1="2144" y2="2144" x1="1792" />
            <wire x2="4384" y1="2144" y2="2416" x1="4384" />
            <wire x2="4384" y1="2416" y2="3280" x1="4384" />
            <wire x2="4464" y1="3280" y2="3280" x1="4384" />
            <wire x2="4384" y1="3280" y2="3568" x1="4384" />
            <wire x2="4464" y1="3568" y2="3568" x1="4384" />
            <wire x2="1792" y1="2144" y2="3536" x1="1792" />
            <wire x2="2048" y1="3536" y2="3536" x1="1792" />
            <wire x2="4384" y1="2416" y2="2416" x1="3840" />
        </branch>
        <iomarker fontsize="28" x="1936" y="3472" name="GO" orien="R180" />
        <branch name="CLK">
            <wire x2="1920" y1="2768" y2="2768" x1="1904" />
            <wire x2="2048" y1="2768" y2="2768" x1="1920" />
            <wire x2="2880" y1="2640" y2="2640" x1="1920" />
            <wire x2="2880" y1="2640" y2="3472" x1="2880" />
            <wire x2="3360" y1="3472" y2="3472" x1="2880" />
            <wire x2="1920" y1="2640" y2="2768" x1="1920" />
        </branch>
        <instance x="2048" y="3376" name="XLXI_8" orien="R0">
        </instance>
        <branch name="FINISHED">
            <wire x2="2544" y1="2960" y2="2960" x1="2512" />
        </branch>
        <branch name="GO">
            <wire x2="2048" y1="3472" y2="3472" x1="1936" />
        </branch>
        <branch name="EC_MUL_ZERO_DIV">
            <wire x2="3856" y1="2480" y2="2480" x1="3840" />
            <wire x2="4016" y1="2480" y2="2480" x1="3856" />
        </branch>
        <iomarker fontsize="28" x="4016" y="2480" name="EC_MUL_ZERO_DIV" orien="R0" />
    </sheet>
</drawing>