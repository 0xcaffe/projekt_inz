<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="P_Y(8:0)" />
        <signal name="Q_Y(8:0)" />
        <signal name="P_X(8:0)" />
        <signal name="Q_X(8:0)" />
        <signal name="OUT_VALUE(8:0)" />
        <signal name="XLXN_6(8:0)" />
        <signal name="XLXN_7(8:0)" />
        <signal name="MUX_LOG_CTRL(1:0)" />
        <signal name="DEMUX_LOG_CTRL(1:0)" />
        <signal name="IN_LOG(8:0)" />
        <signal name="XLXN_12(8:0)" />
        <signal name="REG_LOAD_S2" />
        <signal name="REG_LOAD_S1" />
        <signal name="OUTPUT(8:0)" />
        <signal name="XLXN_20(8:0)" />
        <signal name="XLXN_22(8:0)" />
        <signal name="OUT_LOG(8:0)" />
        <signal name="DEMUX_VAL_CTRL(1:0)" />
        <signal name="IN_VALUE(8:0)" />
        <signal name="MUX_VAL_CTRL(1:0)" />
        <signal name="XLXN_46(8:0)" />
        <signal name="XLXN_47(8:0)" />
        <signal name="A(8:0)" />
        <signal name="XLXN_50(8:0)" />
        <signal name="XLXN_51(8:0)" />
        <signal name="REG_LOAD_R_X" />
        <signal name="R_X(8:0)" />
        <signal name="XLXN_63(8:0)" />
        <signal name="XLXN_66(8:0)" />
        <signal name="XLXN_67(8:0)" />
        <signal name="XLXN_69(8:0)" />
        <signal name="XLXN_70(8:0)" />
        <signal name="XLXN_71(8:0)" />
        <signal name="REG_LOAD_R_Y" />
        <signal name="R_Y(8:0)" />
        <signal name="CLK" />
        <signal name="X_1(8:0)" />
        <signal name="XLXN_93(8:0)" />
        <signal name="XLXN_94(8:0)" />
        <signal name="XLXN_99(8:0)" />
        <signal name="REG_LOAD_SVAL" />
        <signal name="TEMP(8:0)" />
        <port polarity="Input" name="P_Y(8:0)" />
        <port polarity="Input" name="Q_Y(8:0)" />
        <port polarity="Input" name="P_X(8:0)" />
        <port polarity="Input" name="Q_X(8:0)" />
        <port polarity="Output" name="OUT_VALUE(8:0)" />
        <port polarity="Input" name="MUX_LOG_CTRL(1:0)" />
        <port polarity="Input" name="DEMUX_LOG_CTRL(1:0)" />
        <port polarity="Input" name="IN_LOG(8:0)" />
        <port polarity="Input" name="REG_LOAD_S2" />
        <port polarity="Input" name="REG_LOAD_S1" />
        <port polarity="Output" name="OUT_LOG(8:0)" />
        <port polarity="Input" name="DEMUX_VAL_CTRL(1:0)" />
        <port polarity="Input" name="IN_VALUE(8:0)" />
        <port polarity="Input" name="MUX_VAL_CTRL(1:0)" />
        <port polarity="Input" name="A(8:0)" />
        <port polarity="Input" name="REG_LOAD_R_X" />
        <port polarity="Output" name="R_X(8:0)" />
        <port polarity="Input" name="REG_LOAD_R_Y" />
        <port polarity="Output" name="R_Y(8:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="REG_LOAD_SVAL" />
        <blockdef name="MULTIPLEXER_3">
            <timestamp>2014-11-16T13:47:20</timestamp>
            <rect width="336" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="400" y="-236" height="24" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
        </blockdef>
        <blockdef name="GF_Add">
            <timestamp>2014-11-16T13:37:26</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="DEMULTIPLEXER_3">
            <timestamp>2014-11-16T14:2:3</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="REGISTER_8bit">
            <timestamp>2014-11-16T14:12:5</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="GF_Div_Log">
            <timestamp>2014-11-16T14:34:43</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="GF_Mul_Log">
            <timestamp>2014-11-16T14:37:39</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="MULTIPLEXER_3" name="XLXI_1">
            <blockpin signalname="XLXN_6(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_7(8:0)" name="X_2(8:0)" />
            <blockpin signalname="XLXN_63(8:0)" name="X_3(8:0)" />
            <blockpin signalname="MUX_LOG_CTRL(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="OUT_VALUE(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="GF_Add" name="XLXI_2">
            <blockpin signalname="P_Y(8:0)" name="X(8:0)" />
            <blockpin signalname="Q_Y(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_6(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="GF_Add" name="XLXI_3">
            <blockpin signalname="P_X(8:0)" name="X(8:0)" />
            <blockpin signalname="Q_X(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_7(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="DEMULTIPLEXER_3" name="XLXI_4">
            <blockpin signalname="DEMUX_LOG_CTRL(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="IN_LOG(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="X_1(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_12(8:0)" name="X_2(8:0)" />
            <blockpin signalname="XLXN_66(8:0)" name="X_3(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_6">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="REG_LOAD_S2" name="LOAD" />
            <blockpin signalname="XLXN_12(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="XLXN_20(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="GF_Div_Log" name="XLXI_7">
            <blockpin signalname="OUTPUT(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_20(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_22(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="GF_Mul_Log" name="XLXI_9">
            <blockpin signalname="XLXN_22(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_22(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_46(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="GF_Add" name="XLXI_12">
            <blockpin signalname="Q_X(8:0)" name="X(8:0)" />
            <blockpin signalname="A(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_51(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="GF_Add" name="XLXI_13">
            <blockpin signalname="P_X(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_47(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_50(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="GF_Add" name="XLXI_14">
            <blockpin signalname="XLXN_51(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_50(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_94(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_15">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="REG_LOAD_R_X" name="LOAD" />
            <blockpin signalname="XLXN_93(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="R_X(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="GF_Add" name="XLXI_16">
            <blockpin signalname="R_X(8:0)" name="X(8:0)" />
            <blockpin signalname="P_X(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_63(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="GF_Mul_Log" name="XLXI_17">
            <blockpin signalname="XLXN_66(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_22(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_67(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="GF_Add" name="XLXI_18">
            <blockpin signalname="R_X(8:0)" name="X(8:0)" />
            <blockpin signalname="P_Y(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_69(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="GF_Add" name="XLXI_19">
            <blockpin signalname="XLXN_69(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_70(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_71(8:0)" name="PRODUCT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_20">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="REG_LOAD_R_Y" name="LOAD" />
            <blockpin signalname="XLXN_71(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="R_Y(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="REG_S1">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="REG_LOAD_S1" name="LOAD" />
            <blockpin signalname="X_1(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="OUTPUT(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="DEMULTIPLEXER_3" name="XLXI_21">
            <blockpin signalname="DEMUX_VAL_CTRL(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="IN_VALUE(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="XLXN_47(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_70(8:0)" name="X_2(8:0)" />
            <blockpin signalname="XLXN_99(8:0)" name="X_3(8:0)" />
        </block>
        <block symbolname="MULTIPLEXER_3" name="XLXI_22">
            <blockpin signalname="XLXN_46(8:0)" name="X_1(8:0)" />
            <blockpin signalname="XLXN_67(8:0)" name="X_2(8:0)" />
            <blockpin signalname="XLXN_22(8:0)" name="X_3(8:0)" />
            <blockpin signalname="MUX_VAL_CTRL(1:0)" name="CONTROLL(1:0)" />
            <blockpin signalname="OUT_LOG(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="REGISTER_8bit" name="XLXI_23">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin name="CLR" />
            <blockpin signalname="REG_LOAD_SVAL" name="LOAD" />
            <blockpin signalname="XLXN_99(8:0)" name="INPUT(8:0)" />
            <blockpin signalname="TEMP(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="GF_Add" name="XLXI_24">
            <blockpin signalname="XLXN_94(8:0)" name="X(8:0)" />
            <blockpin signalname="TEMP(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_93(8:0)" name="PRODUCT(8:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="832" y="368" name="XLXI_1" orien="R0">
        </instance>
        <instance x="304" y="240" name="XLXI_2" orien="R0">
        </instance>
        <instance x="304" y="448" name="XLXI_3" orien="R0">
        </instance>
        <branch name="P_Y(8:0)">
            <wire x2="32" y1="80" y2="1712" x1="32" />
            <wire x2="1920" y1="1712" y2="1712" x1="32" />
            <wire x2="240" y1="80" y2="80" x1="32" />
            <wire x2="240" y1="80" y2="144" x1="240" />
            <wire x2="304" y1="144" y2="144" x1="240" />
            <wire x2="240" y1="144" y2="144" x1="192" />
        </branch>
        <branch name="Q_Y(8:0)">
            <wire x2="304" y1="208" y2="208" x1="192" />
        </branch>
        <branch name="P_X(8:0)">
            <wire x2="240" y1="352" y2="352" x1="192" />
            <wire x2="304" y1="352" y2="352" x1="240" />
            <wire x2="240" y1="352" y2="464" x1="240" />
            <wire x2="1600" y1="464" y2="464" x1="240" />
            <wire x2="1600" y1="464" y2="704" x1="1600" />
            <wire x2="1600" y1="704" y2="1488" x1="1600" />
            <wire x2="2944" y1="1488" y2="1488" x1="1600" />
            <wire x2="1648" y1="704" y2="704" x1="1600" />
            <wire x2="1648" y1="704" y2="816" x1="1648" />
            <wire x2="1664" y1="816" y2="816" x1="1648" />
        </branch>
        <branch name="Q_X(8:0)">
            <wire x2="272" y1="416" y2="416" x1="192" />
            <wire x2="304" y1="416" y2="416" x1="272" />
            <wire x2="272" y1="416" y2="624" x1="272" />
            <wire x2="1184" y1="624" y2="624" x1="272" />
        </branch>
        <branch name="OUT_VALUE(8:0)">
            <wire x2="1376" y1="144" y2="144" x1="1296" />
        </branch>
        <branch name="XLXN_6(8:0)">
            <wire x2="832" y1="144" y2="144" x1="688" />
        </branch>
        <branch name="XLXN_7(8:0)">
            <wire x2="704" y1="352" y2="352" x1="688" />
            <wire x2="704" y1="208" y2="352" x1="704" />
            <wire x2="832" y1="208" y2="208" x1="704" />
        </branch>
        <branch name="MUX_LOG_CTRL(1:0)">
            <wire x2="832" y1="336" y2="336" x1="816" />
            <wire x2="816" y1="336" y2="400" x1="816" />
            <wire x2="896" y1="400" y2="400" x1="816" />
        </branch>
        <iomarker fontsize="28" x="192" y="144" name="P_Y(8:0)" orien="R180" />
        <iomarker fontsize="28" x="192" y="208" name="Q_Y(8:0)" orien="R180" />
        <iomarker fontsize="28" x="192" y="352" name="P_X(8:0)" orien="R180" />
        <iomarker fontsize="28" x="192" y="416" name="Q_X(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1376" y="144" name="OUT_VALUE(8:0)" orien="R0" />
        <branch name="DEMUX_LOG_CTRL(1:0)">
            <wire x2="1712" y1="288" y2="288" x1="1632" />
        </branch>
        <branch name="IN_LOG(8:0)">
            <wire x2="1712" y1="416" y2="416" x1="1456" />
        </branch>
        <instance x="1712" y="448" name="XLXI_4" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1632" y="288" name="DEMUX_LOG_CTRL(1:0)" orien="R180" />
        <iomarker fontsize="28" x="1456" y="416" name="IN_LOG(8:0)" orien="R180" />
        <branch name="XLXN_12(8:0)">
            <wire x2="2160" y1="352" y2="352" x1="2096" />
            <wire x2="2160" y1="352" y2="640" x1="2160" />
            <wire x2="2480" y1="640" y2="640" x1="2160" />
        </branch>
        <branch name="REG_LOAD_S2">
            <wire x2="2480" y1="576" y2="576" x1="2416" />
        </branch>
        <branch name="REG_LOAD_S1">
            <wire x2="2480" y1="224" y2="224" x1="2464" />
        </branch>
        <instance x="2480" y="672" name="XLXI_6" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2416" y="576" name="REG_LOAD_S2" orien="R180" />
        <instance x="2976" y="224" name="XLXI_7" orien="R0">
        </instance>
        <branch name="XLXN_20(8:0)">
            <wire x2="2912" y1="448" y2="448" x1="2864" />
            <wire x2="2912" y1="192" y2="448" x1="2912" />
            <wire x2="2976" y1="192" y2="192" x1="2912" />
        </branch>
        <instance x="2992" y="928" name="XLXI_9" orien="R0">
        </instance>
        <branch name="OUT_LOG(8:0)">
            <wire x2="848" y1="816" y2="816" x1="768" />
        </branch>
        <branch name="DEMUX_VAL_CTRL(1:0)">
            <wire x2="1184" y1="880" y2="880" x1="1088" />
        </branch>
        <branch name="IN_VALUE(8:0)">
            <wire x2="1184" y1="1008" y2="1008" x1="1072" />
        </branch>
        <iomarker fontsize="28" x="1088" y="880" name="DEMUX_VAL_CTRL(1:0)" orien="R180" />
        <iomarker fontsize="28" x="848" y="816" name="OUT_LOG(8:0)" orien="R0" />
        <iomarker fontsize="28" x="896" y="400" name="MUX_LOG_CTRL(1:0)" orien="R0" />
        <branch name="XLXN_46(8:0)">
            <wire x2="2128" y1="480" y2="480" x1="240" />
            <wire x2="2128" y1="480" y2="944" x1="2128" />
            <wire x2="3456" y1="944" y2="944" x1="2128" />
            <wire x2="240" y1="480" y2="816" x1="240" />
            <wire x2="304" y1="816" y2="816" x1="240" />
            <wire x2="3456" y1="832" y2="832" x1="3376" />
            <wire x2="3456" y1="832" y2="944" x1="3456" />
        </branch>
        <instance x="2128" y="1152" name="XLXI_14" orien="R0">
        </instance>
        <instance x="1184" y="720" name="XLXI_12" orien="R0">
        </instance>
        <branch name="A(8:0)">
            <wire x2="1184" y1="688" y2="688" x1="1120" />
        </branch>
        <iomarker fontsize="28" x="1120" y="688" name="A(8:0)" orien="R180" />
        <branch name="XLXN_51(8:0)">
            <wire x2="2112" y1="624" y2="624" x1="1568" />
            <wire x2="2112" y1="624" y2="1056" x1="2112" />
            <wire x2="2128" y1="1056" y2="1056" x1="2112" />
        </branch>
        <instance x="2784" y="1280" name="XLXI_15" orien="R0">
        </instance>
        <branch name="REG_LOAD_R_X">
            <wire x2="2688" y1="1216" y2="1232" x1="2688" />
            <wire x2="2768" y1="1232" y2="1232" x1="2688" />
            <wire x2="2784" y1="1184" y2="1184" x1="2768" />
            <wire x2="2768" y1="1184" y2="1232" x1="2768" />
        </branch>
        <branch name="R_X(8:0)">
            <wire x2="1840" y1="1424" y2="1648" x1="1840" />
            <wire x2="1920" y1="1648" y2="1648" x1="1840" />
            <wire x2="2880" y1="1424" y2="1424" x1="1840" />
            <wire x2="2944" y1="1424" y2="1424" x1="2880" />
            <wire x2="3184" y1="1312" y2="1312" x1="2880" />
            <wire x2="2880" y1="1312" y2="1424" x1="2880" />
            <wire x2="3184" y1="1056" y2="1056" x1="3168" />
            <wire x2="3184" y1="1056" y2="1312" x1="3184" />
            <wire x2="3360" y1="1056" y2="1056" x1="3184" />
            <wire x2="3360" y1="1056" y2="1088" x1="3360" />
        </branch>
        <instance x="2944" y="1520" name="XLXI_16" orien="R0">
        </instance>
        <branch name="XLXN_63(8:0)">
            <wire x2="144" y1="560" y2="1536" x1="144" />
            <wire x2="3440" y1="1536" y2="1536" x1="144" />
            <wire x2="752" y1="560" y2="560" x1="144" />
            <wire x2="752" y1="272" y2="560" x1="752" />
            <wire x2="832" y1="272" y2="272" x1="752" />
            <wire x2="3440" y1="1424" y2="1424" x1="3328" />
            <wire x2="3440" y1="1424" y2="1536" x1="3440" />
        </branch>
        <instance x="2320" y="896" name="XLXI_17" orien="R0">
        </instance>
        <branch name="XLXN_66(8:0)">
            <wire x2="2144" y1="416" y2="416" x1="2096" />
            <wire x2="2144" y1="416" y2="800" x1="2144" />
            <wire x2="2320" y1="800" y2="800" x1="2144" />
        </branch>
        <branch name="XLXN_67(8:0)">
            <wire x2="192" y1="880" y2="1392" x1="192" />
            <wire x2="2736" y1="1392" y2="1392" x1="192" />
            <wire x2="304" y1="880" y2="880" x1="192" />
            <wire x2="2736" y1="800" y2="800" x1="2704" />
            <wire x2="2736" y1="800" y2="1392" x1="2736" />
        </branch>
        <instance x="1920" y="1744" name="XLXI_18" orien="R0">
        </instance>
        <instance x="2400" y="1840" name="XLXI_19" orien="R0">
        </instance>
        <branch name="XLXN_69(8:0)">
            <wire x2="2352" y1="1648" y2="1648" x1="2304" />
            <wire x2="2352" y1="1648" y2="1744" x1="2352" />
            <wire x2="2400" y1="1744" y2="1744" x1="2352" />
        </branch>
        <branch name="XLXN_70(8:0)">
            <wire x2="1728" y1="944" y2="944" x1="1568" />
            <wire x2="1728" y1="944" y2="1808" x1="1728" />
            <wire x2="2400" y1="1808" y2="1808" x1="1728" />
        </branch>
        <instance x="2880" y="2112" name="XLXI_20" orien="R0">
        </instance>
        <branch name="XLXN_71(8:0)">
            <wire x2="2832" y1="1744" y2="1744" x1="2784" />
            <wire x2="2832" y1="1744" y2="2080" x1="2832" />
            <wire x2="2880" y1="2080" y2="2080" x1="2832" />
        </branch>
        <branch name="REG_LOAD_R_Y">
            <wire x2="2880" y1="2016" y2="2016" x1="2656" />
        </branch>
        <iomarker fontsize="28" x="2656" y="2016" name="REG_LOAD_R_Y" orien="R180" />
        <branch name="R_Y(8:0)">
            <wire x2="3392" y1="1888" y2="1888" x1="3264" />
            <wire x2="3392" y1="1888" y2="1920" x1="3392" />
        </branch>
        <iomarker fontsize="28" x="3360" y="1088" name="R_X(8:0)" orien="R90" />
        <iomarker fontsize="28" x="3392" y="1920" name="R_Y(8:0)" orien="R90" />
        <iomarker fontsize="28" x="2080" y="96" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="2464" y="224" name="REG_LOAD_S1" orien="R180" />
        <instance x="2480" y="320" name="REG_S1" orien="R0">
        </instance>
        <branch name="X_1(8:0)">
            <wire x2="2480" y1="288" y2="288" x1="2096" />
        </branch>
        <instance x="1184" y="1040" name="XLXI_21" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1072" y="1008" name="IN_VALUE(8:0)" orien="R180" />
        <instance x="304" y="1040" name="XLXI_22" orien="R0">
        </instance>
        <iomarker fontsize="28" x="368" y="1072" name="MUX_VAL_CTRL(1:0)" orien="R0" />
        <branch name="MUX_VAL_CTRL(1:0)">
            <wire x2="304" y1="1008" y2="1008" x1="288" />
            <wire x2="288" y1="1008" y2="1072" x1="288" />
            <wire x2="368" y1="1072" y2="1072" x1="288" />
        </branch>
        <iomarker fontsize="28" x="2688" y="1216" name="REG_LOAD_R_X" orien="R270" />
        <instance x="2128" y="1376" name="XLXI_24" orien="R0">
        </instance>
        <branch name="XLXN_93(8:0)">
            <wire x2="2640" y1="1280" y2="1280" x1="2512" />
            <wire x2="2640" y1="1248" y2="1280" x1="2640" />
            <wire x2="2784" y1="1248" y2="1248" x1="2640" />
        </branch>
        <branch name="XLXN_94(8:0)">
            <wire x2="2576" y1="1168" y2="1168" x1="2112" />
            <wire x2="2112" y1="1168" y2="1280" x1="2112" />
            <wire x2="2128" y1="1280" y2="1280" x1="2112" />
            <wire x2="2576" y1="1056" y2="1056" x1="2512" />
            <wire x2="2576" y1="1056" y2="1168" x1="2576" />
        </branch>
        <instance x="992" y="1376" name="XLXI_23" orien="R0">
        </instance>
        <branch name="XLXN_99(8:0)">
            <wire x2="992" y1="1344" y2="1344" x1="912" />
            <wire x2="912" y1="1344" y2="1440" x1="912" />
            <wire x2="1680" y1="1440" y2="1440" x1="912" />
            <wire x2="1680" y1="1008" y2="1008" x1="1568" />
            <wire x2="1680" y1="1008" y2="1440" x1="1680" />
        </branch>
        <branch name="REG_LOAD_SVAL">
            <wire x2="992" y1="1280" y2="1280" x1="944" />
        </branch>
        <iomarker fontsize="28" x="944" y="1280" name="REG_LOAD_SVAL" orien="R180" />
        <branch name="XLXN_50(8:0)">
            <wire x2="2080" y1="816" y2="816" x1="2048" />
            <wire x2="2080" y1="816" y2="1120" x1="2080" />
            <wire x2="2128" y1="1120" y2="1120" x1="2080" />
        </branch>
        <branch name="XLXN_47(8:0)">
            <wire x2="1664" y1="880" y2="880" x1="1568" />
        </branch>
        <instance x="1664" y="912" name="XLXI_13" orien="R0">
        </instance>
        <branch name="OUTPUT(8:0)">
            <wire x2="2912" y1="96" y2="96" x1="2864" />
            <wire x2="2912" y1="96" y2="128" x1="2912" />
            <wire x2="2976" y1="128" y2="128" x1="2912" />
        </branch>
        <branch name="CLK">
            <wire x2="16" y1="32" y2="1152" x1="16" />
            <wire x2="992" y1="1152" y2="1152" x1="16" />
            <wire x2="2192" y1="32" y2="32" x1="16" />
            <wire x2="2192" y1="32" y2="96" x1="2192" />
            <wire x2="2480" y1="96" y2="96" x1="2192" />
            <wire x2="2192" y1="96" y2="448" x1="2192" />
            <wire x2="2432" y1="448" y2="448" x1="2192" />
            <wire x2="2480" y1="448" y2="448" x1="2432" />
            <wire x2="2432" y1="448" y2="688" x1="2432" />
            <wire x2="2768" y1="688" y2="688" x1="2432" />
            <wire x2="2768" y1="688" y2="1056" x1="2768" />
            <wire x2="2784" y1="1056" y2="1056" x1="2768" />
            <wire x2="2192" y1="96" y2="96" x1="2080" />
            <wire x2="2752" y1="1056" y2="1616" x1="2752" />
            <wire x2="2848" y1="1616" y2="1616" x1="2752" />
            <wire x2="2848" y1="1616" y2="1888" x1="2848" />
            <wire x2="2880" y1="1888" y2="1888" x1="2848" />
            <wire x2="2768" y1="1056" y2="1056" x1="2752" />
        </branch>
        <branch name="XLXN_22(8:0)">
            <wire x2="208" y1="768" y2="944" x1="208" />
            <wire x2="304" y1="944" y2="944" x1="208" />
            <wire x2="2256" y1="768" y2="768" x1="208" />
            <wire x2="2256" y1="768" y2="864" x1="2256" />
            <wire x2="2320" y1="864" y2="864" x1="2256" />
            <wire x2="2256" y1="864" y2="912" x1="2256" />
            <wire x2="2912" y1="912" y2="912" x1="2256" />
            <wire x2="2912" y1="688" y2="832" x1="2912" />
            <wire x2="2912" y1="832" y2="896" x1="2912" />
            <wire x2="2992" y1="896" y2="896" x1="2912" />
            <wire x2="2912" y1="896" y2="912" x1="2912" />
            <wire x2="2992" y1="832" y2="832" x1="2912" />
            <wire x2="3408" y1="688" y2="688" x1="2912" />
            <wire x2="3408" y1="128" y2="128" x1="3360" />
            <wire x2="3408" y1="128" y2="688" x1="3408" />
        </branch>
        <branch name="TEMP(8:0)">
            <wire x2="1472" y1="1152" y2="1152" x1="1376" />
            <wire x2="1952" y1="1152" y2="1152" x1="1472" />
            <wire x2="1952" y1="1152" y2="1344" x1="1952" />
            <wire x2="2128" y1="1344" y2="1344" x1="1952" />
        </branch>
    </sheet>
</drawing>