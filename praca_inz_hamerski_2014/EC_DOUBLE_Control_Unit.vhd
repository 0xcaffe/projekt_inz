----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:59:35 11/18/2014 
-- Design Name: 
-- Module Name:    EC_DOUBLE_Control_Unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EC_DOUBLE_Control_Unit is
	port( CLK 		     : in std_logic;
		   GO				  : in std_logic;
			VALID			  : in std_logic_vector (1 downto 0);	-- if "01" then input data is valid		
			LOAD_REGT	  : out std_logic;
			LOAD_REGX	  : out std_logic;
			LOAD_REGY 	  : out std_logic;			
			FINISHED	     : out std_logic;
			MUX_LOG		  : out std_logic_vector (2 downto 0);
			DEMUX_LOG	  : out std_logic_vector (2 downto 0);
			MUX_VAL		  : out std_logic_vector (2 downto 0);
			DEMUX_VAL	  : out std_logic_vector (2 downto 0);
			MUX_REGT		  : out std_logic_vector (1 downto 0);
			MUX_REGX		  : out std_logic_vector (1 downto 0);
			MUX_REGY	     : out std_logic_vector (1 downto 0));	
end EC_DOUBLE_Control_Unit;

architecture Behavioral of EC_DOUBLE_Control_Unit is

type state_type is (q0, q1, q2, q3, q4, q5, q6, q7);
signal present_state, next_state: state_type;

begin

process(CLK)
begin
	if(rising_edge(CLK)) then
		present_state <= next_state;
	end if;
end process;

process(present_state, GO, VALID)
begin
	case present_state is 										
						
				when q0 => 
						if(GO = '1' and VALID = "10") then							
							next_state <= q7;
						elsif(GO = '1' and VALID = "01") then
							next_state <= q1;								
						else 
							next_state <= q0;
						end if;
						
				when q1 =>
						next_state <= q2;		
						
				when q2 =>
						next_state <= q3;
						
				when q3 =>
						next_state <= q4;
				
				when q4 =>
						next_state <= q5;
						
				when q5 =>
						next_state <= q6;
						
				when q6 =>
						next_state <= q7;
						
				when q7 =>
						next_state <= q0;
						
				when others =>
						null;
	end case;
end process;

process(present_state)
begin

	LOAD_REGT <= '0';
	LOAD_REGX <= '0';
	LOAD_REGY <= '0';
	FINISHED  <= '0';
	
	case present_state is 						
	
			when q1 =>
				MUX_LOG 		<= "001";
				DEMUX_LOG 	<= "001";
				MUX_REGX 	<= "01";
				LOAD_REGX 	<= '1';
				MUX_VAL 		<= "001";
				DEMUX_VAL 	<= "001";
				MUX_REGY 	<= "01";
				LOAD_REGY 	<= '1';
				
			when q2 =>
				MUX_LOG 		<= "010";
				DEMUX_LOG 	<= "010";
				MUX_VAL 		<= "010";
				DEMUX_VAL 	<= "010";
				MUX_REGX 	<= "10";
				LOAD_REGX 	<= '1';				
				
			when q3 =>
				MUX_LOG 		<= "011";
				DEMUX_LOG 	<= "011";				
				MUX_REGT 	<= "01";
				LOAD_REGT	<= '1';
				
			when q4 =>
				MUX_LOG 		<= "001";
				DEMUX_LOG 	<= "001";
				MUX_VAL 		<= "011";
				DEMUX_VAL 	<= "011";
				MUX_REGT 	<= "10";
				LOAD_REGT	<= '1';
				
			when q5 => 
				MUX_LOG 		<= "100";
				DEMUX_LOG 	<= "100";	
				MUX_REGT 	<= "11";
				LOAD_REGT	<= '1';				
				
			when q6 => 
				MUX_LOG 		<= "101";
				DEMUX_LOG 	<= "101";
				MUX_VAL 		<= "100";
				DEMUX_VAL 	<= "100";	
				MUX_REGY 	<= "10";
				LOAD_REGY 	<= '1';				

			when q7 => 
				FINISHED <= '1';		
				
			when others =>
					null;
	end case;	
end process;

end Behavioral;

