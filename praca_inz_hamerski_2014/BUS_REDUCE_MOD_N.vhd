----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:39:45 11/30/2014 
-- Design Name: 
-- Module Name:    BUS_REDUCE_MOD_N - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BUS_REDUCE_MOD_N is
	port( X : in std_logic_vector(17 downto 0);
			N : in std_logic_vector(8 downto 0);
			OUTPUT : out std_logic_vector(8 downto 0));
end BUS_REDUCE_MOD_N;

architecture Behavioral of BUS_REDUCE_MOD_N is

	signal temp : std_logic_vector(17 downto 0);	

begin
process(X,N)
	begin
		if(unsigned(N) > 0) then
			temp <= conv_std_logic_vector((conv_integer(unsigned(X)) mod conv_integer(unsigned(N))),18);
--			temp <= "000000000000000000";
		else
			temp <= "000000000000000000";
		end if;
	end process;

process(temp)
	begin
		OUTPUT <= temp(8 downto 0);
	end process;

end Behavioral;

