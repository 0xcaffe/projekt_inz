--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   23:32:46 11/16/2014
-- Design Name:   
-- Module Name:   D:/inzynierka/praca_inz_hamerski_2014/REGISTER_Test_Bench.vhd
-- Project Name:  praca_inz_hamerski_2014
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: REGISTER_8bit
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY REGISTER_Test_Bench IS
END REGISTER_Test_Bench;
 
ARCHITECTURE behavior OF REGISTER_Test_Bench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT REGISTER_8bit
    PORT(
         CLK : IN  std_logic;
         CLR : IN  std_logic;
         LOAD : IN  std_logic;
         INPUT : IN  std_logic_vector(8 downto 0);
         OUTPUT : OUT  std_logic_vector(8 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal CLR : std_logic := '0';
   signal LOAD : std_logic := '0';
   signal INPUT : std_logic_vector(8 downto 0) := (others => '0');

 	--Outputs
   signal OUTPUT : std_logic_vector(8 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: REGISTER_8bit PORT MAP (
          CLK => CLK,
          CLR => CLR,
          LOAD => LOAD,
          INPUT => INPUT,
          OUTPUT => OUTPUT
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

		INPUT <= conv_std_logic_vector(5, 9);
		LOAD <= '1';
		wait for 10 ns;	
		LOAD <= '0';
		
      wait;
   end process;

END;
