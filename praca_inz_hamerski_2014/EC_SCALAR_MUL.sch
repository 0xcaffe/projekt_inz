<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5(1:0)" />
        <signal name="XLXN_6(1:0)" />
        <signal name="XLXN_7(1:0)" />
        <signal name="XLXN_8(1:0)" />
        <signal name="A(8:0)" />
        <signal name="P_Y(8:0)" />
        <signal name="P_X(8:0)" />
        <signal name="k(8:0)" />
        <signal name="GO" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="CLK" />
        <signal name="SIG_WRONG_INPUT" />
        <signal name="R_Y(8:0)" />
        <signal name="R_X(8:0)" />
        <signal name="FINISHED" />
        <signal name="B(8:0)" />
        <signal name="EC_ADD_ZERO_DIV" />
        <signal name="EC_MUL_ZERO_DIV" />
        <port polarity="Input" name="A(8:0)" />
        <port polarity="Input" name="P_Y(8:0)" />
        <port polarity="Input" name="P_X(8:0)" />
        <port polarity="Input" name="k(8:0)" />
        <port polarity="Input" name="GO" />
        <port polarity="Input" name="CLK" />
        <port polarity="Output" name="SIG_WRONG_INPUT" />
        <port polarity="Output" name="R_Y(8:0)" />
        <port polarity="Output" name="R_X(8:0)" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Input" name="B(8:0)" />
        <port polarity="Output" name="EC_ADD_ZERO_DIV" />
        <port polarity="Output" name="EC_MUL_ZERO_DIV" />
        <blockdef name="EC_SCALAR_MUL_Datapath">
            <timestamp>2014-11-29T20:5:44</timestamp>
            <line x2="544" y1="96" y2="96" x1="480" />
            <line x2="544" y1="160" y2="160" x1="480" />
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <rect width="64" x="0" y="-684" height="24" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <rect width="64" x="0" y="-620" height="24" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="480" y="-748" height="24" />
            <line x2="544" y1="-736" y2="-736" x1="480" />
            <rect width="64" x="480" y="-572" height="24" />
            <line x2="544" y1="-560" y2="-560" x1="480" />
            <line x2="544" y1="-384" y2="-384" x1="480" />
            <line x2="544" y1="-208" y2="-208" x1="480" />
            <line x2="544" y1="-32" y2="-32" x1="480" />
            <rect width="416" x="64" y="-768" height="960" />
        </blockdef>
        <blockdef name="EC_SCALAR_MUL_Control_Unit">
            <timestamp>2014-11-29T16:21:5</timestamp>
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="496" y1="-544" y2="-544" x1="432" />
            <line x2="496" y1="-480" y2="-480" x1="432" />
            <line x2="496" y1="-416" y2="-416" x1="432" />
            <line x2="496" y1="-352" y2="-352" x1="432" />
            <line x2="496" y1="-288" y2="-288" x1="432" />
            <rect width="64" x="432" y="-236" height="24" />
            <line x2="496" y1="-224" y2="-224" x1="432" />
            <rect width="64" x="432" y="-172" height="24" />
            <line x2="496" y1="-160" y2="-160" x1="432" />
            <rect width="64" x="432" y="-108" height="24" />
            <line x2="496" y1="-96" y2="-96" x1="432" />
            <rect width="64" x="432" y="-44" height="24" />
            <line x2="496" y1="-32" y2="-32" x1="432" />
            <rect width="368" x="64" y="-576" height="704" />
        </blockdef>
        <block symbolname="EC_SCALAR_MUL_Datapath" name="XLXI_1">
            <blockpin signalname="XLXN_1" name="GO_ADD" />
            <blockpin signalname="XLXN_5(1:0)" name="MUX_CONV(1:0)" />
            <blockpin signalname="XLXN_6(1:0)" name="MUX_MUL(1:0)" />
            <blockpin signalname="XLXN_2" name="GO_MUL" />
            <blockpin signalname="P_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="P_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="XLXN_7(1:0)" name="MUX_REG_R(1:0)" />
            <blockpin signalname="XLXN_8(1:0)" name="MUX_REG_Q(1:0)" />
            <blockpin signalname="XLXN_4" name="LOAD_REG_Q" />
            <blockpin signalname="XLXN_3" name="LOAD_REG_R" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="A(8:0)" name="A(8:0)" />
            <blockpin signalname="B(8:0)" name="B(8:0)" />
            <blockpin signalname="R_X(8:0)" name="R_X(8:0)" />
            <blockpin signalname="R_Y(8:0)" name="R_Y(8:0)" />
            <blockpin signalname="SIG_WRONG_INPUT" name="SIG_WRONG_INPUT" />
            <blockpin signalname="XLXN_14" name="ADD_FINISHED" />
            <blockpin signalname="XLXN_15" name="MUL_FINISHED" />
            <blockpin signalname="EC_ADD_ZERO_DIV" name="EC_ADD_ZERO_DIV" />
            <blockpin signalname="EC_MUL_ZERO_DIV" name="EC_MUL_ZERO_DIV" />
        </block>
        <block symbolname="EC_SCALAR_MUL_Control_Unit" name="XLXI_2">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="GO" name="GO" />
            <blockpin signalname="XLXN_14" name="ADD_FINISHED" />
            <blockpin signalname="XLXN_15" name="MUL_FINISHED" />
            <blockpin signalname="k(8:0)" name="k(8:0)" />
            <blockpin signalname="FINISHED" name="FINISHED" />
            <blockpin signalname="XLXN_1" name="GO_ADD" />
            <blockpin signalname="XLXN_2" name="GO_MUL" />
            <blockpin signalname="XLXN_3" name="LOAD_REG_R" />
            <blockpin signalname="XLXN_4" name="LOAD_REG_Q" />
            <blockpin signalname="XLXN_5(1:0)" name="MUX_CONV(1:0)" />
            <blockpin signalname="XLXN_6(1:0)" name="MUX_MUL(1:0)" />
            <blockpin signalname="XLXN_7(1:0)" name="MUX_REG_R(1:0)" />
            <blockpin signalname="XLXN_8(1:0)" name="MUX_REG_Q(1:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="2016" y="1600" name="XLXI_1" orien="R0">
        </instance>
        <instance x="848" y="1600" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_1">
            <wire x2="1760" y1="1120" y2="1120" x1="1344" />
            <wire x2="2016" y1="864" y2="864" x1="1760" />
            <wire x2="1760" y1="864" y2="1120" x1="1760" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="1680" y1="1184" y2="1184" x1="1344" />
            <wire x2="1680" y1="1056" y2="1184" x1="1680" />
            <wire x2="2016" y1="1056" y2="1056" x1="1680" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1680" y1="1248" y2="1248" x1="1344" />
            <wire x2="1680" y1="1248" y2="1440" x1="1680" />
            <wire x2="2016" y1="1440" y2="1440" x1="1680" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1440" y1="1312" y2="1312" x1="1344" />
            <wire x2="1440" y1="1312" y2="1344" x1="1440" />
            <wire x2="1648" y1="1344" y2="1344" x1="1440" />
            <wire x2="1648" y1="1344" y2="1376" x1="1648" />
            <wire x2="2016" y1="1376" y2="1376" x1="1648" />
        </branch>
        <branch name="XLXN_5(1:0)">
            <wire x2="1520" y1="1376" y2="1376" x1="1344" />
            <wire x2="1520" y1="928" y2="1376" x1="1520" />
            <wire x2="2016" y1="928" y2="928" x1="1520" />
        </branch>
        <branch name="XLXN_6(1:0)">
            <wire x2="1600" y1="1440" y2="1440" x1="1344" />
            <wire x2="1600" y1="992" y2="1440" x1="1600" />
            <wire x2="2016" y1="992" y2="992" x1="1600" />
        </branch>
        <branch name="XLXN_7(1:0)">
            <wire x2="1760" y1="1504" y2="1504" x1="1344" />
            <wire x2="2016" y1="1248" y2="1248" x1="1760" />
            <wire x2="1760" y1="1248" y2="1504" x1="1760" />
        </branch>
        <branch name="XLXN_8(1:0)">
            <wire x2="1840" y1="1568" y2="1568" x1="1344" />
            <wire x2="2016" y1="1312" y2="1312" x1="1840" />
            <wire x2="1840" y1="1312" y2="1568" x1="1840" />
        </branch>
        <branch name="A(8:0)">
            <wire x2="2016" y1="1568" y2="1568" x1="1984" />
        </branch>
        <branch name="P_Y(8:0)">
            <wire x2="2016" y1="1184" y2="1184" x1="1984" />
        </branch>
        <iomarker fontsize="28" x="1984" y="1184" name="P_Y(8:0)" orien="R180" />
        <branch name="P_X(8:0)">
            <wire x2="2016" y1="1120" y2="1120" x1="1984" />
        </branch>
        <iomarker fontsize="28" x="1984" y="1120" name="P_X(8:0)" orien="R180" />
        <branch name="k(8:0)">
            <wire x2="848" y1="1568" y2="1568" x1="816" />
        </branch>
        <iomarker fontsize="28" x="816" y="1568" name="k(8:0)" orien="R180" />
        <branch name="GO">
            <wire x2="848" y1="1184" y2="1184" x1="816" />
        </branch>
        <iomarker fontsize="28" x="816" y="1184" name="GO" orien="R180" />
        <branch name="XLXN_14">
            <wire x2="848" y1="1312" y2="1312" x1="560" />
            <wire x2="560" y1="1312" y2="1840" x1="560" />
            <wire x2="3040" y1="1840" y2="1840" x1="560" />
            <wire x2="3040" y1="1392" y2="1392" x1="2560" />
            <wire x2="3040" y1="1392" y2="1840" x1="3040" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="640" y1="720" y2="1440" x1="640" />
            <wire x2="832" y1="1440" y2="1440" x1="640" />
            <wire x2="848" y1="1440" y2="1440" x1="832" />
            <wire x2="2960" y1="720" y2="720" x1="640" />
            <wire x2="2960" y1="720" y2="1568" x1="2960" />
            <wire x2="2960" y1="1568" y2="1568" x1="2560" />
        </branch>
        <branch name="CLK">
            <wire x2="480" y1="1040" y2="1056" x1="480" />
            <wire x2="848" y1="1056" y2="1056" x1="480" />
            <wire x2="480" y1="1056" y2="1760" x1="480" />
            <wire x2="1920" y1="1760" y2="1760" x1="480" />
            <wire x2="2016" y1="1504" y2="1504" x1="1920" />
            <wire x2="1920" y1="1504" y2="1760" x1="1920" />
        </branch>
        <iomarker fontsize="28" x="480" y="1040" name="CLK" orien="R270" />
        <branch name="SIG_WRONG_INPUT">
            <wire x2="2576" y1="1216" y2="1216" x1="2560" />
            <wire x2="2592" y1="1216" y2="1216" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2592" y="1216" name="SIG_WRONG_INPUT" orien="R0" />
        <branch name="R_Y(8:0)">
            <wire x2="2576" y1="1040" y2="1040" x1="2560" />
            <wire x2="2592" y1="1040" y2="1040" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2592" y="1040" name="R_Y(8:0)" orien="R0" />
        <branch name="R_X(8:0)">
            <wire x2="2576" y1="864" y2="864" x1="2560" />
            <wire x2="2592" y1="864" y2="864" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2592" y="864" name="R_X(8:0)" orien="R0" />
        <branch name="FINISHED">
            <wire x2="1360" y1="1056" y2="1056" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="1360" y="1056" name="FINISHED" orien="R0" />
        <branch name="B(8:0)">
            <wire x2="2016" y1="1632" y2="1632" x1="1984" />
        </branch>
        <iomarker fontsize="28" x="1984" y="1632" name="B(8:0)" orien="R180" />
        <iomarker fontsize="28" x="1984" y="1568" name="A(8:0)" orien="R180" />
        <branch name="EC_ADD_ZERO_DIV">
            <wire x2="2592" y1="1696" y2="1696" x1="2560" />
        </branch>
        <iomarker fontsize="28" x="2592" y="1696" name="EC_ADD_ZERO_DIV" orien="R0" />
        <branch name="EC_MUL_ZERO_DIV">
            <wire x2="2592" y1="1760" y2="1760" x1="2560" />
        </branch>
        <iomarker fontsize="28" x="2592" y="1760" name="EC_MUL_ZERO_DIV" orien="R0" />
    </sheet>
</drawing>