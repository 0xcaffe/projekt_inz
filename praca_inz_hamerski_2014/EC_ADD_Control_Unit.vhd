----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:17:13 11/16/2014 
-- Design Name: 
-- Module Name:    EC_ADD_Control_Unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EC_ADD_Control_Unit is
	port( CLK 		     : in std_logic;
		   GO				  : in std_logic;
			VALID			  : in std_logic_vector (1 downto 0);	-- if "01" then input data is valid
			REG_LOAD_S1   : out std_logic;
			REG_LOAD_S2   : out std_logic;
			REG_LOAD_SVAL : out std_logic;
			REG_LOAD_RX   : out std_logic;
			REG_LOAD_RY   : out std_logic;
			FINISHED	     : out std_logic;
			MUX_LOG_CTRL   : out std_logic_vector (1 downto 0);
			DEMUX_LOG_CTRL : out std_logic_vector (1 downto 0);
			MUX_VAL_CTRL   : out std_logic_vector (1 downto 0);
			DEMUX_VAL_CTRL : out std_logic_vector (1 downto 0));			
end EC_ADD_Control_Unit;

architecture Behavioral of EC_ADD_Control_Unit is

type state_type is (q1, q2, q3, q4, q5, q6);
signal present_state, next_state: state_type;

begin

process(CLK)
begin
	if(rising_edge(CLK)) then
		present_state <= next_state;
	end if;
end process;

process(present_state, VALID, GO)
begin
	case present_state is 										
						
				when q1 =>
						if(GO = '1') then
							if(VALID = "01") then 
								next_state <= q2;
							else
								next_state <= q6;
							end if;
						else
							next_state <= q1;
						end if;
						
				when q2 =>
						next_state <= q3;
						
				when q3 =>
						next_state <= q4;
				
				when q4 =>
						next_state <= q5;
						
				when q5 =>
						next_state <= q6;
						
				when q6 =>
						next_state <= q1;							
				
				when others =>
						null;
	end case;
end process;


process(present_state)
begin

	REG_LOAD_S1   <= '0';
	REG_LOAD_S2   <= '0';
	REG_LOAD_SVAL <= '0';
	REG_LOAD_RX   <= '0';
	REG_LOAD_RY   <= '0';
	FINISHED      <= '0';
	
	case present_state is 						
	
			when q1 =>		
				MUX_LOG_CTRL 	<= "01";
				DEMUX_LOG_CTRL <= "01";
				REG_LOAD_S1 	<= '1';
				
			when q2 =>
				MUX_LOG_CTRL 	<= "10";
				DEMUX_LOG_CTRL <= "10";
				REG_LOAD_S2 	<= '1';

			when q3 => 				
				MUX_VAL_CTRL 	<= "11";
				DEMUX_VAL_CTRL <= "11";					
				REG_LOAD_SVAL 	<= '1';

			when q4 => 				
				MUX_VAL_CTRL 	<= "01";
				DEMUX_VAL_CTRL <= "01";
				REG_LOAD_RX 	<= '1';

			when q5 => 
				MUX_LOG_CTRL 	<= "11";
				DEMUX_LOG_CTRL <= "11";
				MUX_VAL_CTRL 	<= "10";
				DEMUX_VAL_CTRL <= "10";
				REG_LOAD_RY 	<= '1';

			when q6 => 
				FINISHED 		<= '1';		
				
			when others =>
					null;
	end case;	
end process;

end Behavioral;

