----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    02:51:27 11/19/2014 
-- Design Name: 
-- Module Name:    EC_ADD_Validator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity EC_ADD_Validator is
	port( P_X 				 : in std_logic_vector (8 downto 0);
			P_Y 				 : in std_logic_vector (8 downto 0);
			Q_X 				 : in std_logic_vector (8 downto 0);
			Q_Y 				 : in std_logic_vector (8 downto 0);
			SIG_WRONG_INPUT : out std_logic;
			SIG_ZERO_DIV	 : out std_logic;
			OUTPUT	       : out std_logic_vector (1 downto 0));	
end EC_ADD_Validator;

architecture Behavioral of EC_ADD_Validator is

begin

process(P_X,P_Y,Q_X,Q_Y)
begin
	SIG_ZERO_DIV <= '0';
	if(P_X = Q_X and (P_X xor P_Y) = Q_Y) then
		SIG_WRONG_INPUT <= '1';
		OUTPUT <= "00"; 
	else
		SIG_WRONG_INPUT <= '0';
		if(P_X = "000000000" and P_Y = "000000000") then	
			OUTPUT <= "11";
		elsif(Q_X = "000000000" and Q_Y = "000000000") then	
			OUTPUT <= "10";
		elsif(P_X = Q_X and P_Y /= Q_Y) then
			SIG_ZERO_DIV <= '1';
			OUTPUT <= "00";
		else
			OUTPUT <= "01"; -- ok
		end if;
	end if;
end process;

end Behavioral;

