----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    03:04:25 11/30/2014 
-- Design Name: 
-- Module Name:    Inverter_Mod_N_Control_Unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Inverter_Mod_N_Control_Unit is
	port( CLK 				: in std_logic;	-- Clock
			GO 				: in std_logic;	-- Go signal				
			Y_GT_0			: in std_logic;	-- Y2 greater then 0
			Y_EQ_0			: in std_logic;	-- Y2 equals 0
			Y1_GT_Y2			: in std_logic;	-- Y1 greater then Y2
			MUX_SET_1		: out std_logic_vector(1 downto 0);	-- Load initial (X1,Y1) and (X2,Y2) values
			LOAD_VEC_1		: out std_logic;	-- Store data in vector (X1,Y1)
			LOAD_VEC_2		: out std_logic;	-- Store data in vector (X2,Y2)	
			LOAD_REG_Y 		: out std_logic;	-- Store result in register
			FINISHED			: out std_logic;	-- Finished signal
			ERROR_SIG		: out std_logic;	-- Inverse doesn't exist
			ZERO				: out std_logic_vector (8 downto 0);
			ONE				: out std_logic_vector (8 downto 0));	
end Inverter_Mod_N_Control_Unit;

architecture Behavioral of Inverter_Mod_N_Control_Unit is

type state_type is (start, load, check1, check2, update1, update2, store, done, error);
signal present_state, next_state: state_type;

begin

process(CLK)
begin
	if(rising_edge(CLK)) then
		present_state <= next_state;
	end if;
end process;

process(present_state,GO,Y_GT_0,Y_EQ_0,Y1_GT_Y2)
begin
	case present_state is
	
				when start =>
						if(GO='1') then
							next_state <= load;
						else
							next_state <= start;
						end if;
						
				when load =>
						next_state <= check1;
						
				when check1 =>
						if(Y_EQ_0 = '1') then
							next_state <= store;
						elsif(Y_GT_0 = '1') then
							next_state <= check2;
						else
							next_state <= error;
						end if;
						
				when check2 =>
						if(Y1_GT_Y2 ='1') then
							next_state <= update1;
						else
							next_state <= update2;
						end if;
						
				when update1 =>
						next_state <= check1;
					
				when update2 =>
						next_state <= check1;
						
				when store =>
						next_state <= done;
						
				when done =>
						next_state <= start;
						
				when error =>
						next_state <= start;		
						
				when others =>
						null;
	end case;
end process;

process(present_state)
begin

	LOAD_VEC_1 <= '0';
	LOAD_VEC_2 <= '0';
	LOAD_REG_Y <= '0';
	ERROR_SIG  <= '0';
	FINISHED   <= '0';
	MUX_SET_1  <= "10";
	ZERO <= (others => '0');
	ONE <= (0 => '1', others => '0');
	
	case present_state is 
					
			when load => 
					MUX_SET_1  <= "01";
					LOAD_VEC_1 <= '1';
					LOAD_VEC_2 <= '1';				
					
			when update1 =>
					LOAD_VEC_1 <= '1';
					
			when update2 =>
					LOAD_VEC_2 <= '1';
					
			when store =>					
					LOAD_REG_Y <= '1';
				
			when done =>
					FINISHED <= '1';
	
			when error =>
					ERROR_SIG <= '1';
					
			when others =>
					null;
	end case;	
end process;

end Behavioral;

