<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_3(8:0)" />
        <signal name="XLXN_4(1:0)" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="X(8:0)" />
        <signal name="N(8:0)" />
        <signal name="GO" />
        <signal name="Y(8:0)" />
        <signal name="CLK" />
        <signal name="XLXN_18(8:0)" />
        <signal name="FINISHED" />
        <signal name="ERROR_SIG" />
        <port polarity="Input" name="X(8:0)" />
        <port polarity="Input" name="N(8:0)" />
        <port polarity="Input" name="GO" />
        <port polarity="Output" name="Y(8:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Output" name="ERROR_SIG" />
        <blockdef name="Inverter_Mod_N_Datapath">
            <timestamp>2014-11-30T15:25:52</timestamp>
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="448" y1="-544" y2="-544" x1="384" />
            <line x2="448" y1="-384" y2="-384" x1="384" />
            <line x2="448" y1="-224" y2="-224" x1="384" />
            <rect width="64" x="384" y="-76" height="24" />
            <line x2="448" y1="-64" y2="-64" x1="384" />
            <rect width="320" x="64" y="-576" height="640" />
        </blockdef>
        <blockdef name="Inverter_Mod_N_Control_Unit">
            <timestamp>2014-11-30T15:32:31</timestamp>
            <line x2="448" y1="96" y2="96" x1="384" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="448" y1="-416" y2="-416" x1="384" />
            <line x2="448" y1="-352" y2="-352" x1="384" />
            <line x2="448" y1="-288" y2="-288" x1="384" />
            <line x2="448" y1="-224" y2="-224" x1="384" />
            <rect width="64" x="384" y="-172" height="24" />
            <line x2="448" y1="-160" y2="-160" x1="384" />
            <rect width="64" x="384" y="-108" height="24" />
            <line x2="448" y1="-96" y2="-96" x1="384" />
            <rect width="64" x="384" y="-44" height="24" />
            <line x2="448" y1="-32" y2="-32" x1="384" />
            <rect width="320" x="64" y="-448" height="576" />
        </blockdef>
        <block symbolname="Inverter_Mod_N_Datapath" name="XLXI_1">
            <blockpin signalname="XLXN_18(8:0)" name="ONE(8:0)" />
            <blockpin signalname="X(8:0)" name="Y_1(8:0)" />
            <blockpin signalname="XLXN_3(8:0)" name="ZERO(8:0)" />
            <blockpin signalname="N(8:0)" name="Y_2(8:0)" />
            <blockpin signalname="XLXN_5" name="LOAD_REG_Y" />
            <blockpin signalname="XLXN_7" name="LOAD_VEC_1" />
            <blockpin signalname="XLXN_6" name="LOAD_VEC_2" />
            <blockpin signalname="XLXN_4(1:0)" name="MUX_SET_1(1:0)" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="XLXN_8" name="Y_GT_0" />
            <blockpin signalname="XLXN_9" name="Y_EQ_0" />
            <blockpin signalname="XLXN_10" name="Y1_GT_Y2" />
            <blockpin signalname="Y(8:0)" name="Y(8:0)" />
        </block>
        <block symbolname="Inverter_Mod_N_Control_Unit" name="XLXI_2">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="GO" name="GO" />
            <blockpin signalname="XLXN_8" name="Y_GT_0" />
            <blockpin signalname="XLXN_9" name="Y_EQ_0" />
            <blockpin signalname="XLXN_10" name="Y1_GT_Y2" />
            <blockpin signalname="XLXN_7" name="LOAD_VEC_1" />
            <blockpin signalname="XLXN_6" name="LOAD_VEC_2" />
            <blockpin signalname="XLXN_5" name="LOAD_REG_Y" />
            <blockpin signalname="FINISHED" name="FINISHED" />
            <blockpin signalname="XLXN_4(1:0)" name="MUX_SET_1(1:0)" />
            <blockpin signalname="XLXN_3(8:0)" name="ZERO(8:0)" />
            <blockpin signalname="XLXN_18(8:0)" name="ONE(8:0)" />
            <blockpin signalname="ERROR_SIG" name="ERROR_SIG" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="896" y="1408" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2096" y="1456" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_3(8:0)">
            <wire x2="1920" y1="1312" y2="1312" x1="1344" />
            <wire x2="2096" y1="1040" y2="1040" x1="1920" />
            <wire x2="1920" y1="1040" y2="1312" x1="1920" />
        </branch>
        <branch name="XLXN_4(1:0)">
            <wire x2="1840" y1="1248" y2="1248" x1="1344" />
            <wire x2="1840" y1="1248" y2="1360" x1="1840" />
            <wire x2="2096" y1="1360" y2="1360" x1="1840" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1712" y1="1120" y2="1120" x1="1344" />
            <wire x2="1712" y1="1120" y2="1168" x1="1712" />
            <wire x2="2096" y1="1168" y2="1168" x1="1712" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1520" y1="1056" y2="1056" x1="1344" />
            <wire x2="1520" y1="1056" y2="1296" x1="1520" />
            <wire x2="2096" y1="1296" y2="1296" x1="1520" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1600" y1="992" y2="992" x1="1344" />
            <wire x2="1600" y1="992" y2="1232" x1="1600" />
            <wire x2="2096" y1="1232" y2="1232" x1="1600" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="720" y1="720" y2="1184" x1="720" />
            <wire x2="896" y1="1184" y2="1184" x1="720" />
            <wire x2="2720" y1="720" y2="720" x1="720" />
            <wire x2="2720" y1="720" y2="912" x1="2720" />
            <wire x2="2720" y1="912" y2="912" x1="2544" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="896" y1="1280" y2="1280" x1="720" />
            <wire x2="720" y1="1280" y2="1600" x1="720" />
            <wire x2="2880" y1="1600" y2="1600" x1="720" />
            <wire x2="2880" y1="1072" y2="1072" x1="2544" />
            <wire x2="2880" y1="1072" y2="1600" x1="2880" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="2800" y1="640" y2="640" x1="560" />
            <wire x2="2800" y1="640" y2="1232" x1="2800" />
            <wire x2="560" y1="640" y2="1376" x1="560" />
            <wire x2="896" y1="1376" y2="1376" x1="560" />
            <wire x2="2800" y1="1232" y2="1232" x1="2544" />
        </branch>
        <branch name="X(8:0)">
            <wire x2="2096" y1="976" y2="976" x1="2064" />
        </branch>
        <iomarker fontsize="28" x="2064" y="976" name="X(8:0)" orien="R180" />
        <branch name="N(8:0)">
            <wire x2="2096" y1="1104" y2="1104" x1="2064" />
        </branch>
        <iomarker fontsize="28" x="2064" y="1104" name="N(8:0)" orien="R180" />
        <branch name="GO">
            <wire x2="896" y1="1088" y2="1088" x1="864" />
        </branch>
        <iomarker fontsize="28" x="864" y="1088" name="GO" orien="R180" />
        <branch name="Y(8:0)">
            <wire x2="2576" y1="1392" y2="1392" x1="2544" />
        </branch>
        <iomarker fontsize="28" x="2576" y="1392" name="Y(8:0)" orien="R0" />
        <branch name="CLK">
            <wire x2="640" y1="944" y2="992" x1="640" />
            <wire x2="896" y1="992" y2="992" x1="640" />
            <wire x2="640" y1="992" y2="1680" x1="640" />
            <wire x2="2000" y1="1680" y2="1680" x1="640" />
            <wire x2="2000" y1="1424" y2="1680" x1="2000" />
            <wire x2="2096" y1="1424" y2="1424" x1="2000" />
        </branch>
        <iomarker fontsize="28" x="640" y="944" name="CLK" orien="R270" />
        <branch name="XLXN_18(8:0)">
            <wire x2="1760" y1="1376" y2="1376" x1="1344" />
            <wire x2="2096" y1="912" y2="912" x1="1760" />
            <wire x2="1760" y1="912" y2="1376" x1="1760" />
        </branch>
        <branch name="FINISHED">
            <wire x2="1376" y1="1184" y2="1184" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="1376" y="1184" name="FINISHED" orien="R0" />
        <branch name="ERROR_SIG">
            <wire x2="1376" y1="1504" y2="1504" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="1376" y="1504" name="ERROR_SIG" orien="R0" />
    </sheet>
</drawing>