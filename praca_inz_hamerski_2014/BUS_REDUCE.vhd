----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:54:11 11/30/2014 
-- Design Name: 
-- Module Name:    BUS_REDUCE - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BUS_REDUCE is
	port( INPUT	 : in std_logic_vector(17 downto 0);		
			OUTPUT : out std_logic_vector(8 downto 0));
end BUS_REDUCE;

architecture Behavioral of BUS_REDUCE is
begin
	process(INPUT)
	begin
		OUTPUT <= INPUT(8 downto 0);
	end process;
end Behavioral;

