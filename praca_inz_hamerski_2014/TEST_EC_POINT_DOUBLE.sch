<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK" />
        <signal name="P_X(8:0)" />
        <signal name="P_Y(8:0)" />
        <signal name="R_X(8:0)" />
        <signal name="R_Y(8:0)" />
        <signal name="FINISHED" />
        <signal name="OUT_VALUE1(8:0)" />
        <signal name="OUT_LOG(8:0)" />
        <signal name="OUT_VALUE(8:0)" />
        <signal name="OUT_LOG1(8:0)" />
        <signal name="GO" />
        <signal name="B(8:0)" />
        <signal name="XLXN_1" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="P_X(8:0)" />
        <port polarity="Input" name="P_Y(8:0)" />
        <port polarity="Output" name="R_X(8:0)" />
        <port polarity="Output" name="R_Y(8:0)" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Input" name="GO" />
        <port polarity="Input" name="B(8:0)" />
        <blockdef name="EC_POINT_DOUBLE">
            <timestamp>2014-11-29T20:4:51</timestamp>
            <line x2="480" y1="160" y2="160" x1="416" />
            <rect width="64" x="0" y="84" height="24" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-364" height="24" />
            <line x2="480" y1="-352" y2="-352" x1="416" />
            <rect width="64" x="416" y="-284" height="24" />
            <line x2="480" y1="-272" y2="-272" x1="416" />
            <rect width="64" x="416" y="-204" height="24" />
            <line x2="480" y1="-192" y2="-192" x1="416" />
            <rect width="64" x="416" y="-124" height="24" />
            <line x2="480" y1="-112" y2="-112" x1="416" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
            <rect width="352" x="64" y="-384" height="640" />
        </blockdef>
        <blockdef name="Coverter">
            <timestamp>2014-11-15T19:47:59</timestamp>
            <rect width="352" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-108" height="24" />
            <line x2="480" y1="-96" y2="-96" x1="416" />
            <rect width="64" x="416" y="-44" height="24" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
        </blockdef>
        <block symbolname="Coverter" name="XLXI_2">
            <blockpin signalname="OUT_LOG(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="OUT_VALUE(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="OUT_LOG1(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="OUT_VALUE1(8:0)" name="OUT_VALUE(8:0)" />
        </block>
        <block symbolname="EC_POINT_DOUBLE" name="XLXI_1">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="P_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="P_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="OUT_LOG1(8:0)" name="IN_LOG(8:0)" />
            <blockpin signalname="OUT_VALUE1(8:0)" name="IN_VALUE(8:0)" />
            <blockpin signalname="GO" name="GO" />
            <blockpin signalname="B(8:0)" name="B(8:0)" />
            <blockpin signalname="OUT_VALUE(8:0)" name="OUT_VALUE(8:0)" />
            <blockpin signalname="OUT_LOG(8:0)" name="OUT_LOG(8:0)" />
            <blockpin signalname="FINISHED" name="FINISHED" />
            <blockpin signalname="R_X(8:0)" name="R_X(8:0)" />
            <blockpin signalname="R_Y(8:0)" name="R_Y(8:0)" />
            <blockpin name="EC_MUL_ZERO_DIV" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="CLK">
            <wire x2="1520" y1="928" y2="928" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="1488" y="928" name="CLK" orien="R180" />
        <branch name="P_X(8:0)">
            <wire x2="1520" y1="992" y2="992" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="1488" y="992" name="P_X(8:0)" orien="R180" />
        <branch name="P_Y(8:0)">
            <wire x2="1520" y1="1056" y2="1056" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="1488" y="1056" name="P_Y(8:0)" orien="R180" />
        <branch name="R_X(8:0)">
            <wire x2="2032" y1="1088" y2="1088" x1="2000" />
        </branch>
        <iomarker fontsize="28" x="2032" y="1088" name="R_X(8:0)" orien="R0" />
        <branch name="R_Y(8:0)">
            <wire x2="2032" y1="1168" y2="1168" x1="2000" />
        </branch>
        <iomarker fontsize="28" x="2032" y="1168" name="R_Y(8:0)" orien="R0" />
        <branch name="FINISHED">
            <wire x2="2016" y1="1248" y2="1248" x1="2000" />
        </branch>
        <branch name="OUT_VALUE(8:0)">
            <wire x2="2160" y1="720" y2="720" x1="1120" />
            <wire x2="2160" y1="720" y2="928" x1="2160" />
            <wire x2="1120" y1="720" y2="1664" x1="1120" />
            <wire x2="1520" y1="1664" y2="1664" x1="1120" />
            <wire x2="2160" y1="928" y2="928" x1="2000" />
        </branch>
        <branch name="OUT_LOG(8:0)">
            <wire x2="1520" y1="1600" y2="1600" x1="1456" />
            <wire x2="1456" y1="1600" y2="1760" x1="1456" />
            <wire x2="2240" y1="1760" y2="1760" x1="1456" />
            <wire x2="2240" y1="1008" y2="1008" x1="2000" />
            <wire x2="2240" y1="1008" y2="1760" x1="2240" />
        </branch>
        <branch name="OUT_LOG1(8:0)">
            <wire x2="1280" y1="800" y2="1120" x1="1280" />
            <wire x2="1520" y1="1120" y2="1120" x1="1280" />
            <wire x2="2320" y1="800" y2="800" x1="1280" />
            <wire x2="2320" y1="800" y2="1600" x1="2320" />
            <wire x2="2320" y1="1600" y2="1600" x1="2000" />
        </branch>
        <branch name="GO">
            <wire x2="1520" y1="1312" y2="1312" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="1488" y="1312" name="GO" orien="R180" />
        <instance x="1520" y="1696" name="XLXI_2" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2016" y="1248" name="FINISHED" orien="R0" />
        <instance x="1520" y="1280" name="XLXI_1" orien="R0">
        </instance>
        <branch name="B(8:0)">
            <wire x2="1520" y1="1376" y2="1376" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="1488" y="1376" name="B(8:0)" orien="R180" />
        <branch name="OUT_VALUE1(8:0)">
            <wire x2="1520" y1="1248" y2="1248" x1="1280" />
            <wire x2="1280" y1="1248" y2="1440" x1="1280" />
            <wire x2="1280" y1="1440" y2="1552" x1="1280" />
            <wire x2="2080" y1="1552" y2="1552" x1="1280" />
            <wire x2="2080" y1="1552" y2="1664" x1="2080" />
            <wire x2="2080" y1="1664" y2="1664" x1="2000" />
        </branch>
    </sheet>
</drawing>