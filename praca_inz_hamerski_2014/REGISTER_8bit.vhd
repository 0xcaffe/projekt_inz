----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:11:02 11/16/2014 
-- Design Name: 
-- Module Name:    REGISTER_8bit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity REGISTER_8bit is
	port( CLK 		: in std_logic;
			CLR 		: in std_logic;
			LOAD 		: in std_logic;
			INPUT 	: in std_logic_vector(8 downto 0);
			OUTPUT 	: out std_logic_vector(8 downto 0));
end REGISTER_8bit;

architecture Behavioral of REGISTER_8bit is
	
begin
	process(CLK,CLR,LOAD,INPUT)
	begin
		if(rising_edge(CLK)) then
			if(CLR = '1') then
				OUTPUT <= (others => '0');
			elsif(LOAD = '1') then
				OUTPUT <= INPUT;
			end if;
		end if;
	end process;
end Behavioral;

