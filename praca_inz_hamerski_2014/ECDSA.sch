<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="k(8:0)" />
        <signal name="GO" />
        <signal name="CLK" />
        <signal name="XLXN_12(8:0)" />
        <signal name="XLXN_13(8:0)" />
        <signal name="dA(8:0)" />
        <signal name="r(8:0)" />
        <signal name="EC_ADD_ZERO_DIV" />
        <signal name="EC_MUL_ZERO_DIV" />
        <signal name="SIG_WRONG_INPUT" />
        <signal name="e(8:0)" />
        <signal name="XLXN_39(8:0)" />
        <signal name="XLXN_40(8:0)" />
        <signal name="XLXN_45(17:0)" />
        <signal name="s(8:0)" />
        <signal name="FINISHED_MUL" />
        <signal name="FINISHED_INV" />
        <signal name="FINISHED" />
        <signal name="INVERTER_ERROR" />
        <signal name="A(8:0)" />
        <signal name="G_Y(8:0)" />
        <signal name="G_X(8:0)" />
        <signal name="B(8:0)" />
        <signal name="n(8:0)" />
        <signal name="XLXN_57(17:0)" />
        <signal name="XLXN_58(8:0)" />
        <signal name="XLXN_70" />
        <signal name="XLXN_78" />
        <port polarity="Input" name="k(8:0)" />
        <port polarity="Input" name="GO" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="dA(8:0)" />
        <port polarity="Output" name="r(8:0)" />
        <port polarity="Output" name="EC_ADD_ZERO_DIV" />
        <port polarity="Output" name="EC_MUL_ZERO_DIV" />
        <port polarity="Output" name="SIG_WRONG_INPUT" />
        <port polarity="Input" name="e(8:0)" />
        <port polarity="Output" name="s(8:0)" />
        <port polarity="Output" name="FINISHED" />
        <port polarity="Output" name="INVERTER_ERROR" />
        <port polarity="Input" name="A(8:0)" />
        <port polarity="Input" name="G_Y(8:0)" />
        <port polarity="Input" name="G_X(8:0)" />
        <port polarity="Input" name="B(8:0)" />
        <port polarity="Input" name="n(8:0)" />
        <blockdef name="EC_SCALAR_MUL">
            <timestamp>2014-11-30T0:36:6</timestamp>
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="464" y1="32" y2="32" x1="400" />
            <line x2="464" y1="96" y2="96" x1="400" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="464" y1="-352" y2="-352" x1="400" />
            <rect width="64" x="400" y="-268" height="24" />
            <line x2="464" y1="-256" y2="-256" x1="400" />
            <rect width="64" x="400" y="-172" height="24" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
            <line x2="464" y1="-64" y2="-64" x1="400" />
            <rect width="336" x="64" y="-384" height="512" />
        </blockdef>
        <blockdef name="Reduce_Mod_N">
            <timestamp>2014-11-30T0:58:9</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="Inverter_Mod_N">
            <timestamp>2014-11-30T15:43:58</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-128" y2="-128" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="MULTIPLIER">
            <timestamp>2014-11-30T16:58:47</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="ADDER">
            <timestamp>2014-11-30T16:59:49</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="BUS_REDUCE_MOD_N">
            <timestamp>2014-11-30T17:48:20</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="LATCH">
            <timestamp>2014-11-30T20:4:5</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="EC_SCALAR_MUL" name="XLXI_1">
            <blockpin signalname="A(8:0)" name="A(8:0)" />
            <blockpin signalname="G_Y(8:0)" name="P_Y(8:0)" />
            <blockpin signalname="G_X(8:0)" name="P_X(8:0)" />
            <blockpin signalname="XLXN_12(8:0)" name="k(8:0)" />
            <blockpin signalname="GO" name="GO" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="SIG_WRONG_INPUT" name="SIG_WRONG_INPUT" />
            <blockpin name="R_Y(8:0)" />
            <blockpin signalname="XLXN_13(8:0)" name="R_X(8:0)" />
            <blockpin signalname="FINISHED_MUL" name="FINISHED" />
            <blockpin signalname="B(8:0)" name="B(8:0)" />
            <blockpin signalname="EC_ADD_ZERO_DIV" name="EC_ADD_ZERO_DIV" />
            <blockpin signalname="EC_MUL_ZERO_DIV" name="EC_MUL_ZERO_DIV" />
        </block>
        <block symbolname="Reduce_Mod_N" name="XLXI_2">
            <blockpin signalname="k(8:0)" name="X(8:0)" />
            <blockpin signalname="n(8:0)" name="N(8:0)" />
            <blockpin signalname="XLXN_12(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="Inverter_Mod_N" name="XLXI_3">
            <blockpin signalname="XLXN_12(8:0)" name="X(8:0)" />
            <blockpin signalname="n(8:0)" name="N(8:0)" />
            <blockpin signalname="GO" name="GO" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="XLXN_40(8:0)" name="Y(8:0)" />
            <blockpin signalname="FINISHED_INV" name="FINISHED" />
            <blockpin signalname="INVERTER_ERROR" name="ERROR_SIG" />
        </block>
        <block symbolname="Reduce_Mod_N" name="XLXI_4">
            <blockpin signalname="XLXN_13(8:0)" name="X(8:0)" />
            <blockpin signalname="n(8:0)" name="N(8:0)" />
            <blockpin signalname="r(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLIER" name="XLXI_6">
            <blockpin signalname="r(8:0)" name="X(8:0)" />
            <blockpin signalname="dA(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_57(17:0)" name="OUTPUT(17:0)" />
        </block>
        <block symbolname="ADDER" name="XLXI_7">
            <blockpin signalname="XLXN_58(8:0)" name="X(8:0)" />
            <blockpin signalname="e(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_39(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="MULTIPLIER" name="XLXI_10">
            <blockpin signalname="XLXN_39(8:0)" name="X(8:0)" />
            <blockpin signalname="XLXN_40(8:0)" name="Y(8:0)" />
            <blockpin signalname="XLXN_45(17:0)" name="OUTPUT(17:0)" />
        </block>
        <block symbolname="and2" name="XLXI_17">
            <blockpin signalname="XLXN_78" name="I0" />
            <blockpin signalname="XLXN_70" name="I1" />
            <blockpin signalname="FINISHED" name="O" />
        </block>
        <block symbolname="BUS_REDUCE_MOD_N" name="XLXI_18">
            <blockpin signalname="XLXN_45(17:0)" name="X(17:0)" />
            <blockpin signalname="n(8:0)" name="N(8:0)" />
            <blockpin signalname="s(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="BUS_REDUCE_MOD_N" name="XLXI_19">
            <blockpin signalname="XLXN_57(17:0)" name="X(17:0)" />
            <blockpin signalname="n(8:0)" name="N(8:0)" />
            <blockpin signalname="XLXN_58(8:0)" name="OUTPUT(8:0)" />
        </block>
        <block symbolname="LATCH" name="XLXI_49">
            <blockpin signalname="FINISHED_MUL" name="INPUT" />
            <blockpin signalname="XLXN_70" name="OUTPUT" />
        </block>
        <block symbolname="LATCH" name="XLXI_50">
            <blockpin signalname="FINISHED_INV" name="INPUT" />
            <blockpin signalname="XLXN_78" name="OUTPUT" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="GO">
            <wire x2="320" y1="1200" y2="1200" x1="128" />
            <wire x2="320" y1="1200" y2="2000" x1="320" />
            <wire x2="1056" y1="2000" y2="2000" x1="320" />
            <wire x2="320" y1="848" y2="1200" x1="320" />
            <wire x2="1056" y1="848" y2="848" x1="320" />
        </branch>
        <iomarker fontsize="28" x="128" y="1120" name="CLK" orien="R180" />
        <iomarker fontsize="28" x="128" y="1200" name="GO" orien="R180" />
        <iomarker fontsize="28" x="128" y="1280" name="k(8:0)" orien="R180" />
        <branch name="k(8:0)">
            <wire x2="432" y1="1280" y2="1280" x1="128" />
        </branch>
        <instance x="432" y="1376" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1056" y="2096" name="XLXI_3" orien="R0">
        </instance>
        <instance x="1056" y="944" name="XLXI_1" orien="R0">
        </instance>
        <iomarker fontsize="28" x="128" y="1344" name="n(8:0)" orien="R180" />
        <branch name="XLXN_12(8:0)">
            <wire x2="880" y1="1280" y2="1280" x1="816" />
            <wire x2="880" y1="1280" y2="1872" x1="880" />
            <wire x2="1056" y1="1872" y2="1872" x1="880" />
            <wire x2="880" y1="784" y2="1280" x1="880" />
            <wire x2="1056" y1="784" y2="784" x1="880" />
        </branch>
        <branch name="XLXN_13(8:0)">
            <wire x2="1712" y1="784" y2="784" x1="1520" />
        </branch>
        <branch name="dA(8:0)">
            <wire x2="2240" y1="848" y2="848" x1="2224" />
        </branch>
        <branch name="r(8:0)">
            <wire x2="2160" y1="784" y2="784" x1="2096" />
            <wire x2="2240" y1="784" y2="784" x1="2160" />
            <wire x2="2160" y1="640" y2="784" x1="2160" />
        </branch>
        <branch name="EC_ADD_ZERO_DIV">
            <wire x2="1600" y1="976" y2="976" x1="1520" />
            <wire x2="1600" y1="976" y2="992" x1="1600" />
        </branch>
        <branch name="EC_MUL_ZERO_DIV">
            <wire x2="1536" y1="1040" y2="1040" x1="1520" />
        </branch>
        <branch name="SIG_WRONG_INPUT">
            <wire x2="1552" y1="592" y2="592" x1="1520" />
        </branch>
        <iomarker fontsize="28" x="1552" y="592" name="SIG_WRONG_INPUT" orien="R0" />
        <iomarker fontsize="28" x="1536" y="1040" name="EC_MUL_ZERO_DIV" orien="R90" />
        <iomarker fontsize="28" x="1600" y="992" name="EC_ADD_ZERO_DIV" orien="R90" />
        <instance x="1712" y="880" name="XLXI_4" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2224" y="848" name="dA(8:0)" orien="R180" />
        <instance x="2240" y="880" name="XLXI_6" orien="R0">
        </instance>
        <branch name="e(8:0)">
            <wire x2="2656" y1="1104" y2="1104" x1="2560" />
        </branch>
        <branch name="XLXN_39(8:0)">
            <wire x2="3120" y1="1600" y2="1600" x1="1600" />
            <wire x2="1600" y1="1600" y2="1808" x1="1600" />
            <wire x2="1616" y1="1808" y2="1808" x1="1600" />
            <wire x2="3120" y1="1040" y2="1040" x1="3040" />
            <wire x2="3120" y1="1040" y2="1600" x1="3120" />
        </branch>
        <branch name="XLXN_40(8:0)">
            <wire x2="1616" y1="1872" y2="1872" x1="1440" />
        </branch>
        <instance x="1616" y="1904" name="XLXI_10" orien="R0">
        </instance>
        <branch name="XLXN_45(17:0)">
            <wire x2="2240" y1="1808" y2="1808" x1="2000" />
        </branch>
        <branch name="s(8:0)">
            <wire x2="2800" y1="1808" y2="1808" x1="2624" />
        </branch>
        <iomarker fontsize="28" x="2160" y="640" name="r(8:0)" orien="R270" />
        <branch name="FINISHED">
            <wire x2="2640" y1="1328" y2="1328" x1="2624" />
        </branch>
        <branch name="INVERTER_ERROR">
            <wire x2="1472" y1="2064" y2="2064" x1="1440" />
            <wire x2="1472" y1="2064" y2="2144" x1="1472" />
            <wire x2="1472" y1="2144" y2="2144" x1="1456" />
        </branch>
        <iomarker fontsize="28" x="1456" y="2144" name="INVERTER_ERROR" orien="R180" />
        <branch name="A(8:0)">
            <wire x2="1056" y1="592" y2="592" x1="1024" />
        </branch>
        <iomarker fontsize="28" x="1024" y="592" name="A(8:0)" orien="R180" />
        <branch name="G_Y(8:0)">
            <wire x2="1056" y1="656" y2="656" x1="1024" />
        </branch>
        <iomarker fontsize="28" x="1024" y="656" name="G_Y(8:0)" orien="R180" />
        <branch name="G_X(8:0)">
            <wire x2="1056" y1="720" y2="720" x1="1024" />
        </branch>
        <iomarker fontsize="28" x="1024" y="720" name="G_X(8:0)" orien="R180" />
        <branch name="B(8:0)">
            <wire x2="1056" y1="976" y2="976" x1="1024" />
        </branch>
        <iomarker fontsize="28" x="1024" y="976" name="B(8:0)" orien="R180" />
        <instance x="2240" y="1904" name="XLXI_18" orien="R0">
        </instance>
        <branch name="n(8:0)">
            <wire x2="400" y1="1344" y2="1344" x1="128" />
            <wire x2="432" y1="1344" y2="1344" x1="400" />
            <wire x2="400" y1="1344" y2="1440" x1="400" />
            <wire x2="400" y1="1440" y2="1936" x1="400" />
            <wire x2="1056" y1="1936" y2="1936" x1="400" />
            <wire x2="1680" y1="1440" y2="1440" x1="400" />
            <wire x2="2080" y1="1440" y2="1440" x1="1680" />
            <wire x2="2080" y1="1440" y2="1872" x1="2080" />
            <wire x2="2240" y1="1872" y2="1872" x1="2080" />
            <wire x2="2240" y1="1440" y2="1440" x1="2080" />
            <wire x2="1712" y1="848" y2="848" x1="1680" />
            <wire x2="1680" y1="848" y2="1440" x1="1680" />
            <wire x2="2240" y1="912" y2="1440" x1="2240" />
            <wire x2="2640" y1="912" y2="912" x1="2240" />
            <wire x2="2640" y1="848" y2="912" x1="2640" />
            <wire x2="2816" y1="848" y2="848" x1="2640" />
        </branch>
        <iomarker fontsize="28" x="2800" y="1808" name="s(8:0)" orien="R0" />
        <iomarker fontsize="28" x="2560" y="1104" name="e(8:0)" orien="R180" />
        <instance x="2656" y="1136" name="XLXI_7" orien="R0">
        </instance>
        <instance x="2816" y="880" name="XLXI_19" orien="R0">
        </instance>
        <branch name="XLXN_57(17:0)">
            <wire x2="2816" y1="784" y2="784" x1="2624" />
        </branch>
        <branch name="XLXN_58(8:0)">
            <wire x2="3280" y1="928" y2="928" x1="2576" />
            <wire x2="2576" y1="928" y2="1040" x1="2576" />
            <wire x2="2656" y1="1040" y2="1040" x1="2576" />
            <wire x2="3280" y1="784" y2="784" x1="3200" />
            <wire x2="3280" y1="784" y2="928" x1="3280" />
        </branch>
        <branch name="FINISHED_MUL">
            <wire x2="1648" y1="880" y2="880" x1="1520" />
            <wire x2="1648" y1="880" y2="1024" x1="1648" />
            <wire x2="1712" y1="1024" y2="1024" x1="1648" />
        </branch>
        <branch name="CLK">
            <wire x2="240" y1="1120" y2="1120" x1="128" />
            <wire x2="240" y1="1120" y2="2064" x1="240" />
            <wire x2="1056" y1="2064" y2="2064" x1="240" />
            <wire x2="1056" y1="912" y2="912" x1="240" />
            <wire x2="240" y1="912" y2="1120" x1="240" />
        </branch>
        <branch name="FINISHED_INV">
            <wire x2="1600" y1="1968" y2="1968" x1="1440" />
            <wire x2="1600" y1="1968" y2="2064" x1="1600" />
            <wire x2="1616" y1="2064" y2="2064" x1="1600" />
        </branch>
        <instance x="1712" y="1056" name="XLXI_49" orien="R0">
        </instance>
        <instance x="1616" y="2096" name="XLXI_50" orien="R0">
        </instance>
        <instance x="2368" y="1424" name="XLXI_17" orien="R0" />
        <branch name="XLXN_70">
            <wire x2="2160" y1="1024" y2="1024" x1="2096" />
            <wire x2="2160" y1="1024" y2="1296" x1="2160" />
            <wire x2="2368" y1="1296" y2="1296" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2640" y="1328" name="FINISHED" orien="R0" />
        <branch name="XLXN_78">
            <wire x2="3072" y1="2064" y2="2064" x1="2000" />
            <wire x2="2368" y1="1360" y2="1360" x1="2352" />
            <wire x2="2352" y1="1360" y2="1440" x1="2352" />
            <wire x2="3072" y1="1440" y2="1440" x1="2352" />
            <wire x2="3072" y1="1440" y2="2064" x1="3072" />
        </branch>
    </sheet>
</drawing>