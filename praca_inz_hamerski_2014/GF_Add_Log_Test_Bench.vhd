-- Vhdl test bench created from schematic D:\inzynierka\praca_inz_hamerski_2014\TEST_GF_Add.sch - Sat Nov 15 20:50:43 2014
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY TEST_GF_Add_TEST_GF_Add_sch_tb IS
END TEST_GF_Add_TEST_GF_Add_sch_tb;
ARCHITECTURE behavioral OF TEST_GF_Add_TEST_GF_Add_sch_tb IS 

   COMPONENT TEST_GF_Add
   PORT( CLK	:	IN	STD_LOGIC; 
          X	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          Y	:	IN	STD_LOGIC_VECTOR (8 DOWNTO 0); 
          FINISHED	:	OUT	STD_LOGIC; 
          PRODUCT	:	OUT	STD_LOGIC_VECTOR (8 DOWNTO 0));
   END COMPONENT;

   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL X	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL Y	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
   SIGNAL FINISHED	:	STD_LOGIC;
   SIGNAL PRODUCT	:	STD_LOGIC_VECTOR (8 DOWNTO 0);
	constant CLK_period : time := 10 ns;

BEGIN

   UUT: TEST_GF_Add PORT MAP(
		CLK => CLK, 
		X => X, 
		Y => Y, 
		FINISHED => FINISHED, 
		PRODUCT => PRODUCT
   );


   -- Clock process definitions
   clk50_in_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process 
   begin	
			X <= conv_std_logic_vector(255, 9);
			Y <= conv_std_logic_vector(246, 9); 			 
			wait for 2000 ns;
	   
   end process;

END;
