﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GF_generator
{
    class Program
    {

        private long mCardinality;
        private int mIrreducablePoly;
        private int mGenerator;
        private int mBitsCount;

        public long cardinality
        {
            get { return mCardinality; }
            set { mCardinality = value; }
        }

        public int irreducablePoly
        {
            get { return mIrreducablePoly; }
            set { mIrreducablePoly = value; }
        }

        public int generator
        {
            get { return mGenerator; }
            set { mGenerator = value; }
        }

        public int bitsCount
        {
            get { return mBitsCount; }
            set { mBitsCount = value; }
        }
        
        static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Invalid Input\nUsage: >GF_generator <number_of_bits> <irrreducible_poly> <generetor>" +
                                    "\neg. >GF_generator 8 100011011 00000011");
                return;
            }

            Program instance = new Program();

            instance.bitsCount = int.Parse(args[0]);
            instance.cardinality = Program.IntPower(2, short.Parse(args[0]));
            instance.irreducablePoly = Convert.ToInt32(args[1], 2);
            instance.generator = Convert.ToInt32(args[2], 2);

            int[] values = new int[instance.cardinality];
            int[] powers = new int[instance.cardinality];
            powers[0] = 0;
            powers[1] = 0;
            powers[instance.generator] = 1;

            values[0] = 1;
            values[1] = instance.generator;
            for (int i = 2; i < instance.cardinality; i++)
            {
                values[i] = instance.GF_mul(values[i - 1], instance.generator);
                powers[values[i]] = i;
            }            

            string[] lines = new string[instance.cardinality + 4];
            lines[0] = "; width = " + instance.bitsCount.ToString() + " bits";
            lines[1] = "; depth = " + instance.cardinality.ToString() + " values";
            lines[2] = "memory_initialization_radix=2;";
            lines[3] = "memory_initialization_vector=";
            string comma = ",";

            for (int i = 0; i < instance.cardinality; i++ )
            {
                Console.WriteLine("g{0}={1}", i, values[i]); 
                if (i == instance.cardinality - 1)
                    comma = ";";
                lines[i + 4] = instance.GetIntBinaryString(values[i]) + comma;
            }

            string[] lines_power = new string[instance.cardinality + 4];
            lines_power[0] = "; width = " + instance.bitsCount.ToString() + " bits";
            lines_power[1] = "; depth = " + instance.cardinality.ToString() + " values";
            lines_power[2] = "memory_initialization_radix=2;";
            lines_power[3] = "memory_initialization_vector=";
            comma = ",";

            powers[1] = 0;
            for (int i = 0; i < instance.cardinality; i++)
            {
                Console.WriteLine("{0}={1}", i, powers[i]);
                if (i == instance.cardinality - 1)
                    comma = ";";
                lines_power[i + 4] = instance.GetIntBinaryString(powers[i]) + comma;
            }

            System.IO.File.WriteAllLines(@"ROM_VALUES.coe", lines);
            System.IO.File.WriteAllLines(@"ROM_ADDRESSES.coe", lines_power);
            Console.WriteLine("Created file ROM_VALUES.coe\n");
            Console.WriteLine("Created file ROM_ADDRESSES.coe\n");

        }

        public int GF_mul(int a, int b)
        {            
	        int product = 0;            
            
	        for(int i = 0; i < bitsCount; i++)
            {
    		    if((b & 1) == 1) 
	    		    product ^= a;		        
		        a <<= 1;
		        if((a & (1 << bitsCount)) > 0) 
        			a ^= irreducablePoly;		
	    	    b >>= 1;
	        }
	        return product;
        }


        public static long IntPower(int x, short power)
        {
            if (power == 0) return 1;
            if (power == 1) return x;

            int n = 15;
            while ((power <<= 1) >= 0) 
                n--;

            long tmp = x;
            while (--n > 0)
                tmp = tmp * tmp *
                     (((power <<= 1) < 0) ? x : 1);
            return tmp;
        }

        public string GetIntBinaryString(int n)
        {
            char[] b = new char[bitsCount];
            int pos = bitsCount-1;
            int i = 0;

            while (i < bitsCount)
            {
                if ((n & (1 << i)) != 0)
                {
                    b[pos] = '1';
                }
                else
                {
                    b[pos] = '0';
                }
                pos--;
                i++;
            }
            return new string(b);
        }
    }
}
